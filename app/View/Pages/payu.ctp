
<title>Payment</title>
<style type="text/css">
 

.stepwizard-step p {
    margin-top: 10px;    
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;     
    width: 100%;
    position: relative;
}
.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;
  }
.stepwizard-step {    
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
  width: 40px;
  height: 40px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
  .list-cust .list-group-item:first-child {
        border-top-right-radius: 0px;
        border-top-left-radius: 0px;
    }
    .list-cust .list-group-item:last-child {
        border-bottom-right-radius: 0px;
        border-bottom-left-radius: 0px;
    }
    .list-cust .list-group-item {
       border-right:0px;
       border-left:0px;
    }
    .list-group :not(:first-child) {
        background-color: #EEE;
    }
    .buttonload {
background-color: transparent;
border: none;
color: white;
padding: 3px 26px;
font-size: 19px;
}

/* Add a right margin to each icon */
.fa {
  margin-left: -12px;
  margin-right: 8px;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
 
<div style="background-image: url(<?php echo $this->webroot.'img/bg.jpg';?>); width: 100%; height:100vh;">
  <body onload="submitPayuForm()">
    <div class="container" style="padding-top: 150px;">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-5" style="color:white;">
          <ul style="list-style-type: none; ">
          <li style="background-color: dimgray;padding: 7px;width: 100%; text-align: center;"><h4>Order Id: <?php echo $orderdata['0']['Order']['orderid']; ?></h4></li>
          <li style="background: #c67d00;padding: 3px;width: 100%;text-align: center;"><h3>Amount Payable: <?php echo $orderdata['0']['Order']['price']; ?> INR</h3></li>          
          <li style="border-bottom: 1px solid #6c6c6c; border-left: 1px solid #6c6c6c; border-right: 1px solid #6c6c6c; padding: 10px;"><h4 style="padding: 10px;letter-spacing: 3px; line-height: revert;">Biling Name: <?php echo $orderdata['0']['Order']['name']; ?><br>
          Mail Id: <?php echo $orderdata['0']['Order']['mail']; ?><br>
          Contact No: <?php echo $orderdata['0']['Order']['number']; ?><br>
          Billing Address: <?php echo $orderdata['0']['Order']['address']; ?></h4>
        </li>
        </ul>

   <?php if($formError) { ?>
    <span style="color:red">Please fill all mandatory fields.</span>
      <?php } ?>
      <form action="<?php echo $action; ?>" method="post" name="payuForm">
      <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY ?>" />
      <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
      <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
      <input type="hidden" name="amount" value="<?php echo $price; ?>" />
      <input type="hidden" name="firstname" id="firstname" value="<?php echo $name; ?>" />
      <input type="hidden" name="email" id="email" value="<?php echo $mail; ?>" />
      <input type="hidden" name="phone" value="<?php echo $number; ?>" />
      <input type="hidden" name="productinfo" value="Developerpoint"/>
    
  <td colspan="3"><input type="hidden" name="surl" value="./pages/success/<?php echo $orderdata['0']['Order']['orderid']; ?>" size="64" /></td>
        
  <td colspan="3"><input type="hidden" name="furl" value="https://developerpoint.in/payu/failure.php" size="64" /></td>
       
  <td colspan="3"><input type="hidden" name="service_provider" value="payu_paisa" size="64" /></td>
         
          <?php if(!$hash) { ?>
          <input style="float: right;" type="submit" value="Make Payment" class="btn btn-success btn-lg" />
          <?php } else{ ?>
<button class="buttonload" style="float: right;">
  <i class="fa fa-refresh fa-spin"></i>Please Wait.....
</button>
              <?php }  ?>
<div class="col-md-4"></div>
      </div>
    </div>
</form>

</div>
 



</body>
</div>
