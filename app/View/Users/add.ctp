<!-- app/View/Users/add.ctp -->
<br><br><br><br><br>
<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <?php 
        echo $this->Form->input('username');
        echo $this->Form->input('password');?>
         <input type="text" class="form-control" placeholder="Enter Name" name="data[User][name]" style="height: 42px;" required>
        <?php echo $this->Form->input('mail');
        echo $this->Form->input('role', array(
        'options' => array('admin' => 'Admin', 'author' => 'Author')
        ));
        ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>