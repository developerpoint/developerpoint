    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>
    /* lazyload.js (c) Lorenzo Giuliani
     * MIT License (http://www.opensource.org/licenses/mit-license.html)
     *
     * expects a list of:
     * `<img src="blank.gif" data-src="my_image.png" width="600" height="400" class="lazy">`
     */
     $(function() {
      var $q = function(q, res){
            if (document.querySelectorAll) {
              res = document.querySelectorAll(q);
            } else {
              var d=document
                , a=d.styleSheets[0] || d.createStyleSheet();
              a.addRule(q,'f:b');
              for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
                l[b].currentStyle.f && c.push(l[b]);

              a.removeRule(0);
              res = c;
            }
            return res;
          }
        , addEventListener = function(evt, fn){
            window.addEventListener
              ? this.addEventListener(evt, fn, false)
              : (window.attachEvent)
                ? this.attachEvent('on' + evt, fn)
                : this['on' + evt] = fn;
          }
        , _has = function(obj, key) {
            return Object.prototype.hasOwnProperty.call(obj, key);
          }
        ;

      function loadImage (el, fn) {
        var img = new Image()
          , src = el.getAttribute('data-src');
        img.onload = function() {
          if (!! el.parent)
            el.parent.replaceChild(img, el)
          else
            el.src = src;

          fn? fn() : null;
        }
        img.src = src;
      }

      function elementInViewport(el) {
        var rect = el.getBoundingClientRect()

        return (
           rect.top    >= 0
        && rect.left   >= 0
        && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
        )
      }

        var images = new Array()
          , query = $q('img.lazy')
          , processScroll = function(){
              for (var i = 0; i < images.length; i++) {
                if (elementInViewport(images[i])) {
                  loadImage(images[i], function () {
                    images.splice(i, i);
                  });
                }
              };
            }
          ;
        // Array.prototype.slice.call is not callable under our lovely IE8
        for (var i = 0; i < query.length; i++) {
          images.push(query[i]);
        };

        processScroll();
        addEventListener('scroll',processScroll);

      });
    </script>
<br>
<center>
 <h1 style="text-align:center;padding-top: 30px;font-size: 31px;color:  #3498db ; text-shadow: 1px 1px 1px #a4a2a2;"><strong>OUR PROJECTS</strong></h1>
<h4> <a href=""  style="text-decoration:none; color: #16a085; text-align:center;" class="typewrite" data-period="2000" data-type='[ "Hi !!!", "We design and develope website for every kind of business or Personal use.", "Our responsive design and highly customizible developement", "is the best in industries","Have a Nice day !!!"  ]'>

    <span class="wrap"></span>
  </a></h4></center>
   <!-- portfolio div -->
    <div class="portfolio-div" style="padding-top: 30px;">
        <div class="portfolio">
            <div class="no-padding portfolio_container">
                <!-- single work -->
                <div class="col-md-3 col-sm-6 fashion ads">
                    <a href="./img/developerpoint.png" class="portfolio_item">
             <img src="blank.gif" class="lazy" data-src="./img/p24.png" height="337" width="350">
                        
                        <div class="portfolio_item_hover">
                            <div class="portfolio-border clearfix">
                                <div class="item_info">
                                    <span>Business Website</span>
                                    <em>View</em>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
                <div class="col-md-3 col-sm-6  fashion logo">
                    <a href="single-project.html" class="portfolio_item">
          <img src="blank.gif" class="lazy" data-src="./img/project1.png" height="337" width="350">
                       
                        <div class="portfolio_item_hover">
                            <div class="portfolio-border clearfix">
                                <div class="item_info">
                                    <span>Mockups in seconds</span>
                                    <em>Fashion / Logo</em>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- end single work -->

                <!-- single work -->
                <div class="col-md-3 col-sm-6 ads graphics">
                    <a href="single-project.html" class="portfolio_item">
              <img src="blank.gif" class="lazy" data-src="./img/project3.png" height="337" width="350">
                      
                        <div class="portfolio_item_hover">
                            <div class="portfolio-border clearfix">
                                <div class="item_info">
                                    <span>Floating mockups</span>
                                    <em>Ads / Graphics</em>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- end single work -->

                <!-- single work -->
               <div class="col-md-3 col-sm-6 fashion ads">
                    <a href="./img/developerpoint-in-trip.png" class="portfolio_item">
                <img src="blank.gif" class="lazy" data-src="./img/p27.png" height="337" width="350">
                        
                        <div class="portfolio_item_hover">
                            <div class="portfolio-border clearfix">
                                <div class="item_info">
                                    <span>Teavel website</span>
                                    <em>View</em>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- end single work -->

                <!-- single work -->
                <div class="col-md-3 col-sm-6 fashion ads">
                    <a href="single-project.html" class="portfolio_item">
                  <img src="blank.gif" class="lazy" data-src="./img/project2.png" height="337" width="350">
                        
                        <div class="portfolio_item_hover">
                            <div class="portfolio-border clearfix">
                                <div class="item_info">
                                    <span>Held by hands</span>
                                    <em>Fashion / Ads</em>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- end single work -->
                

                <div class="col-md-3 col-sm-6 fashion ads">
                    <a href="./img/lamemoirs.png" class="portfolio_item">
                        <img src="blank.gif" class="lazy" data-src="./img/p26.png" alt="image" height="337" width="350" />
                        <div class="portfolio_item_hover">
                            <div class="portfolio-border clearfix">
                                <div class="item_info">
                                    <span>Event Management website</span>
                                    <em>View</em>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-6 fashion ads">
                    <a href="./img/organicgreenbean.png" class="portfolio_item">
                        <img src="blank.gif" class="lazy" data-src="./img/p25.png" alt="image" height="337" width="350" />
                        <div class="portfolio_item_hover">
                            <div class="portfolio-border clearfix">
                                <div class="item_info">
                                    <span>E-CommerceWebsite</span>
                                    <em>View</em>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                



                <div class="col-md-3 col-sm-6 fashion ads">
                    <a href="./img/greencoffeeraaz.png" class="portfolio_item">
   <img src="blank.gif" class="lazy" data-src="./img/p23.png" height="337" width="350">                      <div class="portfolio_item_hover">
                            <div class="portfolio-border clearfix">
                                <div class="item_info">
                                    <span>E-commerce Website</span>
                                    <em>View</em>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- single work -->
                
                <!-- end single work -->

            </div>
            <!-- end portfolio_container -->
        </div>
        <!-- portfolio -->
    </div>
    <!-- end portfolio div -->

