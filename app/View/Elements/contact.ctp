

    <!-- main-container -->
    <div class="container main-container">
        <br><br>
        <div class="col-md-6">
            <form action="#" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-contact">
                            <input type="text" name="name">
                            <span>your name</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-contact">
                            <input type="text" name="email">
                            <span>your email</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="input-contact">
                            <input type="text" name="object">
                            <span>object</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="textarea-contact">
                            <textarea name="message"></textarea>
                            <span>message</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                       <center> <a href="#" class="btn btn-box" style="font-size: 2rem; background: #16a085; width: 521px;">Send</a></center>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6">
            <h1 class="text-uppercase" style="color: #16a085;">contact me</h1>
            <h5>Creative & Lorem ipsum dolor sit amet</h5>
            <div class="h-30"></div>
            <p style="margin-bottom: 5rem; font-size: 14px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliter enim nosmet ipsos nosse non possumus. Inscite autem medicinae et gubernationis ultimum cum ultimo sapientiae comparatur. Tecum optime, deinde etiam cum mediocri amico. Et nemo nimium beatus est; Ac ne plura complectar-sunt enim innumerabilia-, bene laudata virtus voluptatis aditus </p>
            <div class="contact-info">
                <h2><p><i class="ion-android-call" style="background: #16a085;"></i> +91 8001691299</p></h2>
               <h2> <p><i class="ion-ios-email" style="background: #16a085;"></i> biswanath@developerpoint.in</p></h2>
            </div>
        </div>


    </div>
    <!-- end main-container -->