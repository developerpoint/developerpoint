<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Developerpoint</title>
    <link rel="icon" href="img/logo.png" type="image/x-icon">

    <!-- Bootstrap -->
    <?php 
    echo $this->Html->css('bootstrap.min.css');
    echo $this->Html->css('ionicons.min.css');
    echo $this->Html->css('style.css');
    echo $this->Html->css('bootstrap.min.css');

     ?>
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="ionicons/css/ionicons.min.css" rel="stylesheet">

   
   <link href="css/style.css" rel="stylesheet"> -->

<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">  -->
  
    <!-- <script src="js/modernizr.js"></script> -->
   <?php  

       echo $this->Html->script('modernizr.js');
    ?>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
    WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<!------ Include the above in your HEAD tag ---------->
<style>
 
    #webcoderskull{
    position: absolute;
    left: 0;
    top: 50%;
    padding: 0 20px;
    width: 100%;
    text-align: center;
}

canvas{
    height:100vh;
    /*background-color:#16a085;*/
    background-color:#0D6146;
}
#webcoderskull h1{
  letter-spacing: 5px;
  font-size: 5rem;
  font-family: 'Roboto', sans-serif;
  text-transform: uppercase;
  font-weight: bold;
}

.container span{
  text-transform: uppercase;
  display: block;
}
.mytext1{
  color: white;
  font-size: 60px;
  font-weight: 700;
  letter-spacing: 8px;
  margin-bottom: 20px;
 
  position: relative;
  animation: text 3s 1;

}
.mytext2{
  font-size: 26px;
    color: white;
    font-weight: bold;
    font-family: cursive;
}
@keyframes text {
  0%{
    color: black;
    margin-bottom: -40px;

  }
  30%{
    letter-spacing: 18px;
    margin-bottom: -40px;

  }
  85%{
    letter-spacing: 8px;
    margin-bottom: -40px;

  }
}
.di mark{
  background: none;
}
</style>

<!-- <mark style="color: #4285f4;">D</mark><mark style="color: #ea4335;">e</mark><mark style="color: #fbbc05;">v</mark><mark style="color:  #2ecc71 ;">e</mark><mark style="color:  #3498db ;">l</mark><mark style="color:  #f39c12 ;">o</mark><mark>p</mark><mark>e</mark><mark>r</mark><mark>p</mark><mark>o</mark><mark>i</mark><mark>n</mark><mark>t</mark> -->
</head>
<body>
    <div class="container-fluid" >
        <!-- box header -->
        <header class="box-header">
            <div class="box-logo">
                <a href="index.html">
        <h2><b class="is-visible" style="color:white; letter-spacing: 3px; font-size: 25px;">  
            Developerpoint</b></h2>
        <h5 style="letter-spacing:8px; color: white; font-size: 10px;">DELIVERING VALUE</h5></a>
            </div>
            <!-- box-nav -->
            <a class="box-primary-nav-trigger" href="#">
               <span class="box-menu-text" >Menu</span>
               <span class="box-menu-icon"></span>
            </a>
            <!-- box-primary-nav-trigger -->
        </header>
        <!-- end box header -->
        <!-- nav -->
        <nav>
            <ul class="box-primary-nav">
                <li class="box-label">About me</li>
                <li><a href="index.html">Intro</a> <i class="ion-ios-circle-filled color"></i></li>
                <li><a href="about.html">About me</a></li>
                <li><a href="services.html">services</a></li>
                <li><a href="portfolio.html">portfolio</a></li>
                <li><a href="contact.html">contact me</a></li>
                <li class="box-label">Follow me</li>
                <li class="box-social"><a href="#0"><i class="ion-social-facebook"></i></a></li>
                <li class="box-social"><a href="#0"><i class="ion-social-instagram-outline"></i></a></li>
                <li class="box-social"><a href="#0"><i class="ion-social-twitter"></i></a></li>
                <li class="box-social"><a href="#0"><i class="ion-social-dribbble"></i></a></li>
            </ul>
        </nav>
        <!-- end nav -->
            
    </div>
  