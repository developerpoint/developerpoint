<?php    
App::uses('CakeEmail', 'Network/Email');
class RegistersController extends AppController
{
	 
	public $helper =array("Html","Form");
	//public $uses=array("Register");
	public $uses=array("Email","Category","Register","Product","Theme","Location","Page","PetProfile","Order","OrderItem","RecentScan","UserActivityLog","Faq","TagReplacementRequest","TagReplacementHistory","CheckOut","Cart","State","PaymentDetail", "Menu", "Submenu", "AdminFunctionality","Operator","Role","StripeData","TagChangeRequest","PaymentHistory","TagChangeHistory");

  	//public $components = array("Session","Paginator","Captcha");
	public $components=array("Session","Captcha","Twilio.Twilio","Stripe.Stripe","Paginator","Folder");

//////////////////////////////////////////////////

	public function beforeFilter()
	{
		parent::beforeFilter();
			 
		if(!$this->Auth->loggedIn())
		{
			$this->Auth->deny("setting");	 
		}
	}

///////////////////////////////////////////////////

	public function login($p_type=null,$cart_id=null,$session_id=null)
	{
		error_reporting(0);
		$this->layout = 'site_login';
		if($this->request->is("post"))
		{
			if(isset($this->request->data['registerbtn']) && !empty($this->request->data['registerbtn'])) 
			{
				$this->request->data["Register"]["mailrand"]=$this->random(12);
				$this->request->data["Register"]["smsrand"]=$this->random(5);
				  
				$this->request->data["Register"]["user_type"]="S";
				$this->request->data["Register"]["status"]="0";
				
				$name = $this->request->data["Register11"]["first_name"]." ".$this->request->data["Register11"]["last_name"];
				$email_send = $this->request->data["Register11"]["email"];
				$this->request->data["Register"]["name"]=$name;
				$this->request->data["Register"]["email"]=$this->request->data["Register11"]["email"];
				$this->request->data["Register"]["password"]=$this->request->data["Register11"]["password"];
				$this->request->data["Register"]["hint"]=$this->request->data["Register11"]["password"];
				$this->request->data["Register"]["account_no"]="55667788".$this->random(5).date("s")."1";
								 
				$this->Register->create();
				if($this->Register->save($this->request->data))
				{

				   	$massage = '';
				   	$massage.='Hello <strong>'.$name.'</strong>,<br/>';
                   	$massage.='We have created your account. Please wait for approval. After approving, you will receive an email with user id and password';
                   	$massage.='<br/> Thanks, <br/> Pinpaws.com';
				   	$mymail = $email_send;
				   	$mail=$this->Email->find('first');//*****************
				   	$Email = new CakeEmail();
				   	$Email->emailFormat('html');
				   	$siteName=Configure::read("site_name");
				   	$email =$mail['Email']['info_email'];// "info@pinpaws.com";
				   	$Email->from(array($email=>$siteName));
				   	$Email->to($mymail);
				   	$Email->subject($mail['Email']['email_verification']/*"Email verification mail from ".$siteName*/);
				   	$Email->send($massage);
				   	$this->Session->setFlash(__("Registration successful please login with your details"));										
				}
			}else{
									
				$this->request->data["Register"]["email"]=$this->request->data["email"];
				$this->request->data["Register"]["password"]=$this->request->data["password"];
				if($this->Auth->login())
				{
					$activity_name="login";
                    $activity_date=date("Y-m-d H:i:s");;
                    $this->update_price();
                    $p_type=$this->request->data["checkout_type"];
                    $cart_id=$this->request->data["checkout_cart_id"];
                    $this->user_activity_log($activity_name,$activity_date);
					$u_tupe="S";										       
					$currentUser = $this->Auth->user();
                    $type = $currentUser['user_type'];

					$email_verified = $currentUser['email_verified'];
					if($email_verified==1){
						$this->Register->id=$currentUser['id'];
						$this->request->data["Register"]["is_login"]=1;
						$this->Register->save($this->request->data);
						$session_id=$this->Session->read('browser_session');
						if(!empty($session_id) )
						{
							$this->redirect(array('controller'=>'pages','action' => 'check_out',$session_id,$cart_id,$p_type));
						}
						else{
							$this->redirect(array("controller"=>"registers","action"=>"overview_user",$currentUser['id'])); 
						}
						if($type==$u_tupe){
							if($type=='S'){
								$this->redirect(array("controller"=>"registers","action"=>"overview_user",$currentUser['id'])); 
							}else{
								$this->redirect(array("controller"=>"registers","action"=>"view_user_message",$currentUser['id']));			
							}
						}
						else{
						   	unset($order_details);
						   	unset($cart_details);
						   	$this->Session->delete("userLogin");
						   	$this->Session->setFlash(__("Invalid Username/Password."));
						   	$this->redirect($this->Auth->logout());
						}
					}else{
						$this->Folder->createuser('usa',$currentUser['id']);
						$this->redirect(array('controller'=>'pages','action' => 'product'));											  
					}
				}else
				{
					$this->Session->setFlash(__("Invalid Username/Password."));
				}
			}		
		}
		$page_cms = $this->Page->find('first', array(
            'conditions' => array(
                'Page.slug' => "login-cms"
            )
        ));
       
		$this->set('page_cms',$page_cms);
	}

//////////////////////////////////////////////////////////

	public function view_message(){
		$this->layout="site_login";
	}

///////////////////////////////////////////////////////

	public function view_user_message(){
		$this->layout="site_login";
	}

///////////////////////////////////////////////////////

	public function change_password($email=null,$uid=null,$passcode=null){
		$this->layout="site_login";
		// echo Configure::read('URL').'registers/login';
		$user_email=base64_decode($email);
		$user_id=base64_decode($uid);
		$user_passcode=base64_decode($passcode);
		$datas=$this->Register->find('all', array(
         	'recursive' => -1,
		 	'contain' => array(
	    	),
	    	'conditions'=>array(
               	'Register.email' =>$user_email,
				'Register.id' =>$user_id,
				'Register.is_pass_change' =>1,
				'Register.passcode' =>$user_passcode
            ),
        	'order' => array(
                'Register.id' => 'DESC'
            )
        ));
		$this->set(compact('datas'));
		if($this->request->is("post")){
			$this->Register->id=$user_id;
			$this->request->data['Register']['password']=$this->request->data["password"];
			$this->request->data['Register']['hint']=$this->request->data["password"];
			$this->Register->save($this->request->data);

			$this->redirect(Configure::read('URL').'registers/login');
		}	
	}

//////////////////////////////////////////////////////

	public function forget_password($id=null){
		$mails=$this->Email->find('first');
		$this->layout = 'site_login';
		$this->set('id',$id);
		if($this->request->is("post")){
			$check_email=$this->request->data['email'];
			$exit_email=$this->Register->findByEmail($check_email);
			if(!empty($exit_email)){
				$customer_id=$exit_email['Register']['id'];
				$name=$exit_email['Register']['name'];
				$user_email=$exit_email['Register']['email'];
				$password=$exit_email['Register']['hint'];
				$email_send=$check_email;
				$passcode=time();
				$forget_user_id = base64_encode($customer_id);
				$forget_pass_link=base64_encode($user_email);
				$forget_passcode=base64_encode($passcode);
				$this->Register->id=$customer_id;
				$this->request->data['Register']['is_pass_change']=1;
				$this->request->data['Register']['passcode']=$passcode;
				$this->Register->save($this->request->data);
				if(isset($this->request->data['email']) && !empty($this->request->data['email'])){
											   $massage = '';
											   $massage.='<!doctype html>
		<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
			<head>
				<!-- NAME: FOLLOW UP -->
				<!--[if gte mso 15]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
				<!--[endif]-->
				<meta charset="UTF-8">
				<meta https-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<title>Your Current Pin Paws Password</title>
				
			<style type="text/css">
				p{
					margin:10px 0;
					padding:0;
				}
				table{
					border-collapse:collapse;
				}
				h1,h2,h3,h4,h5,h6{
					display:block;
					margin:0;
					padding:0;
				}
				img,a img{
					border:0;
					height:auto;
					outline:none;
					text-decoration:none;
				}
				body,#bodyTable,#bodyCell{
					height:100%;
					margin:0;
					padding:0;
					width:100%;
				}
				.mcnPreviewText{
					display:none !important;
				}
				#outlook a{
					padding:0;
				}
				img{
					-ms-interpolation-mode:bicubic;
				}
				table{
					mso-table-lspace:0pt;
					mso-table-rspace:0pt;
				}
				.ReadMsgBody{
					width:100%;
				}
				.ExternalClass{
					width:100%;
				}
				p,a,li,td,blockquote{
					mso-line-height-rule:exactly;
				}
				a[href^=tel],a[href^=sms]{
					color:inherit;
					cursor:default;
					text-decoration:none;
				}
				p,a,li,td,body,table,blockquote{
					-ms-text-size-adjust:100%;
					-webkit-text-size-adjust:100%;
				}
				.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
					line-height:100%;
				}
				a[x-apple-data-detectors]{
					color:inherit !important;
					text-decoration:none !important;
					font-size:inherit !important;
					font-family:inherit !important;
					font-weight:inherit !important;
					line-height:inherit !important;
				}
				.templateContainer{
					max-width:600px !important;
				}
				a.mcnButton{
					display:block;
				}
				.mcnImage{
					vertical-align:bottom;
				}
				.mcnTextContent{
					word-break:break-word;
				}
				.mcnTextContent img{
					height:auto !important;
				}
				.mcnDividerBlock{
					table-layout:fixed !important;
				}
			/*
			@tab Page
			@section Heading 1
			@style heading 1
			*/
				h1{
					/*@editable*/color:#222222;
					/*@editable*/font-family:Helvetica;
					/*@editable*/font-size:40px;
					/*@editable*/font-style:normal;
					/*@editable*/font-weight:bold;
					/*@editable*/line-height:150%;
					/*@editable*/letter-spacing:normal;
					/*@editable*/text-align:center;
				}
			/*
			@tab Page
			@section Heading 2
			@style heading 2
			*/
				h2{
					/*@editable*/color:#222222;
					/*@editable*/font-family:Helvetica;
					/*@editable*/font-size:34px;
					/*@editable*/font-style:normal;
					/*@editable*/font-weight:bold;
					/*@editable*/line-height:150%;
					/*@editable*/letter-spacing:normal;
					/*@editable*/text-align:left;
				}
			/*
			@tab Page
			@section Heading 3
			@style heading 3
			*/
				h3{
					/*@editable*/color:#444444;
					/*@editable*/font-family:Helvetica;
					/*@editable*/font-size:22px;
					/*@editable*/font-style:normal;
					/*@editable*/font-weight:bold;
					/*@editable*/line-height:150%;
					/*@editable*/letter-spacing:normal;
					/*@editable*/text-align:center;
				}
			/*
			@tab Page
			@section Heading 4
			@style heading 4
			*/
				h4{
					/*@editable*/color:#999999;
					/*@editable*/font-family:Georgia;
					/*@editable*/font-size:20px;
					/*@editable*/font-style:italic;
					/*@editable*/font-weight:normal;
					/*@editable*/line-height:125%;
					/*@editable*/letter-spacing:normal;
					/*@editable*/text-align:left;
				}
			/*
			@tab Header
			@section Header Container Style
			*/
				#templateHeader{
					/*@editable*/background-color:#ea4847;
					/*@editable*/background-image:none;
					/*@editable*/background-repeat:no-repeat;
					/*@editable*/background-position:50% 50%;
					/*@editable*/background-size:cover;
					/*@editable*/border-top:0;
					/*@editable*/border-bottom:0;
					/*@editable*/padding-top:0px;
					/*@editable*/padding-bottom:0px;
				}
			/*
			@tab Header
			@section Header Interior Style
			*/
				.headerContainer{
					/*@editable*/background-color:#transparent;
					/*@editable*/background-image:none;
					/*@editable*/background-repeat:no-repeat;
					/*@editable*/background-position:center;
					/*@editable*/background-size:cover;
					/*@editable*/border-top:0;
					/*@editable*/border-bottom:0;
					/*@editable*/padding-top:0;
					/*@editable*/padding-bottom:0;
				}
			/*
			@tab Header
			@section Header Text
			*/
				.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
					/*@editable*/color:#808080;
					/*@editable*/font-family:Helvetica;
					/*@editable*/font-size:16px;
					/*@editable*/line-height:150%;
					/*@editable*/text-align:left;
				}
			/*
			@tab Header
			@section Header Link
			*/
				.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
					/*@editable*/color:#00ADD8;
					/*@editable*/font-weight:normal;
					/*@editable*/text-decoration:underline;
				}
			/*
			@tab Body
			@section Body Container Style
			*/
				#templateBody{
					/*@editable*/background-color:#ffffff;
					/*@editable*/background-image:none;
					/*@editable*/background-repeat:no-repeat;
					/*@editable*/background-position:center;
					/*@editable*/background-size:cover;
					/*@editable*/border-top:0;
					/*@editable*/border-bottom:0;
					/*@editable*/padding-top:20px;
					/*@editable*/padding-bottom:54px;
				}
			/*
			@tab Body
			@section Body Interior Style
			*/
				.bodyContainer{
					/*@editable*/background-color:#transparent;
					/*@editable*/background-image:none;
					/*@editable*/background-repeat:no-repeat;
					/*@editable*/background-position:center;
					/*@editable*/background-size:cover;
					/*@editable*/border-top:0;
					/*@editable*/border-bottom:0;
					/*@editable*/padding-top:0;
					/*@editable*/padding-bottom:0;
				}
			/*
			@tab Body
			@section Body Text
			*/
				.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
					/*@editable*/color:#808080;
					/*@editable*/font-family:Helvetica;
					/*@editable*/font-size:16px;
					/*@editable*/line-height:150%;
					/*@editable*/text-align:left;
				}
			/*
			@tab Body
			@section Body Link
			*/
				.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
					/*@editable*/color:#00ADD8;
					/*@editable*/font-weight:normal;
					/*@editable*/text-decoration:underline;
				}
			/*
			@tab Footer
			@section Footer Style
			*/
				#templateFooter{
					/*@editable*/background-color:#333333;
					/*@editable*/background-image:none;
					/*@editable*/background-repeat:no-repeat;
					/*@editable*/background-position:center;
					/*@editable*/background-size:cover;
					/*@editable*/border-top:0;
					/*@editable*/border-bottom:0;
					/*@editable*/padding-top:45px;
					/*@editable*/padding-bottom:63px;
				}
			/*
			@tab Footer
			@section Footer Interior Style
			*/
				.footerContainer{
					/*@editable*/background-color:transparent;
					/*@editable*/background-image:none;
					/*@editable*/background-repeat:no-repeat;
					/*@editable*/background-position:center;
					/*@editable*/background-size:cover;
					/*@editable*/border-top:0;
					/*@editable*/border-bottom:0;
					/*@editable*/padding-top:0;
					/*@editable*/padding-bottom:0;
				}
			/*
			@tab Footer
			@section Footer Text
			*/
				.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
					/*@editable*/color:#FFFFFF;
					/*@editable*/font-family:Helvetica;
					/*@editable*/font-size:12px;
					/*@editable*/line-height:150%;
					/*@editable*/text-align:center;
				}
			/*
			@tab Footer
			@section Footer Link
			*/
				.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
					/*@editable*/color:#FFFFFF;
					/*@editable*/font-weight:normal;
					/*@editable*/text-decoration:underline;
				}
			@media only screen and (min-width:768px){
				.templateContainer{
					width:600px !important;
				}

		}	@media only screen and (max-width: 480px){
				body,table,td,p,a,li,blockquote{
					-webkit-text-size-adjust:none !important;
				}

		}	@media only screen and (max-width: 480px){
				body{
					width:100% !important;
					min-width:100% !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnImage{
					width:100% !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
					max-width:100% !important;
					width:100% !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnBoxedTextContentContainer{
					min-width:100% !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnImageGroupContent{
					padding:9px !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
					padding-top:9px !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
					padding-top:18px !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnImageCardBottomImageContent{
					padding-bottom:9px !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnImageGroupBlockInner{
					padding-top:0 !important;
					padding-bottom:0 !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnImageGroupBlockOuter{
					padding-top:9px !important;
					padding-bottom:9px !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnTextContent,.mcnBoxedTextContentColumn{
					padding-right:18px !important;
					padding-left:18px !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
					padding-right:18px !important;
					padding-bottom:0 !important;
					padding-left:18px !important;
				}

		}	@media only screen and (max-width: 480px){
				.mcpreview-image-uploader{
					display:none !important;
					width:100% !important;
				}

		}	@media only screen and (max-width: 480px){
			/*
			@tab Mobile Styles
			@section Heading 1
			@tip Make the first-level headings larger in size for better readability on small screens.
			*/
				h1{
					/*@editable*/font-size:30px !important;
					/*@editable*/line-height:125% !important;
				}

		}	@media only screen and (max-width: 480px){
			/*
			@tab Mobile Styles
			@section Heading 2
			@tip Make the second-level headings larger in size for better readability on small screens.
			*/
				h2{
					/*@editable*/font-size:26px !important;
					/*@editable*/line-height:125% !important;
				}

		}	@media only screen and (max-width: 480px){
			/*
			@tab Mobile Styles
			@section Heading 3
			@tip Make the third-level headings larger in size for better readability on small screens.
			*/
				h3{
					/*@editable*/font-size:20px !important;
					/*@editable*/line-height:150% !important;
				}

		}	@media only screen and (max-width: 480px){
			/*
			@tab Mobile Styles
			@section Heading 4
			@tip Make the fourth-level headings larger in size for better readability on small screens.
			*/
				h4{
					/*@editable*/font-size:18px !important;
					/*@editable*/line-height:150% !important;
				}

		}	@media only screen and (max-width: 480px){
			/*
			@tab Mobile Styles
			@section Boxed Text
			@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
			*/
				.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
					/*@editable*/font-size:14px !important;
					/*@editable*/line-height:150% !important;
				}

		}	@media only screen and (max-width: 480px){
			/*
			@tab Mobile Styles
			@section Header Text
			@tip Make the header text larger in size for better readability on small screens.
			*/
				.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
					/*@editable*/font-size:16px !important;
					/*@editable*/line-height:150% !important;
				}

		}	@media only screen and (max-width: 480px){
			/*
			@tab Mobile Styles
			@section Body Text
			@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
			*/
				.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
					/*@editable*/font-size:16px !important;
					/*@editable*/line-height:150% !important;
				}

		}	@media only screen and (max-width: 480px){
			/*
			@tab Mobile Styles
			@section Footer Text
			@tip Make the footer content text larger in size for better readability on small screens.
			*/
				.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
					/*@editable*/font-size:14px !important;
					/*@editable*/line-height:150% !important;
				}

		}}</style></head>
			<body>
				
				<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></span><!--<!--[endif]-->
				<!--*|END:IF|*-->
				<center>
					<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
						<tr>
							<td align="center" valign="top" id="bodyCell">
								<!-- BEGIN TEMPLATE // -->
								<table border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td align="center" valign="top" id="templateHeader" data-template-container  style="background-color: #ea4847; background-image: none; background-repeat: no-repeat; background-position: 50% 50%; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 0px; padding-bottom: 0px;">
											<!--[if gte mso 9]>
											<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
											<tr>
											<td align="center" valign="top" width="600" style="width:600px;">
											<![endif]-->
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
												<tr>
													<td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody class="mcnImageBlockOuter">
					<tr>
						<td style="padding:9px" class="mcnImageBlockInner" valign="top">
							<table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
								<tbody><tr>
									<td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
										
											<a href="https://pinpaws.com" title="" class="" target="_blank">
												<img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="192" align="middle">
											</a>
										
									</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
			</tbody>
		</table></td>
												</tr>
											</table>
											<!--[if gte mso 9]>
											</td>
											</tr>
											</table>
											<![endif]-->
										</td>
									</tr>
									<tr>
										<td align="center" valign="top" id="templateBody" data-template-container style=" background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 20px; padding-bottom: 54px; ">
											<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
											<tr>
											<td align="center" valign="top" width="600" style="width:600px;">
											<![endif]-->
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
												<tr>
													<td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody class="mcnTextBlockOuter">
				<tr>
					<td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
						<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->
						
						<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
						<table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
							<tbody><tr>
								
								<td class="mcnTextContent" style="padding: 0px 18px 9px; text-align: center" valign="top">
								

									<h1><span style="font-size:30px; line-height: 40px;"><strong>Forgot Your Password?<br>Let\'s Get You a New One ðŸ™‚ </strong></span></h1>

									


								</td>
							</tr>
						</tbody></table>
						<!--[if mso]>
						</td>
						<![endif]-->
						
						<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
					</td>
				</tr>
			</tbody>
		</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody class="mcnTextBlockOuter">
				<tr>
					<td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
						<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->
						
						<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
						<table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
							<tbody><tr>
								
								<td class="mcnTextContent" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;color: #808080;font-family: Helvetica;font-size: 16px;line-height: 150%;text-align: left;" valign="top">
								
									

								</td>
							</tr>
						</tbody></table>
						<!--[if mso]>
						</td>
						<![endif]-->
						
						<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
					</td>
				</tr>
			</tbody>
		</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
			<tbody class="mcnDividerBlockOuter">
				<tr>
					<td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
						<table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
							<tbody><tr>
								<td>
									<span></span>
								</td>
							</tr>
						</tbody></table>
		<!--            
						<td class="mcnDividerBlockInner" style="padding: 18px;">
						<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
		-->
					</td>
				</tr>
			</tbody>
		</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody class="mcnButtonBlockOuter">
				<tr>
					<td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
						<table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
							<tbody>
								<tr>
									<td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" valign="middle" align="center">
										<a class="mcnButton " title="Click here to Login" href="'.Configure::read('URL').'/registers/change_password/'.$forget_pass_link.'/'.$forget_user_id.'/'.$forget_passcode.'" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Reset Password</a>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
			<tbody class="mcnDividerBlockOuter">
				<tr>
					<td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
						<table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
							<tbody><tr>
								<td>
									<span></span>
								</td>
							</tr>
						</tbody></table>
		<!--            
						<td class="mcnDividerBlockInner" style="padding: 18px;">
						<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
		-->
					</td>
				</tr>
			</tbody>
		</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
			<tbody class="mcnDividerBlockOuter">
				<tr>
					<td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
						<table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
							<tbody><tr>
								<td>
									<span></span>
								</td>
							</tr>
						</tbody></table>
		<!--            
						<td class="mcnDividerBlockInner" style="padding: 18px;">
						<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
		-->
					</td>
				</tr>
			</tbody>
		</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<!--[if gte mso 9]>
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
			<![endif]-->
			<tbody class="mcnBoxedTextBlockOuter">
				<tr>
					<td class="mcnBoxedTextBlockInner" valign="top">
						
						<!--[if gte mso 9]>
						<td align="center" valign="top" ">
						<![endif]-->
						<table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
							<tbody><tr>
								
								<td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
								
									<table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
										<tbody><tr>
											<td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
												<h3 style="text-align: center;">Have Questions?</h3>

		<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:'.$mails['Email']['support_email'].'" target="_blank">'.$mails['Email']['support_email'].'</a></div>

											</td>
										</tr>
									</tbody></table>
								</td>
							</tr>
						</tbody></table>
						<!--[if gte mso 9]>
						</td>
						<![endif]-->
						
						<!--[if gte mso 9]>
						</tr>
						</table>
						<![endif]-->
					</td>
				</tr>
			</tbody>
		</table></td>
												</tr>
											</table>
											<!--[if gte mso 9]>
											</td>
											</tr>
											</table>
											<![endif]-->
										</td>
									</tr>
									
									</tr>
								</table>
								<!-- // END TEMPLATE -->
							</td>
						</tr>
					</table>
					<table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
					<!--[if gte mso 9]>
					<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
					<![endif]-->
					<tbody class="mcnBoxedTextBlockOuter">
					<tr>
										<td align="center" valign="top" id="templateFooter" data-template-container style="background-color: #333333; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 45px; padding-bottom: 63px;">
											<!--[if gte mso 9]>
											<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
											<tr>
											<td align="center" valign="top" width="600" style="width:600px;">
											<![endif]-->
											<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
												<tr>
													<td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody class="mcnFollowBlockOuter">
				<tr>
					<td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
						<table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody><tr>
				<td style="padding-left:9px;padding-right:9px;" align="center">
					<table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
						<tbody><tr>
							<td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
								<table cellspacing="0" cellpadding="0" border="0" align="center">
									<tbody><tr>
										<td valign="top" align="center">
											<!--[if mso]>
											<table align="center" border="0" cellspacing="0" cellpadding="0">
											<tr>
											<![endif]-->
											
												<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->
												
												
													<table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
														<tbody><tr>
															<td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
																<table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
																	<tbody><tr>
																		<td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
																			<table width="" cellspacing="0" cellpadding="0" border="0" align="left">
																				<tbody><tr>
																					
																						<td class="mcnFollowIconContent" width="24" valign="middle" align="center">
																							<a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
																						</td>
																					
																					
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
													</tbody></table>
												
												<!--[if mso]>
												</td>
												<![endif]-->
											
												<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->
												
												
													<table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
														<tbody><tr>
															<td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
																<table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
																	<tbody><tr>
																		<td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
																			<table width="" cellspacing="0" cellpadding="0" border="0" align="left">
																				<tbody><tr>
																					
																						<td class="mcnFollowIconContent" width="24" valign="middle" align="center">
																							<a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
																						</td>
																					
																					
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
													</tbody></table>
												
												<!--[if mso]>
												</td>
												<![endif]-->
											
												<!--[if mso]>
												<td align="center" valign="top">
												<![endif]-->
												
												
													<table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
														<tbody><tr>
															<td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
																<table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
																	<tbody><tr>
																		<td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
																			<table width="" cellspacing="0" cellpadding="0" border="0" align="left">
																				<tbody><tr>
																					
																						<td class="mcnFollowIconContent" width="24" valign="middle" align="center">
																							<a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
																						</td>
																					
																					
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
													</tbody></table>
												
												<!--[if mso]>
												</td>
												<![endif]-->
											
											<!--[if mso]>
											</tr>
											</table>
											<![endif]-->
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
					</tbody></table>
				</td>
			</tr>
		</tbody></table>

					</td>
				</tr>
			</tbody>
		</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
			<tbody class="mcnDividerBlockOuter">
				<tr>
					<td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
						<table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
							<tbody><tr>
								<td>
									<span></span>
								</td>
							</tr>
						</tbody></table>
		<!--            
						<td class="mcnDividerBlockInner" style="padding: 18px;">
						<hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
		-->
					</td>
				</tr>
			</tbody>
		</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
			<tbody class="mcnTextBlockOuter">
				<tr>
					<td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
						<!--[if mso]>
						<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
						<tr>
						<![endif]-->
						
						<!--[if mso]>
						<td valign="top" width="600" style="width:600px;">
						<![endif]-->
						<table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
							<tbody><tr>
								
								<td class="mcnTextContent" style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;text-align: center;color: #fff;" valign="top">
								
									<em>Copyright Â© '.date("Y").' Pin Paws. All rights reserved.</em><br>
		<br>
		<strong>Our mailing address is:</strong><br>
		16775 Addison Rd., Suite 600 Addison, TX 75001<br>
		&nbsp;
								</td>
							</tr>
						</tbody></table>
						<!--[if mso]>
						</td>
						<![endif]-->
						
						<!--[if mso]>
						</tr>
						</table>
						<![endif]-->
					</td>
				</tr>
			</tbody>
		</table></td>
												</tr>
											</table>
											<!--[if gte mso 9]>
											</td>
											</tr>
											</table>
											<![endif]-->
										</td>
							</tr>
						</tbody></table>
				</center>
			</body>
		</html>
		';
											   $mail=$this->Email->find('first');
											   $mymail =$user_email;
											   $Email = new CakeEmail();
											   $Email->emailFormat('html');
											   $siteName=Configure::read("site_name");
											   $email =$mail['Email']['info_email'];//"info@pinpaws.com";
											   $Email->from(array($email=>"Pin Paws"));
											   $Email->to($mymail);
											   $Email->subject($mail['Email']['forgot_password']);
											   $Email->send($massage);
											   $this->Session->setFlash(__("Please check your mail for Password Details."));
					
				}	                    
					
			}
			else{
			    //$this->Session->setFlash('Please Enter The Valid Email Id.','default',array(),'failure');
				$this->redirect(Configure::read('URL').'/registers/forget_password');
				$this->Session->setFlash(__("Sorry You Are Not a Registered User"));				
			}
		}
	 }

///////////////////////////////////////////////////
	public function adminforget_password(){
		$this->layout ='site_login';
		if($this->request->is("post")){
			$check_email=$this->request->data['email'];
	    	$exit_email=$this->Operator->findByEmail($check_email);
			if(!empty($exit_email)){
				$name=$exit_email['Operator']['name'];
				$user_email=$exit_email['Operator']['email'];
				$password=$exit_email['Operator']['hint'];
				$email_send=$check_email;
				if(isset($this->request->data['email']) && !empty($this->request->data['email'])){
                   	$massage = '';
				   	$massage.='<strong>Dear Admin</strong>,<br/>';
				   	$massage.='<strong>'."Your Password is:".'</strong>';
				   	$massage.='<strong>'.$password.'</strong><br/>';
                  	$massage.='<br/> Thanks, <br/>info@pinpaws.com';
                   	$mymail =$user_email;
                   	$mail=$this->Email->find('first');
				   	$Email = new CakeEmail();
				   	$Email->emailFormat('html');
				   	$siteName=Configure::read("site_name");
				   	$email =$mail['Email']['info_email'];//"info@pinpaws.com";
				   	$Email->from(array($email=>$siteName));
				   	$Email->to($mymail);
				   	$Email->subject($mail['Email']['forgot_password']); //subject("Forget Password mail ".$siteName);
				   	$Email->send($massage);
			   		$this->Session->setFlash('Please check your mail for Password Details.','default',array(),'success');
					$this->redirect(Configure::read('URL').'/admin/operators/login');
				}	
			}
			else{
			    $this->Session->setFlash('Please Enter The Valid Email Id.','default',array(),'failure');
				 $this->redirect(Configure::read('URL').'/admin/operators/login');
			}
		}	
 	}

 /////////////////////////////////////////////////////

	public function login_user(){
			
		$this->set('title_for_layout',"login");
		$this->set('description_for_layout',"login");
		$this->set('keywords_for_layout',"login");
		 
		if($this->request->is("post"))
		{			
			if(isset($this->request->data['registerbtn']) && !empty($this->request->data['registerbtn'])) 
			{
				$this->request->data["Register"]["mailrand"]=$this->random(12);
				$this->request->data["Register"]["smsrand"]=$this->random(5);
				if($this->request->data["user_type1"]=='user'){
					$this->request->data["Register"]["user_type"]="U";
					$this->request->data["Register"]["status"]="1";
				}else{
     				$this->request->data["Register"]["user_type"]="S";
					$this->request->data["Register"]["status"]="0";
				}
				$name = $this->request->data["Register11"]["first_name"]." ".$this->request->data["Register11"]["last_name"];
				$email_send = $this->request->data["Register11"]["email"];
				$this->request->data["Register"]["name"]=$name;
				$this->request->data["Register"]["email"]=$this->request->data["Register11"]["email"];
				$this->request->data["Register"]["password"]=$this->request->data["Register11"]["password"];
				$this->request->data["Register"]["hint"]=$this->request->data["Register11"]["password"];
				$this->request->data["Register"]["account_no"]="55667788".$this->random(5).date("s")."1";
								 
				$this->Register->create();
				if($this->Register->save($this->request->data))
				{
									
				   	$massage = '';
				    $massage.='Hello <strong>'.$name.'</strong>,<br/>';
                   	$massage.='We have created your account. Please wait for approval. After approving, you will receive an email with user id and password';
                   	$massage.='<br/> Thanks, <br/> Pinpaws.com';
				  	
				   	$mymail = $email_send;
				   	$Email = new CakeEmail();
				   	$Email->emailFormat('html');
				   	$siteName=Configure::read("site_name");

				   	$mail=$this->Email->find('first');//********************

				   	$email = $mail['Email']['admin_email'];//"admin@demostock.co.in";
				   	$Email->from(array($email=>$siteName));
				   	$Email->to($mymail);
				   	$Email->subject($mail['Email']['email_verification']/*"Email verification mail from ".$siteName*/);
				   	$Email->send($massage);
				   	$this->Session->setFlash(__("Registration successful please login with your details"));											
				}
			}else{
									
				$this->request->data["Register"]["email"]=$this->request->data["Register"]["email_log"];
				$this->request->data["Register"]["password"]=$this->request->data["Register"]["password_log"];
				if($this->Auth->login())
				{
					if($this->request->data["user_type"]=='user'){
						$u_tupe="U";
					}else{
     					$u_tupe="S";
					}
					$currentUser = $this->Auth->user();
                    $type = $currentUser['user_type'];
											
					$email_verified = $currentUser['email_verified'];
					if($email_verified==1){
						if($type==$u_tupe){
							if($type=='S'){
								$this->redirect(array("controller"=>"registers","action"=>"view",$currentUser['id'])); 
							}else{
								$this->redirect(array("controller"=>"registers","action"=>"view_user",$currentUser['id'])); 	
							}
						}
						else{
							unset($order_details);
							unset($cart_details);
							$this->Session->delete("userLogin");
							$this->Session->setFlash(__("Invalid Username/Password."));
							$this->redirect($this->Auth->logout());
												   
						}
					}else{
						$this->redirect(array('controller'=>'pages','action' => 'product'));						 
					}   										  
				}else
				{
					$this->Session->setFlash(__("Invalid Username/Password."));
				}									
			}			
		}
		$page_cms = $this->Page->find('first', array(
            'conditions' => array(
                'Page.slug' => "login-cms"
            )
        ));
       
		$this->set('page_cms',$page_cms);		
	}

///////////////////////////////////////////////////////////

	public function admin_view($id=NULL)
    {

        if( filter_var($id, FILTER_VALIDATE_INT) === false ){
        $datasb="";}else{
        $datasb=$this->Register->findById($id);}
        $this->set(compact('datasb'));


        $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
        	if($id==NULL)
        	{
            	throw new NotFoundException(__("Invalid id"));
        	}
			$datas=$this->Register->find('first', array(
         		'recursive' => -1,
		 		'contain' => array(
	   				'PetProfile'
	   			),
	    		'conditions' => array(
                	'Register.id' =>$id
            	),
      			'order' => array(
                	'Register.id' => 'DESC'
            	)
        	));
	        if(!$datas)
	        {
	            throw new NotFoundException(__("Invalid id"));
	        }
        	$this->set("datas",$datas);
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
    }

/////////////////////////////////////////////////

	public function admin_retail_view($id=NULL)
    {
        $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
	        if($id==NULL)
	        {
	            throw new NotFoundException(__("Invalid id"));
	        }
			$datas=$this->Register->find('first', array(
         		'recursive' => -1,
	 			'contain' => array(
	   				'PetProfile'   
	   			),
	    		'conditions' => array(
                	'Register.id' =>$id
            	),
      			'order' => array(
                	'Register.id' => 'DESC'
            	)
        	));
	        if(!$datas)
	        {
	            throw new NotFoundException(__("Invalid id"));
	        }
	        $this->set("datas",$datas);
		}
		else{
				$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
    }

/////////////////////////////////////////////////////////

	public function admin_retail_nonsubscriber_view($id=NULL)
    {

    if( filter_var($id, FILTER_VALIDATE_INT) === false ){
        $datasb="";}else{
        $datasb=$this->Register->findById($id);}
        $this->set(compact('datasb'));


        $currentUser = $this->Auth->user();
		if(!empty($currentUser)){


	        // if($id==NULL)  
	        // {
	        //     throw new NotFoundException(__("Invalid id"));
	        // }
			$datas=$this->Register->find('first', array(
         		'recursive' => -1,
		 		'contain' => array(
	   				'PetProfile'	   
	   			),
	    		'conditions' => array(
                	'Register.id' =>$id
            	),
      			'order' => array(
                	'Register.id' => 'DESC'
            	)
        	));
	        // if(!$datas)
	        // {
	        //     throw new NotFoundException(__("Invalid id"));
	        // }
        	$this->set("datas",$datas);
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
    }

/////////////////////////////////////////////////////////

	public function admin_retail_subscriber_view($id=NULL)
    {

       if( filter_var($id, FILTER_VALIDATE_INT) === false ){
        $datasb="";}else{
        $datasb=$this->Register->findById($id);}
        $this->set(compact('datasb'));



        $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
	        // if($id==NULL)
	        // {
	        //     throw new NotFoundException(__("Invalid id"));
	        // }
			$datas=$this->Register->find('first', array(
         		'recursive' => -1,
		 		'contain' => array(
	   				'PetProfile'	   
	   			),
	    		'conditions' => array(
                	'Register.id' =>$id
            	),
      			'order' => array(
                	'Register.id' => 'DESC'
            	)
        	));
	        // if(!$datas)
	        // {
	        //     throw new NotFoundException(__("Invalid id"));
	        // }
        	$this->set("datas",$datas);
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
    }

////////////////////////////////////////////////////////

	public function admin_retail_edit($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid id"));
			}
			$datas=$this->Register->findById($id);
			if(!$datas)
			{
				throw new NotFoundException(__("Invalid id"));
			}
			$customer_datas=$this->Register->findById($id);
			$this->set('customer_datas',$customer_datas);
			if($this->request->is("post") or $this->request->is("put"))
			{        
                $this->request->data['Register']['id']=$id;
                $this->request->data["Register"]["fname"]=$this->request->data["fname"];
				$this->request->data["Register"]["lname"]=$this->request->data["lname"];
				$this->request->data["Register"]["email"]=$this->request->data["email"];
				$this->request->data["Register"]["phone_no"]=$this->request->data["phone_no"];
				$this->request->data["Register"]["mobile_phone"]=$this->request->data["mobile_phone"];
				$this->request->data["Register"]["address"]=$this->request->data["locationTextField"];
				if($this->Register->save($this->request->data))
				{
			$this->Session->setFlash(__("Data updated successfully"));
					$this->redirect(array("action"=>"admin_retail_index"));

				}
				else
				{
					$this->Session->setFlash(__("Database error"));
				}			 
			}
		  	$datas["Register"]["dob"]=substr($datas["Register"]["dob"],0,10);
			$this->request->data=$datas;
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

/////////////////////////////////////////////////////////

	public function admin_retail_nonsubscriber_edit($id=null)
	{

        if( filter_var($id, FILTER_VALIDATE_INT) === false ){
        $datasb="";}else{
        $datasb=$this->Register->findById($id);}
        $this->set(compact('datasb'));

		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			// if($id==NULL)
			// {
			// 	throw new NotFoundException(__("Invalid id"));
			// }
			$datas=$this->Register->findById($id);
			// if(!$datas)
			// {
			// 	throw new NotFoundException(__("Invalid id"));
			// }
			$customer_datas=$this->Register->findById($id);
			$this->set('customer_datas',$customer_datas);
			if($this->request->is("post") or $this->request->is("put"))
			{        
                $this->request->data['Register']['id']=$id;
                $this->request->data["Register"]["fname"]=$this->request->data["fname"];
				$this->request->data["Register"]["lname"]=$this->request->data["lname"];
				$this->request->data["Register"]["email"]=$this->request->data["email"];
				$this->request->data["Register"]["phone_no"]=$this->request->data["phone_no"];
				$this->request->data["Register"]["mobile_phone"]=$this->request->data["mobile_phone"];
				$this->request->data["Register"]["address"]=$this->request->data["locationTextField"];
				if($this->Register->save($this->request->data))
				{
					// $this->Session->setFlash(__("Data Updated successfully"));
					$this->Session->setFlash(__("Data has been updated successfully", null), 
                            'default', 
                             array('class' => 'alert data_succesfull_message'));					
					$this->redirect(array("action"=>"admin_retail_nonsubscribers"));
				}
				else
				{
					// $this->Session->setFlash(__("Database error"));
					$this->Session->setFlash(__("Database error", null), 
                            'default', 
                             array('class' => 'alert alertt'));
				}			 
			}
		  	$datas["Register"]["dob"]=substr($datas["Register"]["dob"],0,10);
			$this->request->data=$datas;
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////////

	public function admin_retail_subscriber_edit($id=NULL)
	{ 
        if( filter_var($id, FILTER_VALIDATE_INT) === false ){
        $datasb="";}else{
        $datasb=$this->Register->findById($id);}
        $this->set(compact('datasb'));
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			// if($id==NULL)
			// {
			// 	throw new NotFoundException(__("Invalid id"));
			// }
			// $datas=$this->Register->findById($id);
			// if(!$datas)
			// {
			// 	throw new NotFoundException(__("Invalid id"));
			// }
			$customer_datas=$this->Register->findById($id);
			$this->set('customer_datas',$customer_datas);
			if($this->request->is("post") or $this->request->is("put"))
			{        
			 	
                $this->request->data['Register']['id']=$id;
                $this->request->data["Register"]["fname"]=$this->request->data["fname"];
				$this->request->data["Register"]["lname"]=$this->request->data["lname"];
				$this->request->data["Register"]["email"]=$this->request->data["email"];
				$this->request->data["Register"]["phone_no"]=$this->request->data["phone_no"];
				$this->request->data["Register"]["mobile_phone"]=$this->request->data["mobile_phone"];
				$this->request->data["Register"]["address"]=$this->request->data["locationTextField"];
				if($this->Register->save($this->request->data))
				{
					$this->Session->setFlash(__("Data has been updated successfully", null), 
                            'default', 
                             array('class' => 'alert data_succesfull_message'));
					$this->redirect(array("action"=>"admin_retail_subscribers"));
				}
				else
				{
					$this->Session->setFlash(__("Database error"));
				}		 
			}
		  	$datas["Register"]["dob"]=substr($datas["Register"]["dob"],0,10);
			$this->request->data=$datas;
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

/////////////////////////////////////////////////////////

	public function admin_retail_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->Register->findById($id);
			 
				if($this->Register->delete($id))
				{
				 
				$this->Session->setFlash(__("Data deleted successfully", null), 
                            'default', 
                             array('class' => 'alert-success data_succesfull_message'));
				}
				else
				{
					$this->Session->setFlash(__("Delete unsuccessful."));
				}
			
			    $this->redirect(array("action"=>"admin_retail_index"));				   
			}
			$this->redirect(array("action"=>"admin_retail_index"));
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////

	public function admin_retail_nonsubscriber_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->Register->findById($id);
			 
				if($this->Register->delete($id))
				{	
					$this->Session->setFlash(__("Data deleted successfully", null), 
	                            'default', 
	                             array('class' => 'alert data_succesfull_message'));			
				}
				else
				{
					$this->Session->setFlash(__("Delete unsuccessful."));
				}
				$this->redirect(array("action"=>"admin_retail_nonsubscribers"));	   
			}
			$this->redirect(array("action"=>"admin_retail_nonsubscribers"));
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

/////////////////////////////////////////////////////

	public function admin_retail_subscriber_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->Register->findById($id);
				if($this->Register->delete($id))
				{	
					$order=$this->Order->find('all',array(
					'conditions'=>array(
						'or'=>array('Order.user_id'=>$data['Register']['id'])),
					));
					$subscription=array();
					$i=0;
					foreach ($order as $value) {
						if(!empty($value['Order']['plan_id'])){
							$subscription[$i++]=$value['Order']['plan_id'];
						}
					}
					foreach ($subscription as $value) {
					$status=$this->Stripe->SubscriptionCancel($value);
					}
					$this->Session->setFlash(__("Data deleted successfully",h($id)));				
				}
				else
				{
					$this->Session->setFlash(__("Delete unsuccessful."));
				}
			    $this->redirect(array("action"=>"admin_retail_subscribers"));	   
			}
			$this->redirect(array("action"=>"admin_retail_subscribers"));
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

///////////////////////////////////////////////////////

	public function admin_retail_status($id=NULL)
	{  
	   	$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
				$s=$this->Register->findById($id,array("fields"=>"status","name","email","hint"));
				if($s)
				{
					if($s["Register"]["status"]==1){ 
						$ns=0; 
					}
					elseif($s["Register"]["status"]==0){ 
						$ns=1; 
					}
					$this->Register->id=$id;
					if($this->Register->saveField("status",$ns))
					{
						$this->Session->setFlash(__("Status has been changed successfully"));
					}
					else
					{
						$this->Session->setFlash(__("Database error"));
					}
				}	
				else
				{
					$this->Session->setFlash(__("Invalid Id"));
				}
				$this->redirect(array("action"=>"retail_index"));
		 	}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////////

	public function admin_retail_nonsubscriber_status($id=NULL)
	{  
	   $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
				$s=$this->Register->findById($id,array("fields"=>"status","name","email","hint"));
				if($s)
				{
					if($s["Register"]["status"]==1){ 
						$ns=0; 
					}
					elseif($s["Register"]["status"]==0){ 
						$ns=1; 
					}
					$this->Register->id=$id;
					if($this->Register->saveField("status",$ns))
					{
						// $this->Session->setFlash(__("Status change successful"));

						 

						$this->Session->setFlash(__("Status has been change successfully", null), 
                            'default', 
                             array('class' => 'alert data_succesfull_message'));
					}
					else
					{
						// $this->Session->setFlash(__("Database error"));
						$this->Session->setFlash(__('Status change failed', null),
	                            'default',
	                             array('class' => 'alert alertt'));
					}
				}	
				else
				{
					// $this->Session->setFlash(__("Invalid Id"));
					$this->Session->setFlash(__('Invalid Id', null),
	                            'default',
	                             array('class' => 'alert alertt'));
				}
				$this->redirect(array("action"=>"retail_nonsubscribers"));
		 	}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////////

	public function admin_retail_subscriber_status($id=NULL)
	{ 
	    $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
				$s=$this->Register->findById($id,array("fields"=>"status","name","email","hint"));
				if($s)
				{
					if($s["Register"]["status"]==1){ 
						$ns=0;
					}
					elseif($s["Register"]["status"]==0){ 
						$ns=1; 
					}
					$this->Register->id=$id;
					if($this->Register->saveField("status",$ns))
					{
						// $this->Session->setFlash(__("Status change successful"));

						$this->Session->setFlash(__("Status has been changed successfully", null), 
                            'default', 
                             array('class' => 'alert data_succesfull_message'));
					}
					else
					{
						// $this->Session->setFlash(__("Database error"));
						$this->Session->setFlash(__('Status change failed', null),
	                            'default',
	                             array('class' => 'alert alertt'));
					}
				}	
				else
				{
					// $this->Session->setFlash(__("Invalid Id"));
					$this->Session->setFlash(__('Invalid Id', null),
	                            'default',
	                             array('class' => 'alert alertt'));
				}
				$this->redirect(array("action"=>"retail_subscribers"));
		 	}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}	

////////////////////////////////////////////////////////

	public function imageUpload_multiple($data,$field,$i,$j)
	{
		$support_file=array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png");
		
		if(!empty($data[$field]["name"]))
		{
			if($data[$field]["error"]==0)
			{
				if(in_array($data[$field]["type"],$support_file))
				{
					$image= time().$i.$j.$data[$field]["name"];
					if(!move_uploaded_file($data[$field]["tmp_name"],"images/original/".$image))
					{
						$this->Session->setFlash(__("Image upload failed"));
						return false;
					}
					else
					{
						//create temp image
						copy("images/original/".$image,"images/large/".$image);
						$this->resize("images/large/".$image, 200, 200);
						copy("images/original/".$image,"images/small/".$image);
						$this->resize("images/small/".$image, 50, 50);								
						return $image;
					}
				}
				else
				{
					$this->Session->setFlash(__("Image file is not valid "));
					return false;
				}
			}
			else
			{
				$this->Session->setFlash(__("Image file error."));
				return false;
			}				
		}
		else
		{
			return false;
		}
	}

////////////////////////////////////////////////////

	public function view($id=NULL)
	{
		$this->layout = 'saller';
		if($id==NULL)
		{
			throw new NotFoundException(__("Invalid id"));
		}
		$currentUser = $this->Auth->user();
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		if($user_id==$id){
			if(!$datas)
			{
				throw new NotFoundException(__("Invalid id"));
			}
			$datas["Operator"]["password"]=$datas["Register"]["hint"];
			$this->request->data=$datas;
			$this->set('datas',$datas);			
	   	}else{
		    throw new NotFoundException(__("Invalid id"));		   
		}		 
	}

////////////////////////////////////////////////////

	public function update_view()
	{
		if($this->request->is("post"))
		{	       
		    $id = $this->request->data["user_id"];
			$this->request->data["Register"]["name"]=$this->request->data["username"];
			$this->request->data["Register"]["email"]=$this->request->data["email"];					
			if(!empty($this->request->data["phone_no"])){  
				$this->request->data["Register"]["phone_no"]=$this->request->data["phone_no"];
			}
			else{
				$this->request->data["Register"]["phone_no"]='xxxxxxxxxx';
			}
			$this->request->data["Register"]["hint"]=$this->request->data["password"];						  
			$this->request->data["Register"]["password"]=$this->request->data["password"];
			if(!empty($this->request->data["hear_about"])){
				$this->request->data["Register"]["weknow_about"]=$this->request->data["hear_about"];
			}
			if(!empty($this->request->data["online_exp"])){
				$this->request->data["Register"]["online_experience"]=$this->request->data["online_exp"];
			}
							 
			$this->Register->id=$id;
			if($this->Register->save($this->request->data))
			{
				$this->Session->setFlash(__("Details updated successfully..."));
				$this->redirect(array("controller"=>"registers","action"=>"view/$id"));
			}
			else
			{
				$this->Session->setFlash(__("Please try again"));
			}						 
		}
	}

//////////////////////////////////////////////////////
	
  public function update_business_info()
	{
		if($this->request->is("post"))
		{	       
			$flag=0;
		 	if($this->request->form["picimg6"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picimg6");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["logo_img"];
					$this->request->data["logo_img"]=$image;
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picimg6"]);
				$flag=1;
			}
			$flag=0;
		 	if($this->request->form["picimg5"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picimg5");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["doc_first_lincense"];
					$this->request->data["doc_first_lincense"]=$image;
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picimg5"]);
				$flag=1;
			}
			$flag=0;
		 	if($this->request->form["picimg4"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picimg4");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["doc_current_lincense"];
					$this->request->data["doc_current_lincense"]=$image;
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picimg4"]);
				$flag=1;
			}
			$flag=0;
		 	if($this->request->form["picimg3"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picimg3");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["servicetax_reg_img"];
					$this->request->data["servicetax_reg_img"]=$image;
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picimg3"]);
				$flag=1;
			}
			$flag=0;
		 	if($this->request->form["picimg2"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picimg2");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["vat_reg_img"];
					$this->request->data["vat_reg_img"]=$image;
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picimg2"]);
				$flag=1;
			}
			$flag=0;
		 	if($this->request->form["picimg1"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picimg1");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["pan_img"];
					$this->request->data["pan_img"]=$image;
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picimg1"]);
				$flag=1;
			}
		   
			$id = $this->request->data["user_id"];
			$this->request->data["Register"] = $this->request->data;
			$this->Register->id=$id;
			if($this->Register->save($this->request->data))
			{
				$this->Session->setFlash(__("Details updated successfully..."));
				$this->redirect(array("controller"=>"registers","action"=>"view/$id"));
			}
			else
			{
				$this->Session->setFlash(__("Please try again"));
			}
						 
		}
	}

////////////////////////////////////////////////////

  	public function update_personal_view()
	{
		if($this->request->is("post"))
		{	       		
			$flag=0;
		 	if($this->request->form["picture_upload"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picture_upload");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["founder_img"];
					$this->request->data["founder_img"]=$image;
					
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picture_upload"]);
				$flag=1;
			}
			$flag=0;
		 	if($this->request->form["picture_upload2"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picture_upload2");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["sales_rep_img"];
					$this->request->data["sales_rep_img"]=$image;
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picture_upload2"]);
				$flag=1;
			}
		    $flag=0;
		 	if($this->request->form["picture_upload3"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picture_upload3");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["cc_rep_img"];
					$this->request->data["cc_rep_img"]=$image;
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				$flag=1;
			}
		   
			$id = $this->request->data["user_id"];
			$this->request->data["Register"] = $this->request->data;
			$this->Register->id=$id;
		 	if($this->Register->save($this->request->data))
		 	{
		 		$this->Session->setFlash(__("Details updated successfully..."));
				$this->redirect(array("controller"=>"registers","action"=>"view/$id"));
		 	}
			else
			{
				$this->Session->setFlash(__("Please try again"));
			}						 
		}
	}

///////////////////////////////////////////////////

	public function update_bank_view()
	{ 
		if($this->request->is("post"))
		{	       
		
			$flag=0;
		 	if($this->request->form["cancelled_cheque_img1"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"cancelled_cheque_img1");
		  		if($image)
				{
					$flag=1;
					$this->request->data["cancelled_cheque_img"]=$image;
					$preimage=$this->request->data["cancelled_cheque_img_old"];
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["cancelled_cheque_img1"]);
				$this->request->data["cancelled_cheque_img"]=$this->request->data["cancelled_cheque_img_old"];
				$flag=1;
			}
		
		   
			$id = $this->request->data["user_id"];
			$this->request->data["Register"] = $this->request->data;
			$this->Register->id=$id;
			if($this->Register->save($this->request->data))
			{
				$this->Session->setFlash(__("Details updated successfully..."));
				$this->redirect(array("controller"=>"registers","action"=>"view/$id"));
			}
			else
			{
				$this->Session->setFlash(__("Please try again"));
			}					 
		}
	}

//////////////////////////////////////////////////////
	//------------------------ date: 21/12/2015

	public function edit_profile_pic()
	{ 
		if($this->request->is("post"))
		{	       
			 $id = $this->request->data["user_id_for_pic"];
			 if($this->request->form["picture_upload_founder"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picture_upload_founder");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["founder_img"];
					$this->request->data["founder_img"]=$image;
					
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picture_upload_founder"]);
				$flag=1;
			}
							 
			$this->Register->id=$id;
			if($this->Register->save($this->request->data))
			{
			 	$this->Session->setFlash(__("Details updated successfully..."));
				$this->redirect(array("controller"=>"registers","action"=>"view/$id"));
			}
			else
			{
				$this->Session->setFlash(__("Please try again"));
			}						 
		}
	}
	
/////////////////////////////////////////////////////

	public function logout()
	{
		$currentUser=$this->Auth->user();
		$this->Register->id=$currentUser['id'];
		$this->request->data["Register"]["is_login"]=0;
		$this->Register->save($this->request->data);
		$activity_name="logout";
		$activity_date=date("Y-m-d H:i:s");;
		$this->user_activity_log($activity_name,$activity_date);
		$this->Session->delete("userLogin");
		$this->Session->delete("browser_session");
		unset($order_details);		
		unset($cart_details);
		$this->redirect($this->Auth->logout());
	}

//////////////////////////////////////////////////////

	 public function setting()
	 {
		$this->set('title_for_layout',"Account Setting");
		$this->set('description_for_layout',"Account Setting");
		$this->set('keywords_for_layout',"Account Setting");	
		
		$datas=$this->Auth->user();	
		$id=$datas["id"];
		if($id)
		 {
		 	$this->set("registerData",$datas);
			if($this->request->is("post"))
			{
				$old=$this->request->data["Register"]["oldpassword"];
				$new=$this->request->data["Register"]["newpassword"];
				$cnew=$this->request->data["Register"]["cnewpassword"];
				if($datas["hint"]===$old)
				{
					if($new===$cnew)
					{
							 
						$this->request->data["Register"]["password"]=$new;
						$this->request->data["Register"]["hint"]=$new;
						$this->Register->id=$id;
						if($this->Register->save($this->request->data))
						{
						 	$this->Session->setFlash(__("Your  password updated successfully..."));
						}
						else
						{
							$this->Session->setFlash(__("Please try again"));
						}
					}
					else
					{
						$this->Session->setFlash(__("Your confirm password did't match..."));
					}
				}
				else
				{
					$this->Session->setFlash(__("Your old password did't match..."));
				}
			}				 
		}
		else
		{
			$this->Session->setFlash(__("Please login again."));
			$this->redirect(array("controller"=>"registers","action"=>"logout"));
		}				 
	}
	 
//////////////////////////////////////////////////

	public function register()
	{
		$this->layout = 'site_login';
		if($this->request->is("get"))
		{
			$this->Captcha->__init();
			$p=$this->Captcha->getFirst();
			$q=$this->Captcha->getSecond();
			$this->set('p',$p);
			$this->set('q',$q);
			$r=$p+$q;
			$this->Session->write("Register.r",$r);
			$this->Session->write("Register.p",$p);
			$this->Session->write("Register.q",$q);
		}
		else
		{
		 	$this->set('p',$this->Session->read("Register.p"));
		 	$this->set('q',$this->Session->read("Register.q"));
		}			
		if($this->request->is("post"))
		{
			$r=$this->Session->read("Register.r");
			$this->request->data["Register"]["mailrand"]=$this->random(12);
			$this->request->data["Register"]["smsrand"]=$this->random(5);
			$this->request->data["Register"]["hint"]=$this->request->data["password"];
			$this->request->data["Register"]["email"]=$this->request->data["email"];
			$this->request->data["Register"]["password"]=$this->request->data["password"];
			
			$this->request->data["Register"]["fname"]=$this->request->data["fname"];
			$this->request->data["Register"]["lname"]=$this->request->data["lname"];
			$this->request->data["Register"]["phone_no"]=$this->request->data["phone_no"];
			$this->request->data["Register"]["mobile_phone"]=$this->request->data["mobile_phone"];
			$this->request->data["Register"]["address"]= $this->request->data["locationTextField"];
			$this->request->data["Register"]["city"]=$this->request->data["city"];
			$this->request->data["Register"]["state"]=$this->request->data["state"];
			$this->request->data["Register"]["postal_code"]=$this->request->data["postal_code"];
			$this->request->data["Register"]["country"]=$this->request->data["country"];
			$this->request->data["Register"]["status"]=1;
			 
			$this->Register->create();
			if($this->Register->save($this->request->data['Register']))
			{
				$this->Session->delete("Register.r");
				$this->Session->delete("Register.p");
				$this->Session->delete("Register.q");
				$id=$this->Register->getLastInsertId();
				$this->Session->write("Register.id",$id);
				$this->Session->setFlash(__("Registration successful please login with your details"));		
				$this->redirect(array("controller"=>"pages","action"=>"product"));
			}
			else
			{
				$this->Session->setFlash(__("<span style='color:RED'>Registration Unsuccessful .Please try again</span>"));
				$this->redirect(array("controller"=>"registers","action"=>"register"));
			}				
		}
	}

///////////////////////////////////////////////////

	public function mailverify()
	{
		$this->set("description_for_layout","Email verification");
		$this->set("keywords_for_layout","Email verification");
		
		$id=$this->Session->read("Register.id");
		$datas=$this->Register->findById($id);
		$massage='Email verification required. Please click on this <a href="'.Router::url('/', true).'registers/mailveried/?email='.$datas["Register"]["email"].'&mailrand='.$datas["Register"]["mailrand"].'" target="_blank">link</a> for email verification';
		$this->set("email",$datas["Register"]["email"]);

		$mail=$this->Email->find('first');//*******************
		$Email = new CakeEmail();
		$Email->emailFormat('html');
		$siteName=Configure::read("site_name");
		$email=Configure::read('email');
		$Email->from(array($email=>$siteName));
		$Email->to($datas["Register"]["email"]);
		$Email->subject($mail['Email']['email_verification']);/*"Email verification mail from ".$siteName*/
		$Email->send($massage);
	}

//////////////////////////////////////////////////

	public function mailveried()
	{
		$this->set("description_for_layout","Email verified");
		$this->set("keywords_for_layout","Email verified");
		$urlData=$this->params["url"];
		$email=$urlData["email"];
		$mailrand=$urlData["mailrand"];
		$sendSms=0;
		$message="";
	
		$datas=$this->Register->find("first",array("conditions"=>array("email"=>$email,"mailrand"=>$mailrand)));
		if($datas)
		{
			$email_verified=$datas["Register"]["email_verified"];
			$sms_verified=$datas["Register"]["sms_verified"];
			if($email_verified==1 and  $sms_verified==1)
			{
				$msg="Your email address and mobile number are already verified.<br> You can now <a href='".Router::url('/', true)."registers/login'>login</a>";
			}
			elseif($email_verified==1 and  $sms_verified==0)
			{
				$sendSms=1;
				$msg="Your email is verified <br>Now please check your mobile (".$datas["Register"]["phone_no"].") message box.<br>";
				 
			}
			elseif($email_verified==0 and  $sms_verified==0)
			{
				$sendSms=1;
				$this->Register->id=$datas["Register"]["id"];
				$this->Register->saveField("email_verified",1);
				$msg="Your email is verified successfully <br>Now please check your mobile(".$datas["Register"]["phone_no"].") message box.<br>";
				
			}
			elseif($email_verified==0 and  $sms_verified==1)
			{
				$sendSms=0;
				$this->Register->id=$datas["Register"]["id"];
				$this->Register->saveField("email_verified",1);
				$msg="your email is verified successfully <br>Mobile number is verified<br>";
				
			}
			//sms
			if($sendSms==1)
			{
				$sms="Your mobile verification code:".$datas["Register"]["smsrand"];
				if(!$this->request->is("post")){ echo $sms; }			 
			}
			$this->set("msg",$msg);		
		}
		else
		{
			$this->set("msg","Invalid verification link. Please try again..");
		}
		if($this->request->is("post"))
		{
			if($datas["Register"]["smsrand"]==$this->request->data["Register"]["smsrnd"])
			{
				$this->Register->id=$datas["Register"]["id"];
				$this->Register->saveField("sms_verified",1);
				$this->Register->saveField("status",1);

				$this->Session->write("Register.id",$datas["Register"]["id"]);
				$this->redirect(array("controller"=>"registers","action"=>"smsverified"));
			}
			else
			{
				$message="Your mobile verification code is not valid please try again";
				
			}
		}
		$this->set("sendSms",$sendSms);
		$this->set("message",$message);
	}

//////////////////////////////////////////////////

	public function smsverified()
	{
		$this->set("description_for_layout","Sms verified");
		$this->set("keywords_for_layout","Sms verified");
		$templateData=$this->Page->findById(Configure::read('template_id'));
		$message=$templateData["Page"]["description"];
		$name=$templateData["Page"]["name"];
		
		$id=$this->Session->read("Register.id");
		$regData=$this->Register->findById($id);
	 		 
		$message=str_replace("{account_no}",$regData["Register"]["account_no"],$message);
		$message=str_replace("{password}",$regData["Register"]["hint"],$message);
		$message=str_replace("{name}",$regData["Register"]["name"],$message);
		$message=str_replace("{email}",$regData["Register"]["email"],$message);
		$message=str_replace("{phone_no}",$regData["Register"]["phone_no"],$message);
		$message=str_replace("{dob}",$regData["Register"]["dob"],$message);
		$message=str_replace("{address}",$regData["Register"]["address"],$message);
		$message=str_replace("{city}",$regData["Register"]["city"],$message);
		$message=str_replace("{country}",$regData["Register"]["country"],$message);
		$message=str_replace("{postal_code}",$regData["Register"]["postal_code"],$message);
		$message=str_replace("{created}",$regData["Register"]["created"],$message);
		$message=str_replace("{modified}",$regData["Register"]["modified"],$message);
	 
		$Email = new CakeEmail();
		$Email->emailFormat('html');

		$Email->from(array(Configure::read('email')=>Configure::read("site_name")));;
		
		$Email->to($regData["Register"]["email"]);
		$Email->subject("$name".Configure::read("site_name"));
		$Email->send($message);
		
	}

///////////////////////////////////////////////////
	
	public function admin_index()
	{
		ini_set('memory_limit', '-1');
		$this->layout='default';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$datas=$this->Register->find('all',array(
		 				'contain'=>array('PetProfile'=>array('Product'=>array('conditions'=>array(
		     			// 'Product.is_retail' => 0,
		     			'Product.isdeleted' => 0)))),
		 				'conditions'=>array(/*'Register.id'=>376*/),
		 				'order' => array(
	                	'Register.id' => 'DESC'
	            			),
		 			));
		 			$data2=array();
		 			$pos=0;
		 			foreach ($datas as $data) {
		 					$pos2=0;
		 				foreach ($data['PetProfile'] as $pet) {
		 					if($pet['Product']['is_retail']==0){
		 						if($data['Register']['id']!=$data2[$pos-1]['Register']['id'])
		 						{
		 							$data2[$pos]['Register']=$data['Register'];
		 							$insert=$pos;
		 						}
		 						$data2[$insert]['PetProfile'][$pos2]=$pet;
		 						$pos2++;
		 						
		 					}
		 				}
		 				$pos++;
		 			}
		 			$datas=$data2;
		  	if($this->request->is("post") or $this->request->is("put")){
		  	    $post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];
				$start_date=date('Y-m-d H:i:s', strtotime( $this->request->data['start_date'] ));
				$end_to_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));
				$end_date = $end_to_date.' 23:59:59';
				// $datas=$this->Register->find('all', array(
				// 	'recursive' => -1,
				// 	'contain' => array(
				// 		'PetProfile' => array('Product'=>array('conditions'=>array('Product.is_retail' => 0,'Product.isdeleted' => 0)))	   
				// 	),
				// 	'conditions' => array(
				// 		'and' => array(
				// 			// 'Register.created BETWEEN ? and ?' => array($start_date, $end_date),
				// 			array('Register.created >= ' => $start_date,
				// 		  		'Register.created <= ' => $end_date
				// 		  	),					
				// 			//'Register.email_verified' => 1
				// 		)
				// 	),
				// 	'order' => array(
				// 		'Register.id' => 'DESC'
				// 	)
				// ));
				$datas=$this->Register->find('all',array(
		 				'contain'=>array('PetProfile'=>array('Product'=>array('conditions'=>array(
		     			// 'Product.is_retail' => 0,
		     			'Product.isdeleted' => 0)))),
		 				'conditions'=>array(
		 					'and' => array(
							'Register.created BETWEEN ? and ?' => array($start_date, $end_date),
		 				)),
		 				'order' => array(
	                	'Register.id' => 'DESC'
	            			),
		 			));
		 			$data2=array();
		 			$pos=0;
		 			foreach ($datas as $data) {
		 					$pos2=0;
		 				foreach ($data['PetProfile'] as $pet) {
		 					if($pet['Product']['is_retail']==0){
		 						if($data['Register']['id']!=$data2[$pos-1]['Register']['id'])
		 						{
		 							$data2[$pos]['Register']=$data['Register'];
		 							$insert=$pos;
		 						}
		 						$data2[$insert]['PetProfile'][$pos2]=$pet;
		 						$pos2++;
		 						
		 					}
		 				}
		 				$pos++;
		 			}
		 			$datas=$data2;
			}						
		   	$this->set(compact('datas'));
		   	$this->set(compact('post_start_date','post_end_date'));
		}
       	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 	   
	}

//////////////////////////////////////////////////

	public function admin_retail_index()
	{   ini_set('memory_limit', '-1');
		$this->layout='default';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
		 	$datas=$this->Register->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
	     			'PetProfile' => array('Product'=>array('conditions'=>array('Product.is_retail' => 1,'Product.isdeleted' => 0)))	   
	    		),
	   			'conditions'=>array(
                	'Register.email_verified' => 1
            	),
       			'order' => array(
                	'Register.id' => 'DESC'
            	)
        	));
			$this->set(compact('datas'));
		}
       	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 	   
	}

///////////////////////////////////////////////////////

	public function admin_retail_nonsubscribers()
	{
		error_reporting(0);
		ini_set('memory_limit', '-1');
		$this->layout='default';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$datas = array();
			$retail_register_ids=$this->CheckOut->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(   
	    		),
	   			'conditions'=>array(
	   				'CheckOut.is_retail' => 1
            	),
            	'group' => 'CheckOut.register_id',
       			'order' => array(
                	'CheckOut.id' => 'DESC'
            	)
        	));

        	foreach ($retail_register_ids as $val) {
        		$data=$this->Register->find('all', array(
	         		'recursive' => -1,
			 		'contain' => array(
		     			'PetProfile' => array('Product'=>array('conditions'=>array('Product.is_retail' => 1,'Product.isdeleted' => 0)))	   
		    		),
		   			'conditions'=>array(
		   				'and' => array(
		   					'Register.id' => $val['CheckOut']['register_id'],
	                		'Register.email_verified' => 0
	                	)
	            	),
	       			'order' => array(
	                	'Register.id' => 'DESC'
	            	)
	        	));
        		array_push($datas, $data[0]);
        	}
			$this->set(compact('datas'));

 			if($this->request->is("post") or $this->request->is("put")){
 	            $post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];
				$start_date=date('Y-m-d 00:00:00', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d 23:59:59', strtotime( $this->request->data['end_date'] ));
				$datas = array();
				$retail_register_ids=$this->CheckOut->find('all', array(
	         		'recursive' => -1,
			 		'contain' => array(   
		    		),
		   			'conditions'=>array(
		   				'and' => array(
	                		'CheckOut.created BETWEEN ? and ?' => array($start_date, $end_date),
	                		'CheckOut.is_retail' => 1
	                	)	                		
	            	),
	            	'group' => 'CheckOut.register_id',
	       			'order' => array(
	                	'CheckOut.id' => 'DESC'
	            	)

	        	));
	        	foreach ($retail_register_ids as $val) {
					$data=$this->Register->find('all', array(
	         			'recursive' => -1,
			 			'contain' => array(
		     				'PetProfile' => array('Product'=>array('conditions'=>array('Product.is_retail' => 1,
		     				'Product.isdeleted' => 0)))	   
		    			),
		   				'conditions'=>array(
		   					'Register.id' => $val['CheckOut']['register_id'],
	                		'Register.email_verified' => 0
	            		),
	       				'order' => array(
	                		'Register.id' => 'DESC'
	            		)
	        		));
	        		array_push($datas, $data[0]);
				}

				$this->set(compact('datas'));
            	$this->set(compact('post_start_date','post_end_date'));
			}
    	}else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 		   
	}

///////////////////////////////////////////////////////

	public function admin_retail_subscribers()
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
		 	ini_set('memory_limit', '-1');
		 	$this->layout='default';
		 			$datas=$this->Register->find('all',array(
		 				'contain'=>array('PetProfile'=>array('Product'=>array('conditions'=>array(
		     			'Product.is_retail' => 1,
		     			'Product.isdeleted' => 0)))),
		 				'conditions'=>array(''),
		 				'order' => array(
	                	'Register.id' => 'DESC'
	            			),
		 			));
		 			$data2=array();
		 			$pos=0;
		 			foreach ($datas as $data) {
		 					$pos2=0;
		 				foreach ($data['PetProfile'] as $pet) {
		 					// pr($pet);
		 					if($pet['Product']['is_retail']==1){
		 						//pr($pet);
		 						if($data['Register']['id']!=$data2[$pos-1]['Register']['id'])
		 						{
		 							$data2[$pos]['Register']=$data['Register'];
		 							$insert=$pos;
		 						}
		 						$data2[$insert]['PetProfile'][$pos2]=$pet;
		 						$pos2++;
		 						// $pos++;
		 					}
		 				}
		 				$pos++;
		 			}
		 			$datas=$data2;
    		$this->set(compact('datas'));
		 	if($this->request->is("post") or $this->request->is("put")){
		 	    $post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];
				$start_date=date('Y-m-d 00:00:00', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d 23:59:59', strtotime( $this->request->data['end_date'] ));
				// $datas=$this->Register->find('all', array(
	   //       			'recursive' => -1,
			 // 			'contain' => array(
		  //    				'PetProfile' => array('Product'=>array('conditions'=>array(
		  //    				'Product.is_retail' => 1,'Product.isdeleted' => 0)))	   
		  //   			),
		  //  				'conditions'=>array(
		  //  					'and' => array(
		  //  						'Register.created BETWEEN ? and ?' => array($start_date, $end_date),
	   //              			//'Register.email_verified' => 1
	   //              		)
	   //          		),
	   //     				'order' => array(
	   //              		'Register.id' => 'DESC'
	   //          		)
	   //      		));
				$datas=$this->Register->find('all',array(
		 				'contain'=>array('PetProfile'=>array('Product'=>array('conditions'=>array(
		     			'Product.is_retail' => 1,
		     			'Product.isdeleted' => 0)))),
		 				'conditions'=>array(
		 					'and' => array(
		   						'Register.created BETWEEN ? and ?' => array($start_date, $end_date),
	                			//'Register.email_verified' => 1
	                		),
		 				),
		 				'order' => array(
	                	'Register.id' => 'DESC'
	            			),
		 			));
		 			$data2=array();
		 			$pos=0;
		 			foreach ($datas as $data) {
		 					$pos2=0;
		 				foreach ($data['PetProfile'] as $pet) {
		 					// pr($pet);
		 					if($pet['Product']['is_retail']==1){
		 						//pr($pet);
		 						if($data['Register']['id']!=$data2[$pos-1]['Register']['id'])
		 						{
		 							$data2[$pos]['Register']=$data['Register'];
		 							$insert=$pos;
		 						}
		 						$data2[$insert]['PetProfile'][$pos2]=$pet;
		 						$pos2++;
		 						// $pos++;
		 					}
		 				}
		 				$pos++;
		 			}
		 			$datas=$data2;

				$this->set(compact('datas'));
				$this->set(compact('post_start_date','post_end_date'));
		 	}
		}
       	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 		   
	}

///////////////////////////////////////////////////////

	public function admin_active_tag_list()
	{    
		ini_set('memory_limit', '-1');
		$this->layout='default';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
		 	$datas=$this->PetProfile->find('all', array(
	         	'recursive' => -1,
			 	'contain' => array(
			 		'Register',
		     		'Product'=>array('conditions'=>array('Product.active' => 1,'Product.isdeleted' => 0))
	    		),
		   		'conditions'=>array(
					'PetProfile.is_active' => 1
	            ),
	       		'order' => array(
	                'PetProfile.id' => 'DESC'
	            )
        	));
			$this->set(compact('datas'));
		}
       	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 		   
	}

///////////////////////////////////////////////////////

	public function admin_deactive_tag_list()
	{    
		ini_set('memory_limit', '-1');
		$this->layout='default';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
		 	$datas=$this->PetProfile->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
		 			'Register',
	     			'Product'=>array('conditions'=>array('Product.active' => 0,'Product.isdeleted' => 0))
	    		),
	   			'conditions'=>array(
					'PetProfile.is_active' => 0
            	),
       			'order' => array(
                	'PetProfile.id' => 'DESC'
            	)
        	));
			$this->set(compact('datas'));
		}
      	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 
	}

///////////////////////////////////////////////////////

	public function admin_req_deactive_tag_list()
	{
		ini_set('memory_limit', '-1');
		$this->layout='default';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
		 	$datas=$this->PetProfile->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
		 			'Register',
	     			'Product'=>array('conditions'=>array(
	     				// 'Product.active' => 1, //hidden because, if a pet_deactivated before shipped, to show the records
	     				'Product.isdeleted' => 0))
	    		),
	   			'conditions'=>array(
					'PetProfile.is_active' => 0
            	),
       			'order' => array(
                	'PetProfile.cancellations_request_date' => 'DESC',
       				'PetProfile.cancellations_date' => 'ASC',
                	
            	)
        	));
		 	if($this->request->is("post") or $this->request->is("put")){
				$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));
				
				$datas=$this->PetProfile->find('all', array(
					'recursive' => -1,
					'contain' => array(
						'Register',
						'Product'=>array('conditions'=>array('Product.active' => 1,'Product.isdeleted' => 0))
					),
					'conditions' => array(
						'and' => array(
							'PetProfile.created BETWEEN ? and ?' => array($start_date, $end_date),					
						'PetProfile.is_active' => 0
					)),
					'order' => array(
						'PetProfile.id' => 'DESC'
					)
				));				
			}
			$this->set(compact('datas'));
			$this->set(compact('post_start_date','post_end_date'));
		}
       	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		
	}

///////////////////////////////////////////////////////

	public function admin_pet_info($id)
	{
		$this->layout='default';
		if(!empty($id)){
			$pet_code=$id;	
			$this->set('id',$id);
			$this->set('pet_id',$id);
			$currentUser = $this->Auth->user();
			if(!empty($currentUser)){
				$this->set('currentUser',$currentUser);
				$this->set('user_id',$currentUser['id']);
				$user_id = $currentUser['id'];
				$this->set('user_details',$datas);
				$petdetails = $this->PetProfile->find('first', array(
					'recursive' => -1,
					'contain'=>array('Product'=>array('Category')),
					
            		'conditions' => array(								  
               		'PetProfile.pet_code' => $pet_code				 

			 		),
         		));
         		$pet_new_code=$petdetails['PetProfile']['register_id'];
         		$datas=$this->Register->findById($pet_new_code);
				$this->set('datas',$datas);
				$petcount = sizeof($petdetails);
				if($petcount==0)
				{
					$this->redirect(array('action' => '../registers/index'));
				}

        		$this->set('petdetails',$petdetails);
				if ($this->request->is('post') || $this->request->is('put')) {
					$eid=$petdetails[0]['PetProfile']['id'];
					$this->request->data['PetProfile']['id']=$eid;
					$this->request->data['PetProfile']['pet_finder_note']=$this->request->data['notes'];
					if ($this->PetProfile->save($this->request->data)) {
						$this->Session->setFlash(__('The PetProfile has been saved'));
						$this->redirect(array('action' => 'lost_poster/'.$id));
	            	}
				}
		 	}
		 	else{ 
				$this->redirect(array('action' => '../registers/login'));
			}
		}
		else{ 
			$this->redirect(array('action' => '../registers/index'));
		}	 
	}

//////////////////////////////////////////////////
	
	public function admin_nonsubscribers()
	{
		error_reporting(0);
		ini_set('memory_limit', '-1');
		$this->layout='default';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$datas = array();
			$retail_register_ids=$this->CheckOut->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(   
	    		),
	   			'conditions'=>array(
	   				'CheckOut.is_retail' => 0
            	),
            	'group' => 'CheckOut.register_id',
       			'order' => array(
                	'CheckOut.id' => 'DESC'
            	)
        	));

			foreach ($retail_register_ids as $val) {

				$data=$this->Register->find('all', array(
	         		'recursive' => -1,
			 		'contain' => array(
			 			'PetProfile' => array('Product'=>array('conditions'=>array('Product.is_retail' => 0,'Product.isdeleted' => 0)))	   
			 		),
			  		'conditions'=>array(
			  			'and' => array(
			  				'Register.id' => $val['CheckOut']['register_id'],
	                		'Register.email_verified' => 0
	                	)
	            	),
	        		'order' => array(
	                	'Register.id' => 'DESC'
	            	)
	        	));
	        	array_push($datas, $data[0]);
			}

        	if($this->request->is("post") or $this->request->is("put")){
        	    $post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];
				$start_date=date('Y-m-d 00:00:00', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d 23:59:59', strtotime( $this->request->data['end_date'] ));
				
				$datas = array();
				$retail_register_ids=$this->CheckOut->find('all', array(
	         		'recursive' => -1,
			 		'contain' => array(   
		    		),
		   			'conditions'=>array(
		   				'and' =>array(
		   					'CheckOut.created BETWEEN ? and ?' => array($start_date, $end_date),
		   					'CheckOut.is_retail' => 0
		   				)
	            	),
	            	'group' => 'CheckOut.register_id',
	       			'order' => array(
	                	'CheckOut.id' => 'DESC'
	            	)
	        	));
				
	        	foreach ($retail_register_ids as $val) {

					$datas=$this->Register->find('all', array(
						'recursive' => -1,
						'contain' => array(
						 	'PetProfile' => array('Product'=>array('conditions'=>array('Product.is_retail' => 0,'Product.isdeleted' => 0)))	   
						 ),
						'conditions' => array(
							'and' => array(
								'Register.id' => $val['CheckOut']['register_id'],	
								'Register.email_verified' => 0
							)
						),
						'order' => array(
							'Register.id' => 'DESC'
						)
					));
					array_push($datas, $data[0]);
				}		
			}

			$this->set(compact('datas'));
			$this->set(compact('post_start_date','post_end_date'));
		}
       	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 		   
	}

////////////////////////////////////////////////////

	public function admin_pet_active()
	{
		$main_url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		ini_set('memory_limit', '-1');
		$this->layout='default';
	    $uid = $this->Auth->User();
		if(!empty($uid)){		
			$pet_details=$this->PetProfile->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
	   				'Register'	   
	   			),
	   			'conditions'=>array(
					'PetProfile.is_cancellations' => 0
            	),
   				'order' => array(
 					'PetProfile.id' => 'DESC'
            	)
       		));		
			if (strpos($main_url,'devops') !== false || strpos($main_url,'uat') !== false || strpos($main_url,'localhost') !== false) { 
		        $url = "devops_tag";     
	        }else{
	         	$url = "tag";
	        }
	        $this->set(compact('main_url'));
	        $this->set(compact('url'));
			$this->set(compact('pet_details'));
			if($this->request->is("post") or $this->request->is("put")){
				$post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];
				$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));
				
				$pet_details=$this->PetProfile->find('all', array(
					'recursive' => -1,
					'contain' => array(
						'Register'
					),
					'conditions' => array(
					 	'and' => array(
					 		'PetProfile.created BETWEEN ? and ?' => array($start_date, $end_date),						
					 	)
					),
					'order' => array(
						'PetProfile.id' => 'DESC'
					)
				));
				$this->set('pet_details',$pet_details);
                $this->set(compact('post_start_date','post_end_date'));
			}
		}
       	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}				   
	}
///////////////////////////////////
	public function admin_pet_test()
	{
		$main_url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		ini_set('memory_limit', '-1');
		$this->layout='default';
	    $uid = $this->Auth->User();
		if(!empty($uid)){		
			$pet_details=$this->PetProfile->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
	   				'Register',
	   				'Product'	   
	   			),
	   			'conditions'=>array(
					'PetProfile.is_cancellations' => 0
            	),
   				'order' => array(
 					'PetProfile.id' => 'DESC'
            	)
       		));		
			if (strpos($main_url,'devops') !== false || strpos($main_url,'uat') !== false || strpos($main_url,'localhost') !== false) { 
		        $url = "devops_tag";     
	        }else{
	         	$url = "tag";
	        }
	        $this->set(compact('main_url'));
	        $this->set(compact('url'));
			$this->set(compact('pet_details'));
			if($this->request->is("post") or $this->request->is("put")){
				$post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];
				$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));
				
				$pet_details=$this->PetProfile->find('all', array(
					'recursive' => -1,
					'contain' => array(
						'Register'
					),
					'conditions' => array(
					 	'and' => array(
					 		'PetProfile.created BETWEEN ? and ?' => array($start_date, $end_date),						
					 	)
					),
					'order' => array(
						'PetProfile.id' => 'DESC'
					)
				));
				$this->set('pet_details',$pet_details);
                $this->set(compact('post_start_date','post_end_date'));
			}
		}
       	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}				   
	}
///////////////////////////////////////////////////////

	public function admin_pet_inactive(){
		ini_set('memory_limit', '-1');
		$this->layout='default';
		$main_url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	    $uid = $this->Auth->User();
		if(!empty($uid)){

			$pet_details = $this->PetProfile->find("all",array(
				'recursive' => -1,
				'contain'=>array('Register'),
				'conditions' => array(
					'and' => array(
						'PetProfile.is_cancellations'=>1
					)
				),
		        'order'=> array('PetProfile.id'=>'DESC')
			));

			$this->set('pet_details',$pet_details);

			if($this->request->is("post") or $this->request->is("put")){
                $post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];
				$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));
				
				$pet_details = $this->PetProfile->find("all",array(
					'recursive' => -1,
					'contain'=>array('Register'),
					'conditions' => array(
					 	'and' => array(
					 		'PetProfile.created BETWEEN ? and ?' => array($start_date, $end_date),
							'PetProfile.is_cancellations'=>1
					 	)
					),
		        	'order'=> array('PetProfile.id'=>'DESC')
				));

				$this->set('pet_details',$pet_details);
			}
			if (strpos($main_url,'devops') !== false || strpos($main_url,'uat') !== false || strpos($main_url,'localhost') !== false) { 
		        $url = "devops_tag";     
	        }else{
	         	$url = "tag";
	        }
	        $this->set(compact('main_url'));
	        $this->set(compact('url'));
			$this->set(compact('post_start_date','post_end_date'));
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}	
	}

////////////////////////////////////////////////////////

	public function admin_pet_edit($id=NULL)
	{
		$main_url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		$currentUser = $this->Auth->user();
		if(!empty($currentUser) ){
			
			if($id==NULL){
				$msg="REQUIRE PET ID ";
				$this->set(compact('msg'));
			}

			

            if( filter_var($id, FILTER_VALIDATE_INT) === false ){
            $datas="";
              }else{
		
              $datas=$this->PetProfile->findById($id);

              }
			
            $this->set(compact('datas'));

			$state=$this->State->find('all');
			$this->set('state',$state);

			if (strpos($main_url,'devops') !== false || strpos($main_url,'uat') !== false || strpos($main_url,'localhost') !== false) { 
		        $url = "devops_tag";     
	        }else{
	         	$url = "tag";
	        }
	        $this->set(compact('main_url'));
	        $this->set(compact('url'));
			$pet_data = $this->PetProfile->find('first', array(
				'recursive' => -1,
				'contain'=>array('Register','Product'=>array('Category')),
        		'conditions' => array(
				 	'PetProfile.id' => $id
            	),
        	));
			$this->set('pet_data',$pet_data);

			if($this->request->is("post") or $this->request->is("put"))
			{  
				$this->request->data['PetProfile']['id']=$id;
			    $this->request->data["PetProfile"]["pet_name"]=$this->request->data["pet_name"];
				$this->request->data["PetProfile"]["pet_url"]=$this->request->data["pet_url"];
				$this->request->data["PetProfile"]["pet_code"]=$this->request->data["pet_code"];
				$this->request->data["PetProfile"]["breed"]=$this->request->data["breed"];
				$this->request->data["PetProfile"]["sex"]=$this->request->data["i-radio"];
				$this->request->data["PetProfile"]["product_color"]=$this->request->data["product_color"];
				$this->request->data["PetProfile"]["pet_spot"]=$this->request->data["pet_spot"];
				$this->request->data["PetProfile"]["pet_dob"]=$this->request->data["pet_dob"];
				$this->request->data["PetProfile"]["pet_microchip_brand"]=$this->request->data["pet_microchip_brand"];
				$this->request->data["PetProfile"]["pet_microchip"]=$this->request->data["pet_microchip"];
				$this->request->data["PetProfile"]["pet_finder_note"]=$this->request->data["pet_finder_note"];
				
				$this->request->data["PetProfile"]["vet_business_name"]=$this->request->data["vet_business_name"];
				$this->request->data["PetProfile"]["vet_dr_name"]=$this->request->data["vet_dr_name"];
				$this->request->data["PetProfile"]["vet_dr_address"]=$this->request->data["vet_dr_address"];
				$this->request->data["PetProfile"]["vet_dr_suite_apt"]=$this->request->data["vet_dr_suite_apt"];

			// change24_01_19	//

			$this->request->data["PetProfile"]["vet_dr_state"]=$this->request->data["vet_dr_state"];
			
			// end

				$this->request->data["PetProfile"]["var_dr_city"]=$this->request->data["var_dr_city"];
				$this->request->data["PetProfile"]["vet_dr_zip_code"]=$this->request->data["vet_dr_zip_code"];
				$this->request->data["PetProfile"]["vet_dr_phone"]=$this->request->data["vet_dr_phone"];
				$this->request->data["PetProfile"]["vet_dr_email"]=$this->request->data["vet_dr_email"];
				$this->request->data["PetProfile"]["vet_dr_facebook"]=$this->request->data["vet_dr_facebook"];
				$this->request->data["PetProfile"]["vet_dr_twitter"]=$this->request->data["vet_dr_twitter"];
			
			
				$this->request->data["PetProfile"]["medical_conds"]=$this->request->data["medical_conds"];
				$this->request->data["PetProfile"]["med_allergies"]=$this->request->data["med_allergies"];
				$this->request->data["PetProfile"]["med_medications"]=$this->request->data["med_medications"];
				$this->request->data["PetProfile"]["pet_food_like"]=$this->request->data["pet_food_like"];
				$this->request->data["PetProfile"]["vaccin_rabies"]=$this->request->data["vaccin_rabies"];
				$this->request->data["PetProfile"]["rabies_tag"]=$this->request->data["rabies_tag"];
				$this->request->data["PetProfile"]["vaccination_date"]=$this->request->data["vaccination_date"];
				$this->request->data["PetProfile"]["vaccination_due_date"]=$this->request->data["vaccination_due_date"];
				$this->request->data["PetProfile"]["additional_vaccination"]=$this->request->data["additional_vaccination"];
				$this->request->data["PetProfile"]["pet_registration"]=$this->request->data["pet_registration"];
				$this->request->data["PetProfile"]["pet_city"]=$this->request->data["pet_city"];
				$this->request->data["PetProfile"]["pet_state"]=$this->request->data["pet_state"];
				$this->request->data["PetProfile"]["ins_company_name"]=$this->request->data["ins_company_name"];
				$this->request->data["PetProfile"]["ins_policy_number"]=$this->request->data["ins_policy_number"];
				$this->request->data["PetProfile"]["ins_phone"]=$this->request->data["ins_phone"];
			
				if($this->PetProfile->save($this->request->data))
				{
					$this->request->data['Register']['id']=$pet_data['Register']['id'];
					$this->request->data['Register']['add_info_name']=$this->request->data['add_info_name'];
					$this->request->data['Register']['add_info_address']=$this->request->data['add_info_address'];
					$this->request->data['Register']['add_info_suite_apt']=$this->request->data['add_info_suite_apt'];
					$this->request->data['Register']['add_info_city']=$this->request->data['add_info_city'];
					$this->request->data['Register']['add_info_country']=$this->request->data['add_info_country'];
					$this->request->data['Register']['add_info_postal_code']=$this->request->data['add_info_postal_code'];
					$this->request->data['Register']['add_info_mobile_phone']=$this->request->data['add_info_mobile_phone'];
					$this->request->data['Register']['add_info_phone_no']=$this->request->data['add_info_phone_no'];
					$this->request->data['Register']['add_info_email']=$this->request->data['add_info_email'];
					$this->request->data['Register']['add_info_facebook_link']=$this->request->data['add_info_facebook_link'];
					$this->request->data['Register']['add_info_twitter_link']=$this->request->data['add_info_twitter_link'];
					 $this->Register->save($this->request->data);
			 		//$msg=$this->admin_file('Register_Pet_Edit');
                      $msg='Data has been updated successfully';
					$this->Session->setFlash(__($msg, null), 
                        'default', 
                            array('class' => 'alert data_succesfull_message'));
					if($pet_data['PetProfile']['is_demo']==1){
						$this->redirect(array('controller'=>'products','action' => 'demotag'));
					}
					else if ($pet_data['PetProfile']['is_active'] == 1) {
                        $this->redirect(array("action"=>"pet_active"));
                    } else {
                        $this->redirect(array("action"=>"pet_inactive"));
                    }
					
				}
				else
				{
					$msg='Database error';
                	$this->Session->setFlash(__($msg, null), 
                        'default', 
                            array('class' => 'alert alertt'));
				}
			}
	 	}
    	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}	 		  
	}
/////////////////////////
	public function admin_pet_active_delete1($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->PetProfile->find('first',array(
					'contain'=>array('Product'),
					'conditions'=>array(
						'PetProfile.id'=>$id)));		
				if($this->PetProfile->delete($id))
				{
						$order_items=$this->OrderItem->find('first',array(
					'conditions'=>array(
						'OrderItem.product_id'=>$data['PetProfile']['product_id'],
						'OrderItem.subscription_id'=>$data['PetProfile']['subscription_id'],
					)));
				$data_product=$this->Product->findById($order_items['OrderItem']['product_id']);
				$this->Product->id=$data_product['Product']['id'];
				$this->request->data['Product']['active'] = 0;
				$this->request->data['Product']['tag_replace_request'] = 0;
				$this->request->data['Product']['is_request_processed'] = 0;
				$this->request->data['Product']['sold_date'] = NULL;
				$this->request->data['Product']['starting_date'] = NULL;
				$this->request->data['Product']['end_date'] = NULL;

				$this->Product->save($this->request->data);

					if(!empty($data['PetProfile']['subscription_id']))
					{
						$si_id=$data['PetProfile']['subscription_id'];
						$status=$this->Stripe->SubscriptionItemCancel($si_id);
						if($status==1)
						{
							$msg='Data deleted successfully';
		                	$this->Session->setFlash($msg);
							$this->Session->setFlash(__($msg, null), 
		                        'default', 
		                        array('class' => 'alert data_succesfull_message'));	
						}
						if($status==0){
							$result=$this->Stripe->SubscriptionItemRetrive($si_id);
							if(isset($result['sub_id']))
							{
								$result=$this->Stripe->customerRetrieveSubscriptions($result['sub_id']);
								if(sizeof($result['items'])==1)
											{
												$cancel_subscription=$this->Stripe->SubscriptionCancel($result['subscription_id']);
												$msg='Data has been deleted successfully';
		                	$this->Session->setFlash($msg);
							$this->Session->setFlash(__($msg, null), 
		                        'default', 
		                        array('class' => 'alert data_succesfull_message'));
											}
							}else{
								$msg='Delete unsuccessful from stripe';
								$this->Session->setFlash($msg);
							}
						}
					}else{
						$msg='Data deleted successfully';
		                	$this->Session->setFlash($msg);
							$this->Session->setFlash(__($msg, null), 
		                        'default', 
		                        array('class' => 'alert data_succesfull_message'));
					}		
				}
				else
				{
					  $msg='Delete unsuccessful';
                	$this->Session->setFlash($msg);
				}
			}
			$this->redirect(array("action"=>"pet_test"));		 
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
		exit;
	}
///////////////////////////////////////////////////////

	public function admin_pet_active_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->PetProfile->findById($id);
				if($this->PetProfile->delete($id))
				{
					if(!empty($data['PetProfile']['subscription_id']))
					{
						$si_id=$data['PetProfile']['subscription_id'];
						$status=$this->Stripe->SubscriptionItemCancel($si_id);
						if($status==1)
						{
							$msg='Data deleted successfully';
		                	$this->Session->setFlash($msg);
							$this->Session->setFlash(__($msg, null), 
		                        'default', 
		                        array('class' => 'alert data_succesfull_message'));	
						}
						if($status==0){
							$result=$this->Stripe->SubscriptionItemRetrive($si_id);
							if(isset($result['sub_id']))
							{
								$result=$this->Stripe->customerRetrieveSubscriptions($result['sub_id']);
								if(sizeof($result['items'])==1)
											{
    $cancel_subscription=$this->Stripe->SubscriptionCancel($result['subscription_id']);
												$msg='Data has been deleted successfully';
		                	$this->Session->setFlash($msg);
							$this->Session->setFlash(__($msg, null), 
		                        'default', 
		                        array('class' => 'alert data_succesfull_message'));
											}
							}else{
								$msg='Delete unsuccessful from stripe';
								$this->Session->setFlash($msg);
							}
						}
					}else{
						$msg='Data deleted successfully';
		                	$this->Session->setFlash($msg);
							$this->Session->setFlash(__($msg, null), 
		                        'default', 
		                        array('class' => 'alert data_succesfull_message'));
					}		
				}
				else
				{
					//$msg=$this->admin_file('Register_Pet_Delete_Fail');
					  $msg='Delete unsuccessful';
                	$this->Session->setFlash($msg);
				}
			}
			$this->redirect(array("action"=>"pet_active"));		 
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
		exit;
	}

///////////////////////////////////////////////////////////

	public function admin_pet_inactive_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->PetProfile->findById($id);
			 
				if($this->PetProfile->delete($id))
				{
				 
					//$msg=$this->admin_file('Register_Pet_Delete');
					  $msg='Data has been deleted successfully';
                	$this->Session->setFlash($msg);
					$this->Session->setFlash(__($msg, null), 
                        'default', 
                        array('class' => 'alert data_succesfull_message'));				
				}
				else
				{
					//$msg=$this->admin_file('Register_Pet_Delete_Fail');
					  $msg='Delete unsuccessful';
                	$this->Session->setFlash($msg);
				}
			}
			$this->redirect(array("action"=>"pet_inactive"));		 
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

/////////////////////////////2nd phase///////////	

	public function admin_order_list()
	{
	    $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$order_details=$this->Order->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
	     			'OrderItem'
   				),
	   			'conditions' => array(
                	'Order.is_dispatch' =>1
        		),
       			'order' => array(
                	'Order.id' => 'DESC'
            	)
        	));
			$this->set(compact('order_details'));
			$count_order = sizeof($order_details);
			$this->set(compact('count_order'));
			if($this->request->is("post") or $this->request->is("put")){
				$post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];				
				$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));
				$order_details=$this->Order->find('all', array(
            		'recursive' => -1,
		    		'contain' => array(
	        			'OrderItem'
	        		),
	        		'conditions' => array(
			    		'and' => array(
							'Order.created BETWEEN ? and ?' => array($start_date, $end_date),
                			'Order.is_dispatch' =>1
             			)
			    	),
            		'order' => array(
                		'Order.id' => 'DESC'
         			)
            	));
				$this->set(compact('order_details'));
				$this->set(compact('post_start_date','post_end_date'));
			}
		}
     	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 
	}

////////////////////////////////////////////////////
    public function admin_new_order_edit($id)
	{   

           if($id==NULL){
				$msg="REQUIRE PET ID ";
				$this->set(compact('msg'));}
           if( filter_var($id, FILTER_VALIDATE_INT) === false ){
            $datas="";}else{
		$datas=$this->Order->findById($id);}
        $this->set(compact('datas'));

        $state=$this->State->find('all');
			$this->set('state',$state);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$order_details=$this->Order->find('first', array(
				'recursive' => -1,
			 	'conditions' => array(
					'Order.id' =>$id
			 	),
				'order' => array(
					'Order.id' => 'DESC'
				)
			));
			$this->set('order_details',$order_details);
			if($this->request->is("post"))
		    {
		    $this->request->data["Order"]["id"]=$id;
            $this->request->data["Order"]["first_name"]=$this->request->data["fname"];
		    $this->request->data["Order"]["last_name"]=$this->request->data["lname"];
			$this->request->data["Order"]["email"]=$this->request->data["email"];
			$this->request->data["Order"]["phone"]=$this->request->data["billing_mobile"];
			$this->request->data["Order"]["billing_home_phone"]=$this->request->data["phone_no"];
			$this->request->data["Order"]["billing_address"]=$this->request->data["billing_address"];
			$this->request->data["Order"]["billing_city"]=$this->request->data["billing_city"];
			$this->request->data["Order"]["billing_state"]=$this->request->data["billing_state"];
			$this->request->data["Order"]["billing_country"]=$this->request->data["billing_country"];
			$this->request->data["Order"]["billing_zip"]=$this->request->data["billing_postal_code"];
           $this->request->data["Order"]["shipping_first_name"]=$this->request->data["shipping_first_name"];
           $this->request->data["Order"]["shipping_last_name"]=$this->request->data["shipping_last_name"];
           $this->request->data["Order"]["shipping_home_phone"]=$this->request->data["shipping_home_phone"];
           $this->request->data["Order"]["shipping_mob_phone"]=$this->request->data["shipping_mobile_phone"];
           $this->request->data["Order"]["shipping_email"]=$this->request->data["shipping_email"];
			$this->request->data["Order"]["shipping_address"]=$this->request->data["shipping_address"];
			$this->request->data["Order"]["shipping_city"]=$this->request->data["shipping_city"];
			$this->request->data["Order"]["shipping_state"]=$this->request->data["shipping_state"];
			$this->request->data["Order"]["shipping_country"]=$this->request->data["shipping_country"];
			$this->request->data["Order"]["shipping_zip"]=$this->request->data["shipping_postal_code"];
            $this->request->data["Order"]["created"]=$this->request->data["created_date"];
            $this->request->data["Order"]["order_item_count"]=$this->request->data["order_item_count"];
            $this->request->data["Order"]["discount"]=$this->request->data["discount"];
            $this->request->data["Order"]["total"]=$this->request->data["total"];
            $this->request->data["Order"]["tax"]=$this->request->data["tax"];
            	if($this->Order->save($this->request->data))
				{	
				  $this->Session->setFlash(__("Data has been updated successfully"));
				  $this->redirect(array("controller"=>"registers","action"=>"new_order_list"));
				}
			}
			}
			
	}
	public function admin_new_order_list()
	{
		ini_set('memory_limit', '-1');
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$orderdetails=$this->Order->find('all', array(
				'recursive' => -1,
				'contain' => array(
					'OrderItem'=>array('Product'=>array('conditions' => array(
						'Product.isdeleted' =>0,
						'Product.active' =>1
					)))
				),
			 	'conditions' => array(
					'Order.is_dispatch !=' =>1
			 	),
				'order' => array(
					'Order.id' => 'DESC'
				)
			));
			$order_details=array();
			$j=0;
			$z=0;
			for($i=0;$i<count($orderdetails);$i++){
				if(!empty($orderdetails[$i]['Order'])){
					if(!empty($orderdetails[$i]['OrderItem'])){

						if(!empty($orderdetails[$i]['OrderItem'][$z]['Product'])){
							$order_details[$j]=$orderdetails[$i];
							$j++;
							$z=0;
						}
					}
				}
			}
			$order_count=$this->Order->find('all', array(
				'recursive' => -1,
				'contain' => array(
					'OrderItem'=>array('Product'=>array('conditions' => array(
						'Product.isdeleted' =>0,
						'Product.active' =>1
					)))
				),
				'conditions' => array(
					'Order.is_dispatch !=' =>1
				),
				'order' => array(
					'Order.id' => 'DESC'
				)
			));
			$web_count=0;
			$retail_count=0;
			if(!empty($order_count)){
				foreach($order_count as $order_val){
					if(!empty($order_val['OrderItem'])){
						if(!empty($order_val['OrderItem'][0]['Product'])){
							if($order_val['OrderItem'][0]['Product']['is_retail']==1){
								$retail_count=$retail_count+1;
							}
							else{
								$web_count=$web_count+1;;
							}
						}	
					}
				}
			}
			$total_order=($retail_count+$web_count);
			$this->set(compact('order_details','total_order','web_count','retail_count'));
////find data for change			
$changereq=$this->TagReplacementRequest->find('all', array(
				'recursive' => -1,
				'conditions' => array(
				'TagReplacementRequest.change_request ' =>1,
				'TagReplacementRequest.tag_replace_request'=>1
				)
			));
					    $a=array();
					    $c=count($changereq);
					    for ( $i=0; $i < $c; $i++)
					    { 
					      array_push($a,$changereq[$i]['TagReplacementRequest']['product_code']);
					     }

$this->set(compact('a'));
//////submit starts

			if($this->request->is("post") or $this->request->is("put")){
				
				$post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];


				$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));
				
				$order_details=$this->Order->find('all', array(
					'recursive' => -1,
					'contain' => array(
						'OrderItem'=>array('Product'=>array('conditions' => array(
							'Product.isdeleted' =>0,
							'Product.active' =>1
						)),
						)
					),
					'conditions' => array(
						'and' => array(
							'Order.created BETWEEN ? and ?' => array($start_date, $end_date),
							'Order.is_dispatch !=' =>1,
							'Order.order_code !=' =>NULL,
						)
					),
					'order' => array(
						'Order.id' => 'DESC'
					)
				));

				/*******************************************/
				$order_count=$this->Order->find('all', array(
					'recursive' => -1,
					'contain' => array(
						'OrderItem'=>array('Product'=>array('conditions' => array(
							'Product.isdeleted' =>0,
							'Product.active' =>1
						)))
					),
					'conditions' => array(
						'and' => array(
							'Order.created BETWEEN ? and ?' => array($start_date, $end_date),
							'Order.is_dispatch !=' =>1,
							'Order.order_code !=' =>NULL
						)
					),
					'order' => array(
						'Order.id' => 'DESC'
					)
				));
				$web_count=0;
				$retail_count=0;
				if(!empty($order_count)){
					foreach($order_count as $order_val){
						if(!empty($order_val['OrderItem'])){
							if(!empty($order_val['OrderItem'][0]['Product'])){
								if($order_val['OrderItem'][0]['Product']['is_retail']==1){
									$retail_count=$retail_count+1;
								}
								else{
									$web_count=$web_count+1;;
								}
							}	
						}
					}
				}				
				$total_order=($retail_count+$web_count);
				$this->set(compact('order_details','total_order','web_count','retail_count','post_start_date','post_end_date'));
			}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 
	}

///////////////////////End of 2nd Phase////////////////////////	

	public function admin_order_status($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
				$s=$this->Order->findById($id,array("fields"=>"status"));
				if($s)
				{
					if($s["Order"]["status"]==1){ 
						$ns=0; 
					}
					elseif($s["Order"]["status"]==0){      
						$ns=1; 
					}
					$this->Order->id=$id;
					if($this->Order->saveField("status",$ns))
					{
						$this->Session->setFlash(__("Status changed successfully"));
					}
					else
					{
						$this->Session->setFlash(__("Database error"));
					}
				}	
				else
				{
					$this->Session->setFlash(__("Invalid Id"));
				}
				 $this->redirect(array("action"=>"order_list"));			
			}
	 	}
	 	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		} 	 
	}

/////////////////////////////////// pankaj order_dispatch_info work //////////////////
   public function test()
   {
   	$order_item_data=$this->OrderItem->find("all",array(
                  'contain'=>array('Category'),
                   'conditions'=>array('OrderItem.order_id'=>10)
               ));
   	$massage.='<p>';
   	foreach ($order_item_data as $value) {
				$massage.='Product '.$value['OrderItem']['product_code'].'('.$value['Category']['name'].')<br>';
							}
								$massage.='</p>';

   	echo $massage;
   	exit();
   }
	public function admin_order_dispatch_info()
	{
		$mails=$this->Email->find('first');
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
					$is_dispatch=$_POST['is_dispatch']; 
					$order_id=$_POST['order_id'];
					$s=$this->Order->findById($order_id,array("fields"=>"is_dispatch"));
					if($s)
			{				
					$this->Order->id=$order_id;
					if($this->Order->saveField("is_dispatch",$is_dispatch))
				{
                    $email2=$this->Order->findById($order_id,array("fields"=>"email"));
                    $firstname=$this->Order->findById($order_id,array("fields"=>"first_name"));
                    $lastname=$this->Order->findById($order_id,array("fields"=>"last_name"));
                    $shipping_address=$this->Order->findById($order_id,array("fields"=>"shipping_address"));
                    $shipping_city=$this->Order->findById($order_id,array("fields"=>"shipping_city"));
                    $shipping_zip=$this->Order->findById($order_id,array("fields"=>"shipping_zip"));
                    $shipping_state=$this->Order->findById($order_id,array("fields"=>"shipping_state"));
                    $shipping_country=$this->Order->findById($order_id,array("fields"=>"shipping_country"));
                    $order_item_data=$this->OrderItem->find("all",array(
                        'contain'=>array('Category'),
                    	'conditions'=>array('OrderItem.order_id'=>$order_id)));

                    $product_code=$order_item_data["OrderItem"]["product_code"];
                    $shipted_date=date('y-m-d');
                    $product_table_data=$this->Product->find('all', array(
         		    'recursive' => -1,
	   			    'conditions' => array(					
					'Product.product_code' =>$product_code	
					)     ));
                   $product_category_id= $product_table_data['Product']['category_id'];

                   $category_table_data=$this->Category->find('all', array(
         		    'recursive' => -1,
	   			    'conditions' => array(					
					'Category.id' =>$product_category_id	
					)     ));
                   $category_name= $category_table_data['Category']['name'];
                   // echo $product_code ;
                    $email1=$email2["Order"]["email"];
                    $shipping_city=$shipping_city["Order"]["shipping_city"];
                    $shipping_zip=$shipping_zip["Order"]["shipping_zip"];
                    $shipping_state=$shipping_state["Order"]["shipping_state"];
                    $shipping_country=$shipping_country["Order"]["shipping_country"];
                    $shipping_address=$shipping_address["Order"]["shipping_address"];

                    $massage = '';
                    $massage='
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <!-- NAME: FOLLOW UP -->
        <!--[if gte mso 15]>
        <xml>
            <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Your Tags Have Shipped!</title>
        
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage,.mcnRetinaImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	/*
	@tab Page
	@section Heading 1
	@style heading 1
	*/
		h1{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:40px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 2
	@style heading 2
	*/
		h2{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:34px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 3
	@style heading 3
	*/
		h3{
			/*@editable*/color:#444444;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:22px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 4
	@style heading 4
	*/
		h4{
			/*@editable*/color:#949494;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:125%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Container Style
	*/
		#templateHeader{
			/*@editable*/background-color:#ea4847;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:50% 50%;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Header
	@section Header Interior Style
	*/
		.headerContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Header
	@section Header Text
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/color:#757575;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Link
	*/
		.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
			/*@editable*/color:#007C89;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section Body Container Style
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:20px;
			/*@editable*/padding-bottom:54px;
		}
	/*
	@tab Body
	@section Body Interior Style
	*/
		.bodyContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Body
	@section Body Text
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/color:#757575;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section Body Link
	*/
		.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
			/*@editable*/color:#007C89;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Footer
	@section Footer Style
	*/
		#templateFooter{
			/*@editable*/background-color:#333333;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:45px;
			/*@editable*/padding-bottom:63px;
		}
	/*
	@tab Footer
	@section Footer Interior Style
	*/
		.footerContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Footer
	@section Footer Text
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@section Footer Link
	*/
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}

}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}

}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnRetinaImage{
			max-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer,.mcnImageCardLeftImageContentContainer,.mcnImageCardRightImageContentContainer{
			max-width:100% !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBottomContent:last-child .mcnCaptionBottomImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}

}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:30px !important;
			/*@editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:26px !important;
			/*@editable*/line-height:125% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}

}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}

}</style></head>
    <body>
        <!--*|IF:MC_PREVIEW_TEXT|*-->
        <!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Your Pin Paws tags are on their way.</span><!--<![endif]-->
        <!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top" id="templateHeader" data-template-container>
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" border="0" align="left" width="100%" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                
                                    
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" align="middle" width="192">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateBody" data-template-container>
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="bodyContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                
                                    
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/a6c0472e-0437-41b1-a975-8b7f1bda45af.jpg" style="max-width:600px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnRetinaImage" width="300" align="middle">
                                    
                                
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        
                        <td class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;" valign="top">
                        
                            <h1><span style="font-size:30px"><strong>Great news, your tags have shipped!</strong></span></h1>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                        
                            <p>Please allow 3-5 business days before they arrive.&nbsp;&nbsp;&nbsp;</p>

<p>While you wait...</p>

<ol>
	<li>Please add the <span style="color:#EA4847">Pin Paws SMS Alert Number<strong><span style="color:#EA4847"> </span></strong>(469-518-3550)</span><strong> </strong>as a contact in your mobile phone.</li>
	<li>Login to your account via the link below and complete your pet(s) profiles.</li>
</ol>

<p>Cheers to continued long walks on the beach, soapy baths, tummy rubs, table scraps, tennis balls, snuggles, car rides and play dates with your 4-legged friend!</p>

                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" valign="middle" align="center">
                                <a class="mcnButton " title="Account Login" href="https://pinpaws.com/account/login" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Account Login</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">
                
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                        
                            <table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
                                        <h3 style="text-align: center;">Have Questions?</h3>

<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:support@pinpaws.com" target="_blank">support@pinpaws.com</a></div>

                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
                
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" id="templateFooter" data-template-container>
                                    <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="600" style="width:600px;">
                                    <![endif]-->
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
                                        <tr>
                                            <td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding-left:9px;padding-right:9px;" align="center">
            <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                            
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                            
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        
                                        
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                            
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                            
                                                                            
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                        
                            <em>Copyright Â© 2019 Pin Paws. All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
16775 Addison Rd., Suite 600 Addison, TX 75001<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
                                        </tr>
                                    </table>
                                    <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
		
				   	// $massage.='Hello <strong>'.$fistname.'</strong>,<br/>';
        //            	$massage.='We have successfully shipped your product';
        //            	$massage.='<br/> Thanks, <br/> Pinpaws.com';
        
					$mail=$this->Email->find('first');//******************			
						$Email = new CakeEmail();
						$Email->emailFormat('html');
						$siteName=Configure::read("site_name");
						$email =$mail['Email']['info_email']; //"info@pinpaws.com";
						$Email->from(array($email=>$siteName));
						$Email->to($email1);
						$Email->subject($mail['Email']['order_shipped']);//"successfully shipped ".$siteName
						$Email->send($massage);
					
				}
				else
				{
					echo "0"; exit;
				}
			}	
			else
			{
				   echo "0"; exit;
			}
			$this->redirect(array("action"=>"new_order_list"));		
	 	}
	 	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		} 	 
	}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////2nd phase/////////////////	

	public function admin_orderstatus($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
				$s=$this->Order->findById($id,array("fields"=>"is_dispatch"));
				if($s)
				{
					if($s["Order"]["is_dispatch"]==1){ 
						$ns=0; 
					}
					elseif($s["Order"]["is_dispatch"]==0){ 
						$ns=2; 
					}
					elseif($s["Order"]["is_dispatch"]==2){ 
						$ns=1;
					}
					$this->Order->id=$id;
					if($this->Order->saveField("is_dispatch",$ns))
					{
						$this->Session->setFlash(__("Status changed successfully"));
					}
					else
					{
						$this->Session->setFlash(__("Database error"));
					}
				}	
				else
				{
					$this->Session->setFlash(__("Invalid Id"));
				}
				$this->redirect(array("action"=>"new_order_list"));		
			}
	 	}
	 	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		} 	 
	}

///////////////////////////////////////////////////////

	public function admin_order_view($id=NULL)
    {



    	if($id==NULL){
				$msg="REQUIRE PET ID ";
				$this->set(compact('msg'));}
           if( filter_var($id, FILTER_VALIDATE_INT) === false ){
            $datas="";}else{
		$datas=$this->Order->findById($id);}
        $this->set(compact('datas'));

        
        // if($id==NULL)
        // {
        //     throw new NotFoundException(__("Invalid id"));
        // }
       	$order_view=$this->Order->find('first', array(
         	'recursive' => -1,
		 	'contain' => array(
	     		'OrderItem' =>array('Product','Category')
	   		),
	   		'conditions'=>array(
                'Order.id' => $id,
				'Order.is_dispatch' => 1
            ),
      		'order' => array(
                'Order.id' => 'DESC'
            )
        ));
        // if(!$order_view)
        // {
        //     throw new NotFoundException(__("Invalid id"));
        // }
        $this->set("order_view",$order_view);
    }

   /////////////////////////////////////////////////////

	public function admin_new_order_view($id=NULL)
    {		
        if($id==NULL){
				$msg="REQUIRE PET ID ";
				$this->set(compact('msg'));
			}
			if( filter_var($id, FILTER_VALIDATE_INT) === false ){
            $datas="";
              }else{
              $datas=$this->Order->findById($id);
              }
        $this->set(compact('datas'));
        if($this->request->is("post"))
		  {
			$s=$this->Order->findById($id,array("fields"=>"is_dispatch"));
			if($s)
			{
				$status = $this->request->data['status'];
				$s["Order"]["is_dispatch"]= $status;
				$this->Order->id=$id;
				if($this->Order->saveField("is_dispatch",$status))
				{
					  $this->Session->setFlash(__("Status  changed successfully", null), 
                            'default', 
                             array('class' => 'alert data_succesfull_message'));
					$this->redirect(array('controller'=>'registers','action' => 'new_order_list'));
				}
				else{
				$this->Session->setFlash(__("Database error"));
				}
			}
			exit();
		}
       	$new_order_view=$this->Order->find('first', array(
         	'recursive' => -1,
		 	'contain' => array(
	     		'OrderItem' =>array('Product','Category')
	   		),
	   		'conditions'=>array(
                'Order.id' => $id,
				'Order.is_dispatch' =>$datas['Order']['is_dispatch']
            ),
			'order' => array(
                'Order.id' => 'DESC'
            )
        ));
        $this->set("new_order_view",$new_order_view);        
    }

/////////////////////////////////////////////////////////

	public function admin_active_user_list()
	{
		ini_set('memory_limit', '-1');
		$this->layout='default';
	    $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$activity_log_list=$this->Register->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
	    		),
				'conditions'=>array(
					'Register.is_login' =>1
				),
        		'order' => array(
            		'Register.id' => 'DESC'
            	)
        	));
			$this->set(compact('activity_log_list'));
		}
     	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 
	}

////////////////////////////////////////////////////

	public function admin_inactive_user_list()
	{
		ini_set('memory_limit', '-1');
		$this->layout='default';
	    $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$activity_log_list=$this->Register->find('all', array(
		     	'recursive' => -1,
			 	'contain' => array(
		    	),
				'conditions'=>array(
					'Register.is_login' =>0
				),
		    	'order' => array(
		            'Register.id' => 'DESC'
		        )
		    ));
			$this->set(compact('activity_log_list'));
		}
	 	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 
	}

/////////////////////////////////////////////////

	public function admin_user_list()
	{
		ini_set('memory_limit', '-1');
		$this->layout='default';
	    $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$user_list=$this->Register->find('all', array(
		     	'recursive' => -1,
			 	'contain' => array(
		    	),
				'conditions'=>array(
					'Register.email_verified' =>1
				),
		    	'order' => array(
		            'Register.id' => 'DESC'
		        )
		    ));
			$this->set(compact('user_list'));
		}
	 	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 
	}

////////////////////////////////////////////////////////

	public function admin_view_user_activity($user_id=null)
	{
		$this->layout='default';
	    $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$view_user_activity_list=$this->UserActivityLog->find('all', array(
	         	'recursive' => -1,
			 	'contain' => array(
			 		'Register'
		    	),
				'conditions'=>array(
					'UserActivityLog.register_id' =>$user_id,
					'UserActivityLog.is_deleted' =>0
				),
	        	'order' => array(
	                'UserActivityLog.id' => 'DESC'
	            )
	        ));
			$this->set(compact('view_user_activity_list'));
			if($this->request->is("post") or $this->request->is("put")){
				$post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];
				$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));

				$view_user_activity_list=$this->UserActivityLog->find('all', array(
	            	'recursive' => -1,
			    	'contain' => array(
		        		'Register'
		        	),
		        	'conditions' => array(
				    	'and' => array(
							'UserActivityLog.created BETWEEN ? and ?' => array($start_date, $end_date),
	                		'UserActivityLog.register_id' =>$user_id,
							'UserActivityLog.is_deleted' =>0
	             		)
				    ),
	            	'order' => array(
	                	'UserActivityLog.id' => 'DESC'
	             	)
	            ));
				$this->set(compact('view_user_activity_list'));
				$this->set(compact('post_start_date','post_end_date'));
			}
		}
	 	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 
	}

////////////////////////////////////////////

	public function admin_faq_list()
	{
		ini_set('memory_limit', '-1');
		$this->layout='default';
	    $uid = $this->Auth->User();
		if(!empty($uid)){
			$faq_lists=$this->Faq->find('all', array(
		     	'recursive' => -1,
			 	'contain' => array(			 
		   		),
		   		'conditions' => array(
		   			'Faq.is_deleted' => 0
		   		),
				'order' => array(
		        	'Faq.id' => 'DESC'
		    	)
		    ));
			$this->set(compact('faq_lists'));
		}
	   	else{
			$this->redirect(array('controller'=>'operators','action' => 'login'));
		}		
	}

///////////////////////////////////////////////

	public function admin_faq_add()
	{
		if($this->request->is("post"))
		{
		    $this->request->data['Faq']['faq_question']=$this->request->data['faq_question'];
			$this->request->data['Faq']['faq_answer']=$this->request->data['faq_answer'];
			$this->request->data['Faq']['created']=date("Y-m-d H:i:s");			
			$this->request->data['Faq']['modified']=date("Y-m-d H:i:s");
		 	$this->Faq->create(); 
			if ($this->Faq->save($this->request->data)) {
	 			$this->Session->setFlash(__('Data added successfully', null),
	                            'default',
	                             array('class' => 'alert data_succesfull_message'));
				$this->redirect(array('action' => 'admin_faq_list'));
			} 
 			else {
				$this->Session->setFlash(__('The data could not be added', null),
	                            'default',
	                             array('class' => 'alert alertt'));
			} 
		}
	}

//////////////////////////////////////////////////

	public function admin_faq_edit($id=null)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid id"));
			}
			$faq_data=$this->Faq->find('first', array(
         		'recursive' => -1,
		 		'contain' => array(
	    		),
	    		'conditions' => array(
	    			'Faq.id'=>$id
				),
        		'order' => array(
            	)
        	));
			if(!$faq_data)
			{
				throw new NotFoundException(__("Invalid id"));
			}	
			$this->set(compact('faq_data'));
			if($this->request->is("post") or $this->request->is("put"))
			{   
		        $this->Faq->id=$this->request->data['faq_id'];   
			    $this->request->data['Faq']['faq_question']=$this->request->data['faq_question'];
				$this->request->data['Faq']['faq_answer']=$this->request->data['faq_answer'];
				$this->request->data['Faq']['modified']=date("Y-m-d H:i:s");
				if ($this->Faq->save($this->request->data)) {
					$this->Session->setFlash(__('Data has been updated successfully', null),
	                            'default',
	                             array('class' => 'alert data_succesfull_message'));
					$this->redirect(array('action' => 'admin_faq_list'));
   				} 
     			else {
 					$this->Session->setFlash(__('Update unsuccessful', null),
	                            'default',
	                             array('class' => 'alert alertt'));
   				}
			}
  		}
  		else{
			$this->redirect(array('controller'=>'operators','action' => 'login'));
		}
	}

///////////////////////////////////////////////////

	public function admin_faq_view($id=null)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid id"));
			}
			$faq_data=$this->Faq->find('first', array(
         		'recursive' => -1,
		 		'contain' => array(
	    		),
	    		'conditions' => array(
	    			'Faq.id'=>$id
				),
        		'order' => array(
            	)
        	));
			if(!$faq_data)
			{
				throw new NotFoundException(__("Invalid id"));
			}	
			$this->set(compact('faq_data'));
  		}
  		else{
			$this->redirect(array('controller'=>'operators','action' => 'login'));
		}
	}

////////////////////////////////////////////////////

	public function admin_faq_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid id"));
			}
			// if($this->request->is("post"))
			// {
			// 	throw new MethodNotAllowedException();
			// }
			// else
			// {
				$data=$this->Faq->findById($id);
				if(!$data)
			    {
				 throw new NotFoundException(__("Invalid id"));
			    } 
				else
				{
					$this->Faq->id=$id;   
		        	$this->request->data['Faq']['is_deleted']=1;
					if ($this->Faq->save($this->request->data)) {
						$this->Session->setFlash(__('Data has been deleted successfully', null),
	                            'default',
	                             array('class' => 'alert data_succesfull_message'));
   					} 
     				else {
     					$this->Session->setFlash(__('Delete unsuccessful', null),
	                            'default',
	                             array('class' => 'alert alertt'));
   			    	}
				}
			// }
			$this->redirect(array("action"=>"admin_faq_list"));
		}
		else{
			$this->redirect(array('controller'=>'operators','action' => 'login'));
		}
	}

/////////////////////////////////////////////////////////

	public function admin_faq_status_change($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
				$faq_status=$this->Faq->findById($id,array("fields"=>"status"));
				if($faq_status)
				{
					if($faq_status["Faq"]["status"]==1){ 
						$ns=0; 
					}
					else{ 
						$ns=1; 
					}
					$this->Faq->id=$id;
					if($this->Faq->saveField("status",$ns))
					{
						$this->Session->setFlash(__('Status has been changed successfully', null),
	                            'default',
	                             array('class' => 'alert data_succesfull_message'));
					}
					else
					{
						$this->Session->setFlash(__("Sorry, Status could not be changed"));
					}
				}	
				else
				{
					$this->Session->setFlash(__("Invalid Id"));
				}
				$this->redirect(array("action"=>"admin_faq_list"));
			}
 		}
 		else{
			$this->redirect(array('controller'=>'operators','action' => 'login'));
		}
	}

///////////////////////////////////////

	public function faq_list()
	{
		ini_set('memory_limit', '-1');
		//$this->layout = 'user_inner'; 
		$this->layout = 'pet_registration'; 
    	$uid = $this->Auth->User();
    	if(!empty($uid)){
			$user_id = $uid['id'];	
			$datas=$this->Register->findById($uid['id']);
			$this->set('user_details',$datas);		
			$faq_lists=$this->Faq->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(		 
	   			),
	   			'conditions' => array(
	   				'Faq.is_deleted' => 0,
	   				'Faq.status' => 1
	   			),
   				'order' => array(
                	'Faq.id' => 'DESC'
            	)
        	));
			$this->set(compact('faq_lists'));
		}
   		else{
			$this->redirect(array('action' => 'login'));
		}		
	}

////////////////////////////////////////////////////////

	public function admin_tag_replace_request()
	{
		ini_set('memory_limit', '-1');
		$mails=$this->Email->find('first');
		$this->layout='default';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$pet_datas = $this->PetProfile->find('all', array(
				'recursive' => -1,
				'contain'=>array('Register','Product'=>array('conditions' => array(
				 	'Product.active' =>1, 
				 	'Product.isdeleted' =>0,
				 	'Product.tag_replace_request' =>0
				))),
            	'conditions' => array(
				 	'PetProfile.is_active' => 1
            	),
        	));
			$this->set('pet_datas',$pet_datas);
			$state_data= $this->State->find('all', array(
				'contain'=>array(
				),
				// 'conditions' => array(
				// 	'State.country_id'=>1
				// ),
			  	'order' => array('State.id' => 'asc')
			));
			$this->set('state_data',$state_data);
	//// after post

	        if($this->request->is("post"))
			{
		 		$product_code=$this->request->data['tag_id'];	
		 		$product_data=$this->Product->find('first', array(
         			'recursive' => -1,
		 			'contain' => array(		 
	   				),
	   				'conditions' => array(
	   					'Product.product_code' => $product_code,
	   					'Product.isdeleted' => 0,
	   					'Product.active' => 1
	   				),
      				'order' => array(
                		'Product.id' => 'DESC'
            		)
        		));
				$datas=$this->PetProfile->find('first', array(
        			'recursive' => -1,
					'contain' => array(
						'Register'=>array('conditions' => array(
	   						'Register.is_deleted' => 0
	    				))
	    			),
	    			'conditions' => array(
	    				'PetProfile.pet_code' =>$product_code,
						'PetProfile.is_active' =>1
	    			),
        			'order' => array(
                		'PetProfile.id' => 'DESC'
            		)
        		));

				$pet_profile_dtls = $this->PetProfile->find('first', array(
					'recursive' => -1,
					'contain'=>array(
						'Product'=>array('Category')
					),
            		'conditions' => array(
				 		'PetProfile.pet_code' =>$product_code,
				 		'PetProfile.is_active' => 1
            		),
        		));
				$this->set('pet_datas',$pet_datas);
				$this->set('user_details',$datas);
				$state_data= $this->State->find('all', array(
					'contain'=>array(
					),
					// 'conditions' => array(
					// 	'State.country_id'=>1
					// ),
				  	'order' => array('State.id' => 'asc')
				));
				$this->set('state_data',$state_data);
				$current_shipping_address=$this->request->data['shipping_addr'];
				$shipping_city=$this->request->data['shipping_info_city'];
				$shipping_state=$this->request->data['shipping_info_state'];
				$shipping_postal_code=$this->request->data['shipping_info_zip'];
				$shipping_country=$this->request->data['shipping_info_country'];
				$user_id=$datas['PetProfile']['register_id'];
				$user_name=$datas['Register']['fname']." ".$datas['Register']['lname'];
			    $user_mail=$datas['Register']['email'];
			    $this->Product->id=$product_data['Product']['id'];
				$this->request->data['Product']['tag_replace_request']=1;
				$this->request->data['Product']['is_request_processed']=1;


				
				$this->request->data['TagReplacementRequest']['product_id']=$product_data['Product']['id'];
				$this->request->data['TagReplacementRequest']['product_code']=$this->request->data['tag_id'];
				$this->request->data['TagReplacementRequest']['register_id']=$user_id;
				$this->request->data['TagReplacementRequest']['tag_request_date']=$this->request->data['tag_request_date'];
				$this->request->data['TagReplacementRequest']['reason']=$this->request->data['reason'];

				$this->request->data['TagReplacementRequest']['tag_replace_request']=1;
				$this->request->data['TagReplacementRequest']['is_admin']=1;
				$this->request->data['TagReplacementRequest']['shipping_addr']=$this->request->data['shipping_addr'];
				$this->request->data['TagReplacementRequest']['shipping_info_city']=$this->request->data['shipping_info_city'];
				$this->request->data['TagReplacementRequest']['shipping_info_state']=$this->request->data['shipping_info_state'];
				$this->request->data['TagReplacementRequest']['shipping_info_zip']=$this->request->data['shipping_info_zip'];
				$this->request->data['TagReplacementRequest']['shipping_info_country']=$this->request->data['shipping_info_country'];
				$this->request->data['TagReplacementRequest']['created']=date('Y-m-d H:i:s');
				$this->request->data['TagReplacementRequest']['modified']=date('Y-m-d H:i:s');
				$this->TagReplacementRequest->create();

				$this->TagReplacementRequest->save($this->request->data);
				if($this->Product->save($this->request->data))
				{
    				$massage = '';
					$massage.='<!doctype html>
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<!-- NAME: FOLLOW UP -->
		<!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
		<meta charset="UTF-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tag Replace Request</title>
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	/*
	@tab Page
	@section Heading 1
	@style heading 1
	*/
		h1{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:40px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 2
	@style heading 2
	*/
		h2{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:34px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 3
	@style heading 3
	*/
		h3{
			/*@editable*/color:#444444;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:22px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 4
	@style heading 4
	*/
		h4{
			/*@editable*/color:#999999;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:125%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Container Style
	*/
		#templateHeader{
			/*@editable*/background-color:#ea4847;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:50% 50%;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Header
	@section Header Interior Style
	*/
		.headerContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Header
	@section Header Text
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Link
	*/
		.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section Body Container Style
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:20px;
			/*@editable*/padding-bottom:54px;
		}
	/*
	@tab Body
	@section Body Interior Style
	*/
		.bodyContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Body
	@section Body Text
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section Body Link
	*/
		.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Footer
	@section Footer Style
	*/
		#templateFooter{
			/*@editable*/background-color:#333333;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:45px;
			/*@editable*/padding-bottom:63px;
		}
	/*
	@tab Footer
	@section Footer Interior Style
	*/
		.footerContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Footer
	@section Footer Text
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@section Footer Link
	*/
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}
}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}
}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
			max-width:100% !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:30px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:26px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}</style></head>
    <body>
		<!--*|IF:MC_PREVIEW_TEXT|*-->
		<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Tag Replace Request</span><!--<![endif]-->
		<!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="center" valign="top" id="templateHeader" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                
                                    <a href="https://pinpaws.com" title="" class="" target="_blank">
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="192" align="middle">
                                    </a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
							<tr>
								<td align="center" valign="top" id="templateBody" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;" valign="top">
                            <h1><span style="font-size:25px"><strong>New Customer Tag Replacement Request</strong></span></h1>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            Customer:  '.$user_name.'<br>
Pet Name:  '.$pet_profile_dtls['PetProfile']['pet_name'].'<br>
Current Tag ID#:  '.$product_code.'<br>
Tag Color:  '.$pet_profile_dtls['Product']['Category']['name'].'<br>
<br>
Shipping Address:  '.$current_shipping_address.'<br>
<br>
Shipping City:  '.$shipping_city.'<br>
<br>
Shipping State:  '.$shipping_state.'<br>
<br>
Shipping Postal Code:  '.$shipping_postal_code.'<br>
<br>
Shipping Country:  '.$shipping_country.'<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" align="center" valign="top">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" align="center" valign="middle">
                                <a class="mcnButton " title="Click Here to Process Tag Request" href="'.Configure::read('URL').'/admin" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Click Here to Process Tag Request</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                            <table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
                                        <h3 style="text-align: center;">Have Questions?</h3>
<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:'.$mails['Email']['support_email'].'" target="_blank">'.$mails['Email']['support_email'].'</a></div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
                
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                            <tr>
								<td align="center" valign="top" id="templateFooter" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding-left:9px;padding-right:9px;" align="center">
            <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            <em>Copyright Â© '.date("Y").' Pin Paws. All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
16775 Addison Rd., Suite 420 Addison, TX 75001<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
                                       $message1 = '';
									   $message1.='<!doctype html>
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<!-- NAME: FOLLOW UP -->
		<!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
		<meta charset="UTF-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tag Replace Request</title>
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	/*
	@tab Page
	@section Heading 1
	@style heading 1
	*/
		h1{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:40px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 2
	@style heading 2
	*/
		h2{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:34px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 3
	@style heading 3
	*/
		h3{
			/*@editable*/color:#444444;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:22px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 4
	@style heading 4
	*/
		h4{
			/*@editable*/color:#999999;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:125%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Container Style
	*/
		#templateHeader{
			/*@editable*/background-color:#ea4847;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:50% 50%;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Header
	@section Header Interior Style
	*/
		.headerContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Header
	@section Header Text
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Link
	*/
		.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section Body Container Style
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:20px;
			/*@editable*/padding-bottom:54px;
		}
	/*
	@tab Body
	@section Body Interior Style
	*/
		.bodyContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Body
	@section Body Text
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section Body Link
	*/
		.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Footer
	@section Footer Style
	*/
		#templateFooter{
			/*@editable*/background-color:#333333;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:45px;
			/*@editable*/padding-bottom:63px;
		}
	/*
	@tab Footer
	@section Footer Interior Style
	*/
		.footerContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Footer
	@section Footer Text
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@section Footer Link
	*/
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}
}	@media only screen and (max-width: 480px){ 
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}
}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
			max-width:100% !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:26px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}</style></head>
    <body>
		<!--*|IF:MC_PREVIEW_TEXT|*-->
		<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Tag Replace Replace</span><!--<![endif]-->
		<!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="center" valign="top" id="templateHeader" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                    <a href="https://pinpaws.com" title="" class="" target="_blank">
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="192" align="middle">
                                    </a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
							<tr>
								<td align="center" valign="top" id="templateBody" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;" valign="top">
                            <h1 class="null">Your Tag Replacement Request Was Received :)</h1>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            We have received a tag replacement request from your account.&nbsp; We are sad to hear that '.$pet_profile_dtls['PetProfile']['pet_name']."'".'s tag has broken or is lost. Don'."'".'t worry, we will send out a bright and shiny new one as soon as possible!<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" valign="middle" align="center">
                                <a class="mcnButton " title="Click Here to View Your Request Status" href="'.Configure::read('URL').'/login" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Click Here to View Your Request Status</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                            <table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
                                        <h3 style="text-align: center;">Have Questions?</h3>
<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:'.$mails['Email']['support_email'].'" target="_blank">'.$mails['Email']['support_email'].'</a></div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                            <tr>
								<td align="center" valign="top" id="templateFooter" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding-left:9px;padding-right:9px;" align="center">
            <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            <em>Copyright Â© '.date("Y").' Pin Paws. All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
16775 Addison Rd., Suite 420 Addison, TX 75001<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
									$mail=$this->Email->find('first');//*******************
									   $admin_mymail ="support@pinpaws.com";
									   //$admin_mymail ="shrabanti.dhara90@gmail.com";
									   $customer_mymail =$user_mail;
									   $Email = new CakeEmail();
									   $Email->emailFormat('html');
									   $siteName=Configure::read("site_name");
									   $email =$mail['Email']['info_email'];// "info@pinpaws.com";
									   $Email->from(array($email=>$siteName));
									   $Email->to($admin_mymail);
									   $Email->subject($mail['Email']['tag_replace_request']/*"Tag Replacement Request mail from ".$siteName*/);
									   $Email->send($massage);
									   $Email->to($customer_mymail);
									   $Email->subject($mail['Email']['tag_replace_request']/*"Tag Replacement Request mail from ".$siteName*/);
									   $Email->send($message1);
									  // $msg=$this->admin_file('User_Registers_tag_replace_request');
                						// $this->Session->setFlash('Your Replacement Request Has Been Successfully Sent');
                						$this->Session->setFlash(__('Your tag replacement request has been successfully sent', null), 
	                           				 'default', 
	                             array('class' => 'alert data_succesfull_message'));
									   //$this->Session->setFlash(__('Your Request Has Been Successfully Sent'));	
					
    				$this->redirect(array('action' => 'admin_new_req_list'));
   			 } 
				
     		else {
    				// $msg=$this->admin_file('Your Replacement Request Has Been Fail');
        //         	$this->Session->setFlash($msg);

					$this->Session->setFlash(__('Your replacement request has been failed', null),
	                            'default', 
	                             array('class' => 'alert alertt'));
				}
    			   //$this->Session->setFlash(__('Your Request Has Not Been Successfully Sent. Please, try again.'));
   			      
         	
		}		
		 }
       else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		
	}

/////////////////////////////////////////////////////

	public function delete_tag_replace_req($id=NULL,$product_id=null)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$this->TagReplacementRequest->id=$id;
			    $this->request->data["TagReplacementRequest"]["is_deleted"]=1;
				if($this->TagReplacementRequest->save($this->request->data))
				{
					$this->Product->id=$product_id;
			        $this->request->data["Product"]["tag_replace_request"]=0;
			        $this->request->data["Product"]["is_request_processed"]=0;
					if($this->Product->save($this->request->data)){
						$this->Session->setFlash(__("Data  deleted successfully"));

						 array('class' => 'alert data_succesfull_message');
						$this->redirect(array("action"=>"tag_replace_request_list"));
					}
					else
					{
						$this->Session->setFlash(__("Database error"));
						$this->redirect(array("action"=>"tag_replace_request_list"));
					}
				}
				else
				{
					$this->Session->setFlash(__("Database error"));
					$this->redirect(array("action"=>"tag_replace_request_list"));
				}
			}
			$this->redirect(array("action"=>"tag_replace_request_list"));
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

//////////////////////////////////////////////////

	public function tag_replace_request()
	{
		$mails=$this->Email->find('first');
		ini_set('memory_limit', '-1');
		//$this->layout = 'user_inner'; 
		$this->layout = 'pet_registration'; 
	    $uid = $this->Auth->User();
        if(!empty($uid)){
			$user_id = $uid['id'];	
			$datas=$this->Register->findById($uid['id']);

			$user_name=$datas['Register']['fname']." ".$datas['Register']['lname'];
			$user_mail=$datas['Register']['email'];

			$pet_datas = $this->PetProfile->find('all', array(
				'recursive' => -1,
				'contain'=>array('Register','Product'=>array('conditions' => array(
				 	'Product.active' =>1, 
				 	'Product.isdeleted' =>0,
				 	'Product.tag_replace_request' =>0				 
				))),
            	'conditions' => array(
				 	'PetProfile.register_id' => $user_id,
				 	'PetProfile.is_active' => 1
            	),
            	'order' => array('Product.product_code' => 'asc')
        	));
        	//pr($pet_datas);
			$check_out_datas = $this->CheckOut->find('first', array(
				'recursive' => -1,
				'contain'=>array(
				),
            	'conditions' => array(
				 	'CheckOut.register_id' => $user_id,
            	)
        	));
            for($i=0;$i<count($pet_datas);$i++){
			$reqcode=$pet_datas[$i]['PetProfile']['pet_code'];
			$getorderid=$this->OrderItem->find('first', array('conditions'=>array('product_code'=>$reqcode)));
			$order_id_value= $getorderid['OrderItem']['order_id'];
			$order_details = $this->Order->find('first', array('conditions'=>array('id'=>$order_id_value)));
			$newdat=$order_details['Order']['is_dispatch'];
			array_push($pet_datas[$i],"$newdat");
			}
		// pr($pet_datas);	
            $this->set('pet_datas',$pet_datas);
            $this->set('user_details',$datas);
			$this->set('check_out_datas',$check_out_datas);
			$state_data= $this->State->find('all', array(
					'contain'=>array(
					),
					// 'conditions' => array(
					// 	'St                                                                 ate.country_id'=>1
					// ),
				  	'order' => array('State.id' => 'asc')
				));
				$this->set('state_data',$state_data);

// Category
$catlist=$this->Category->find('all');
$this->set('catlist',$catlist);
$prolist=$this->Product->find('all');
$a = array();
for($i=0;$i<=sizeof($catlist);$i++){
    $countpro = $this->Product->find('count', array(
    	'conditions'=>array(
    		'Product.cat_code'=>$catlist[$i]['Category']['cat_code'],
    		'Product.active'=>0,
    		'Product.is_demo'=>0

		)
    ));

array_push($a,$countpro);
}

$this->set('a',$a);

///////After Submit
        	if($this->request->is("post"))
			{
                $req_id=$this->request->data['tag_id'];
                echo $product_code;
                $data=explode(",",$req_id);
                $product_code=$data[0] ;
                $is_dispatchchange=$data[1];

                $product_data=$this->Product->find('first', array(
         			   'recursive' => -1,
		 			   'contain' => array(		 
	   				),
	   				'conditions' => array(
	   					'Product.product_code' => $product_code,
	   					'Product.isdeleted' => 0,
	   					'Product.active' => 1
	   				),
      				    'order' => array(
                		'Product.id' => 'DESC'
            		)
        		));
				$pet_profile_dtls = $this->PetProfile->find('first', array(
					'recursive' => -1,
					'contain'=>array(
						'Product'=>array('Category')
					),
            		'conditions' => array(
				 		'PetProfile.pet_code' =>$product_code,
				 		'PetProfile.is_active' => 1
            		),
        		));
				$current_shipping_address=$this->request->data['shipping_addr'];
				$shipping_city=$this->request->data['shipping_info_city'];
				$shipping_state=$this->request->data['shipping_info_state'];
				$shipping_postal_code=$this->request->data['shipping_info_zip'];
				$shipping_country=$this->request->data['shipping_info_country'];
			    $this->Product->id=$product_data['Product']['id'];
				$this->request->data['Product']['tag_replace_request']=1;
				$this->request->data['Product']['is_request_processed']=1;
                // TagChangeRequest
				$this->request->data['TagReplacementRequest']['product_id']=$product_data['Product']['id'];
	            $this->request->data['TagReplacementRequest']['product_code']=$product_code;
				$this->request->data['TagReplacementRequest']['register_id']=$user_id;
				$this->request->data['TagReplacementRequest']['tag_request_date']=$this->request->data['tag_request_date'];
				$this->request->data['TagReplacementRequest']['tag_replace_request']=1;
				$this->request->data['TagReplacementRequest']['shipping_addr']=$this->request->data['shipping_addr'];
				$this->request->data['TagReplacementRequest']['shipping_info_city']=$this->request->data['shipping_info_city'];
				$this->request->data['TagReplacementRequest']['shipping_info_state']=$this->request->data['shipping_info_state'];
				$this->request->data['TagReplacementRequest']['shipping_info_zip']=$this->request->data['shipping_info_zip'];
				 $this->request->data['TagReplacementRequest']['shipping_info_country']=$this->request->data['shipping_info_country'];
				$this->request->data['TagReplacementRequest']['created']=date('Y-m-d H:i:s');
				$this->request->data['TagReplacementRequest']['modified']=date('Y-m-d H:i:s');
	
	if($is_dispatchchange==1){
		  		$this->request->data['TagReplacementRequest']['change_request']=0;
		  		$this->request->data['TagReplacementRequest']['reason']=$this->request->data['reason'];
    } 
    else if($is_dispatchchange==0 && $is_dispatchchange!= ''){
		    	$this->request->data['TagReplacementRequest']['changeproduct']=$this->request->data['changeproduct'];
		    	$this->request->data['TagReplacementRequest']['requestnote']=$this->request->data['requestnote'];
		    	$this->request->data['TagReplacementRequest']['change_request']=1;
			}
				$this->TagReplacementRequest->create();
				$this->TagReplacementRequest->save($this->request->data);
				if($this->Product->save($this->request->data))
			 	{ 
          
                   $massage = '';
				   $massage.='<!doctype html>
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<!-- NAME: FOLLOW UP -->
		<!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
		<meta charset="UTF-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tag Replace Request</title>
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	/*
	@tab Page
	@section Heading 1
	@style heading 1
	*/
		h1{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:40px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 2
	@style heading 2
	*/
		h2{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:34px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 3
	@style heading 3
	*/
		h3{
			/*@editable*/color:#444444;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:22px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 4
	@style heading 4
	*/
		h4{
			/*@editable*/color:#999999;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:125%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Container Style
	*/
		#templateHeader{
			/*@editable*/background-color:#ea4847;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:50% 50%;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Header
	@section Header Interior Style
	*/
		.headerContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Header
	@section Header Text
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Link
	*/
		.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section Body Container Style
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:20px;
			/*@editable*/padding-bottom:54px;
		}
	/*
	@tab Body
	@section Body Interior Style
	*/
		.bodyContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Body
	@section Body Text
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section Body Link
	*/
		.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Footer
	@section Footer Style
	*/
		#templateFooter{
			/*@editable*/background-color:#333333;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:45px;
			/*@editable*/padding-bottom:63px;
		}
	/*
	@tab Footer
	@section Footer Interior Style
	*/
		.footerContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Footer
	@section Footer Text
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@section Footer Link
	*/
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}
}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}
}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
			max-width:100% !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:30px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:26px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}</style></head>
    <body>
		<!--*|IF:MC_PREVIEW_TEXT|*-->
		<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Tag Replace Request</span><!--<![endif]-->
		<!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="center" valign="top" id="templateHeader" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                
                                    <a href="https://pinpaws.com" title="" class="" target="_blank">
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="192" align="middle">
                                    </a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
							<tr>
								<td align="center" valign="top" id="templateBody" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;" valign="top">
                            <h1><span style="font-size:25px"><strong>New Customer Tag Replacement Request</strong></span></h1>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            Customer:  '.$user_name.'<br>
Pet Name:  '.$pet_profile_dtls['PetProfile']['pet_name'].'<br>
Current Tag ID#:  '.$product_code.'<br>
Tag Color:  '.$pet_profile_dtls['Product']['Category']['name'].'<br>
<br>
Shipping Address:  '.$current_shipping_address.'<br>
<br>
Shipping City:  '.$shipping_city.'<br>
<br>
Shipping State:  '.$shipping_state.'<br>
<br>
Shipping Postal Code:  '.$shipping_postal_code.'<br>
<br>
Shipping Country:  '.$shipping_country.'<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" align="center" valign="top">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" align="center" valign="middle">
                                <a class="mcnButton " title="Click Here to Process Tag Request" href="'.Configure::read('URL').'/admin" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Click Here to Process Tag Request</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                            <table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
                                        <h3 style="text-align: center;">Have Questions?</h3>
<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:'.$mails['Email']['support_email'].'" target="_blank">'.$mails['Email']['support_email'].'</a></div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
                
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                            <tr>
								<td align="center" valign="top" id="templateFooter" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding-left:9px;padding-right:9px;" align="center">
            <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            <em>Copyright Â© '.date("Y").' Pin Paws. All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
16775 Addison Rd., Suite 420 Addison, TX 75001<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
                                       $message1 = '';
									   $message1.='<!doctype html>
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<!-- NAME: FOLLOW UP -->
		<!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
		<meta charset="UTF-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tag Replace Request</title>
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	/*
	@tab Page
	@section Heading 1
	@style heading 1
	*/
		h1{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:40px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 2
	@style heading 2
	*/
		h2{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:34px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 3
	@style heading 3
	*/
		h3{
			/*@editable*/color:#444444;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:22px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 4
	@style heading 4
	*/
		h4{
			/*@editable*/color:#999999;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:125%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Container Style
	*/
		#templateHeader{
			/*@editable*/background-color:#ea4847;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:50% 50%;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Header
	@section Header Interior Style
	*/
		.headerContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Header
	@section Header Text
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Link
	*/
		.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section Body Container Style
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:20px;
			/*@editable*/padding-bottom:54px;
		}
	/*
	@tab Body
	@section Body Interior Style
	*/
		.bodyContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Body
	@section Body Text
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section Body Link
	*/
		.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Footer
	@section Footer Style
	*/
		#templateFooter{
			/*@editable*/background-color:#333333;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:45px;
			/*@editable*/padding-bottom:63px;
		}
	/*
	@tab Footer
	@section Footer Interior Style
	*/
		.footerContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Footer
	@section Footer Text
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@section Footer Link
	*/
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}
}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}
}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
			max-width:100% !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:26px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}</style></head>
    <body>
		<!--*|IF:MC_PREVIEW_TEXT|*-->
		<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Tag Replace Request</span><!--<![endif]-->
		<!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="center" valign="top" id="templateHeader" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                    <a href="https://pinpaws.com" title="" class="" target="_blank">
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="192" align="middle">
                                    </a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
							<tr>
								<td align="center" valign="top" id="templateBody" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;" valign="top">
                            <h1 class="null">Your Tag Replacement Request Was Received :)</h1>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            We have received a tag replacement request from your account.&nbsp; We are sad to hear that '.$pet_profile_dtls['PetProfile']['pet_name']."'".'s tag has broken or is lost. Don'."'".'t worry, we will send out a bright and shiny new one as soon as possible!<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" valign="middle" align="center">
                                <a class="mcnButton " title="Click Here to View Your Request Status" href="'.Configure::read('URL').'/login" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Click Here to View Your Request Status</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                            <table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
                                        <h3 style="text-align: center;">Have Questions?</h3>
<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:'.$mails['Email']['support_email'].'" target="_blank">'.$mails['Email']['support_email'].'</a></div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                            <tr>
								<td align="center" valign="top" id="templateFooter" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding-left:9px;padding-right:9px;" align="center">
            <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            <em>Copyright Â© '.date("Y").' Pin Paws. All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
16775 Addison Rd., Suite 420 Addison, TX 75001<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
/////mail end

			$ip=$this->request->clientIp();
			if($ip!='::1'){
				$mail=$this->Email->find('first');//*******************
				$admin_mymail =$mail['Email']['support_email'];//"support@pinpaws.com";
				$customer_mymail =$user_mail;
				//echo $user_mail; exit();

				$Email = new CakeEmail();
				$Email->emailFormat('html');
				$siteName=Configure::read("site_name");
				$email =$mail['Email']['info_email'];// "info@pinpaws.com";
				$Email->from(array($email=>$siteName));
				$Email->to($admin_mymail);
				$Email->subject($mail['Email']['tag_replace_request']);

				$Email->send($massage);
				$Email->to($customer_mymail);
				$Email->subject($mail['Email']['tag_replace_request']/*"Tag Replacement Request mail from ".$siteName*/);
				$Email->send($message1);
			}

			$this->Session->setFlash(__('Your request has been successfully sent'));
			$this->redirect(array('action' => 'tag_replace_request_list'));
		} 
		
		else {
    
    		$this->Session->setFlash(__('Your request has not been successfully sent. Please, try again.'));
   		} 
         	
	}		
	}
    else{
			$this->redirect(array('action' => 'login'));
		}		
	}

////////////////////////////////////////////////

	public function tag_replace_request_list()
	{
		ini_set('memory_limit', '-1');
		//$this->layout = 'user_inner'; 
		$this->layout = 'pet_registration'; 
    	$uid = $this->Auth->User();
    	if(!empty($uid)){
			$user_id = $uid['id'];	
			$datas=$this->Register->findById($uid['id']);
			$this->set('user_details',$datas);
		 	$data_list=$this->TagReplacementRequest->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
		 			'Register'
	   			),
	   			'conditions' => array(
	   				'TagReplacementRequest.is_deleted' => 0,
	   				'TagReplacementRequest.register_id' =>$user_id
	   			),
      			'order' => array(
                	'TagReplacementRequest.id' => 'DESC'
            	)
        	));
			$this->set('data_list',$data_list);
		}
	   	else{
			$this->redirect(array('action' => 'login'));
		}		
	}

////////////////////// New Change Request//////////////////////////
public function admin_new_changereq_list()
	{

ini_set('memory_limit', '-1');

		$data_list=$this->TagReplacementRequest->find('all', array(
     		'recursive' => -1,
	 		'contain' => array(
	 			'Register'
	 		),
   			'conditions' => array(
   				'TagReplacementRequest.is_deleted' => 0,
   				
                'TagReplacementRequest.change_request' => 1
   				
   			),
  			'order' => array(
            	'TagReplacementRequest.id' => 'DESC'
        	)
    	));
		$this->set('data_list',$data_list);
		if($this->request->is("post") or $this->request->is("put")){
        	$post_start_date=$this->request->data['start_date'];
			$post_end_date=$this->request->data['end_date'];
			$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
			$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));

	 		$data_list=$this->TagReplacementRequest->find('all', array(
     			'recursive' => -1,
	 			'contain' => array(
	 				'Register'
	 			),
   				'conditions' => array(
   					'and' => array(
				 		'TagReplacementRequest.created BETWEEN ? and ?' => array($start_date, $end_date),			
   						'TagReplacementRequest.is_deleted' => 0,
   						
   						'TagReplacementRequest.change_request' => 1
  					)
   				),	
  				'order' => array(
            		'TagReplacementRequest.id' => 'DESC'
        		)
    		));


		 	$this->set('data_list',$data_list);
         	$this->set(compact('post_start_date','post_end_date'));
		}

	}

/////////////////////////////////////////
	public function admin_new_req_list()
	{
		ini_set('memory_limit', '-1');

		$data_list=$this->TagReplacementRequest->find('all', array(
     		'recursive' => -1,
	 		'contain' => array(
	 			'Register'
	 		),
   			'conditions' => array(
   				'TagReplacementRequest.is_deleted' => 0,
   				'TagReplacementRequest.tag_replace_request' => 1,
   				'TagReplacementRequest.change_request' => 0
   			),
  			'order' => array(
            	'TagReplacementRequest.id' => 'DESC'
        	)
    	));
		$this->set('data_list',$data_list);

		if($this->request->is("post") or $this->request->is("put")){
        	$post_start_date=$this->request->data['start_date'];
			$post_end_date=$this->request->data['end_date'];
			$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
			$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));

	 		$data_list=$this->TagReplacementRequest->find('all', array(
     			'recursive' => -1,
	 			'contain' => array(
	 				'Register'
	 			),
   				'conditions' => array(
   					'and' => array(
				 		'TagReplacementRequest.created BETWEEN ? and ?' => array($start_date, $end_date),			
   						'TagReplacementRequest.is_deleted' => 0,
   						'TagReplacementRequest.tag_replace_request' => 1,
   						'TagReplacementRequest.change_request' => 0
  					)
   				),	
  				'order' => array(
            		'TagReplacementRequest.id' => 'DESC'
        		)
    		));
		 	$this->set('data_list',$data_list);
         	$this->set(compact('post_start_date','post_end_date'));
		}
	}

/////////////////////////////////////////////////

	public function admin_sent_req_list()
	{
		ini_set('memory_limit', '-1');
		$data_list=$this->TagReplacementRequest->find('all', array(
	     	'recursive' => -1,
		 	'contain' => array(
		 		'Register'
	   		),
	   		'condition'=>array(

            "AND"=>array(
            	'TagReplacementRequest.is_deleted' => 1,
	   			"OR"=>array(
	   				'TagReplacementRequest.tag_replace_request' => 3,
	   				'TagReplacementRequest.change_request' => 3
	         	)
	   		)  

	),
	  		    'order' => array(
	            'TagReplacementRequest.id' => 'DESC'
	        )
	    ));
		    $this->set('data_list',$data_list);
	}


// $this->Post->find("all",

// 	array('condition'=>array("OR"=>
// 		array('TagReplacementRequest.tag_replace_request' => 3,'TagReplacementRequest.change_request' => 3

// 	)) ));

// 'TagReplacementRequest.is_deleted' => 0,

///////////////////////////////////////////////////////////

	public function admin_delete_tag_replace_req($id=null,$product_id=null)
	{
		if($this->request->is("post"))
		{
			throw new MethodNotAllowedException();
		}
		else
		{
			$this->TagReplacementRequest->id=$id;
		    $this->request->data["TagReplacementRequest"]["is_deleted"]=1;
			if($this->TagReplacementRequest->save($this->request->data))
			{
				$this->Product->id=$product_id;
		        $this->request->data["Product"]["tag_replace_request"]=0;
		        $this->request->data["Product"]["is_request_processed"]=0;
				if($this->Product->save($this->request->data)){
					$this->Session->setFlash(__('Data has been deleted successfully', null), 
	                            'default', 
	                             array('class' => 'alert data_succesfull_message'));
					$this->redirect(array("action"=>"admin_new_req_list"));
				}
				else
				{
					$this->Session->setFlash(__("Database error"));
					$this->redirect(array("action"=>"admin_new_req_list"));
				}
			}
			else
			{
				$this->Session->setFlash(__("Database error"));
				$this->redirect(array("action"=>"admin_new_req_list"));
			}
			
		}
		$this->redirect(array("action"=>"admin_new_req_list"));
	}

public function admin_delete_tag_change_req($id=null,$product_id=null)
	{
		if($this->request->is("post"))
		{
			throw new MethodNotAllowedException();
		}
		else
		{

			$this->TagReplacementRequest->id=$id;
			
			if($this->TagReplacementRequest->delete($this->request->data('TagReplacementRequest.id')))
			{
				$this->Product->id=$product_id;
		        $this->request->data["Product"]["tag_replace_request"]=0;
		        $this->request->data["Product"]["is_request_processed"]=0;
				if($this->Product->save($this->request->data)){
					$this->Session->setFlash(__('Data has been deleted successfully', null), 
	                            'default', 
	                             array('class' => 'alert data_succesfull_message'));
					$this->redirect(array("action"=>"admin_new_changereq_list"));
				}
				else
				{
					$this->Session->setFlash(__("Database error"));
					$this->redirect(array("action"=>"admin_new_changereq_list"));
				}
			}
			else
			{
				$this->Session->setFlash(__("Database error"));
				$this->redirect(array("action"=>"admin_new_changereq_list	"));
			}
 			
				
		}
		$this->redirect(array("action"=>"admin_new_changereq_list"));
	}



	public function admin_delete_changetag_replace_req($id=null,$product_id=null)
	{
		if($this->request->is("post"))
		{
			throw new MethodNotAllowedException();
		}
		else
		{
			$this->TagChangeRequest->id=$id;
		    $this->request->data["TagChangeRequest"]["is_deleted"]=1;
			if($this->TagChangeRequest->save($this->request->data))
			{
				$this->Product->id=$product_id;
		        $this->request->data["Product"]["tag_replace_request"]=0;
		        $this->request->data["Product"]["is_request_processed"]=0;
				if($this->Product->save($this->request->data)){
					$this->Session->setFlash(__('Data has been deleted successfully', null), 
	                            'default', 
	                             array('class' => 'alert data_succesfull_message'));
					$this->redirect(array("action"=>"admin_new_changereq_list"));
				}
				else
				{
					$this->Session->setFlash(__("Database error"));
					$this->redirect(array("action"=>"admin_new_changereq_list"));
				}
			}
			else
			{
				$this->Session->setFlash(__("Database error"));
				$this->redirect(array("action"=>"admin_new_changereq_list"));
			}
			
		}
		$this->redirect(array("action"=>"admin_new_changereq_list"));
	}

///////////////////////////////////////////////////////
	public function admin_check_replace_tag()
	{
		if($this->request->is("post") or $this->request->is("put"))
		{  
			$new_product_code=$this->request->data['product_code'];
			//echo $new_product_code;
			$product_datas=$this->Product->find('first', array(
			           	'recursive' => -1,
			           	'contain'=>array(
				       	),
	                   	'conditions' => array(
					   		'Product.product_code' => $new_product_code,
					   		'Product.isdeleted' => 0,
					   		'Product.active' =>0,
					   		'Product.is_demo'=>0
	                   	),
	                ));

			$product_datasa=$this->Product->find('first', array(
			           	'recursive' => -1,
			           	'contain'=>array(
				       	),
	                   	'conditions' => array(
					   		'Product.product_code' => $new_product_code,
					   		'Product.isdeleted' => 0,
					   		'Product.active' =>1,
					   		'Product.is_demo'=>0
	                   	),
	                ));
			$product_datasd=$this->Product->find('first', array(
			           	'recursive' => -1,
			           	'contain'=>array(
				       	),
	                   	'conditions' => array(
					   		'Product.product_code' => $new_product_code,
					   		'Product.isdeleted' => 0,
					   		'Product.is_demo'=>1
	                   	),
	                ));
			//pr($product_datas);
			if(!empty($product_datas))
			{
				echo 1;
			}
			else if(!empty($product_datasa)){

				echo 2;
			}
			else if(!empty($product_datasd)){

				echo 3;
			}
			else
			{
				echo 0;
			}
		}
		else
		{
			echo 0;
		}
		exit;
	}

/////////////////////////////////////////////////////////Change Tag//////////
public function admin_change_pet_details($id=NULL)
	{
        if( filter_var($id, FILTER_VALIDATE_INT) === false ){
        $datasb="";}else{
        $datasb=$this->PetProfile->findByProductId($id);}
        $this->set(compact('datasb'));
        $oldsolddate=$this->Product->findById($id);
        $olddate=$oldsolddate['Product']['sold_date'];
		$mails=$this->Email->find('first'); 
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$datas=$this->PetProfile->findByProductId($id);
			$product_data_id=$this->Product->findById($id);
			$old_product_code=$product_data_id['Product']['product_code'];
			$product_code_data=$this->TagReplacementHistory->find('first', array(
			    'recursive' => -1,
			    'contain'=>array(
				),
	            'conditions' => array(
					'TagReplacementHistory.product_code' => $old_product_code,
					'TagReplacementHistory.is_deleted' => 0
	            ),
	        ));	
			$pet_data = $this->PetProfile->find('first', array(
				'recursive' => -1,
				'contain'=>array('Register','Product'=>array('Category','TagReplacementRequest'=>array('conditions' => array(
            		)
				))),
            	'conditions' => array(
				 	'PetProfile.id' => $datas['PetProfile']['id']
            	),
        	));
			$customer_name=$pet_data['Register']['fname']." ".$pet_data['Register']['lname'];
			$customer_email=$pet_data['Register']['email'];
			$this->set('pet_data',$pet_data);
//////fetch prefer color and request note
			$datachange=$this->TagReplacementRequest->findByProductId($id);
			$this->set(compact('datachange'));
/////after post
			if($this->request->is("post") or $this->request->is("put"))
			{  
                $new_product_code=$this->request->data['new_tag_id'];
				$product_datas=$this->Product->find('first', array(
		           	'recursive' => -1,
		           	'contain'=>array(
			       	),
                   	'conditions' => array(
				   		'Product.product_code' => $new_product_code,
				   		'Product.isdeleted' => 0,
				   		'Product.active' =>0
                   	)
                ));
            if(!empty($product_datas)){
            		$new_prod_id=$product_datas['Product']['id'];				 
					$this->Product->id=$this->request->data["product_id"];
		    		$this->request->data["Product"]["tag_replace_request"]=$this->request->data["status"];
					$this->request->data["Product"]["is_request_processed"]=$this->request->data["status"];
				if($this->Product->save($this->request->data))
					{  
					$new_product_id=$product_datas['Product']['id'];
					$new_product_code=$product_datas['Product']['product_code'];
					$pet_url=$product_datas['Product']['tags_url'];

//////Tag replacement histories table update
					$this->request->data['TagReplacementHistory']['product_id']=$new_product_id;
					$this->request->data['TagReplacementHistory']['product_code']=$new_product_code;

					if(!empty($product_code_data)){
					$this->request->data['TagReplacementHistory']['parent_product_id']=$product_code_data['TagReplacementHistory']['parent_product_id'];
					$this->request->data['TagReplacementHistory']['parent_product_code']=$product_code_data['TagReplacementHistory']['parent_product_code'];
						}
					else{
					$this->request->data['TagReplacementHistory']['parent_product_id']=$new_product_id;
					$this->request->data['TagReplacementHistory']['parent_product_code']=$new_product_code;	
						}
					$this->request->data['TagReplacementHistory']['replacement_product_id']=$product_data_id['Product']['id'];
					$this->request->data['TagReplacementHistory']['replacement_product_code']=$product_data_id['Product']['product_code'];
					$this->TagReplacementHistory->create();
					$this->TagReplacementHistory->save($this->request->data);
/////Pet Profile table Update
					$this->PetProfile->id=$datas['PetProfile']['id'];
					$this->request->data['PetProfile']['product_id']=$new_product_id;
					$this->request->data['PetProfile']['pet_code']=$new_product_code;
					$this->request->data['PetProfile']['pet_url']=$pet_url;
					$this->PetProfile->save($this->request->data);
/////Payment details table Update
          				$data=$this->PaymentDetail->find('first', array(
         	                           'conditions'=>array(
                                       'product_id'=>$id
         	                                               )));
					$this->PaymentDetail->id=$data['PaymentDetail']['id'];
					$this->request->data['PaymentDetail']['product_id']=$new_product_id;
					$this->PaymentDetail->save($this->request->data);
//////Order items	
						$data2=$this->OrderItem->find('first', array(
					         	       'conditions'=>array(
					                   'product_id'=>$id
			)));
         				$data3=$this->Product->find('first', array(
							           'conditions'=>array(
							           'id'=>$new_product_id
         	)));
						$this->OrderItem->id=$data2['OrderItem']['id'];
						$this->request->data['OrderItem']['product_id']=$new_product_id;
						$this->request->data['OrderItem']['category_id']=$data3['Product']['category_id'];
						$this->request->data['OrderItem']['product_code']=$new_product_code;
						$this->OrderItem->save($this->request->data);
//////payment history
			            $data1=$this->PaymentHistory->find('first', array(
			         	'conditions'=>array(
			            'product_id'=>$id
			         	)));
						$this->PaymentHistory->id=$data1['PaymentHistory']['id'];
						$this->request->data['PaymentHistory']['product_id']=$new_product_id;
						$this->PaymentHistory->save($this->request->data);
/////////// free the tag
						$this->Product->id=$id;
						$this->request->data['Product']['active']=0;
						$this->request->data['Product']['sold_date']=NULL;
						$this->Product->save($this->request->data);
/////////   engage the new tag
						$this->Product->id=$new_product_id;
						$this->request->data['Product']['active']=1;
						$this->request->data['Product']['sold_date']=$olddate;
						$this->Product->save($this->request->data);    

///////update tag replacement request table
						$this->TagReplacementRequest->id=$this->request->data["tag_replacement_request_id"];
						$this->request->data['TagReplacementRequest']['new_product_code']=$this->request->data['new_tag_id'];
						$this->request->data['TagReplacementRequest']['new_product_id']=$new_product_id;
						$this->request->data['TagReplacementRequest']['change_request']=3;
                     if($this->TagReplacementRequest->save($this->request->data)){
			        		 $oldname = UPLOAD_ROOT.'usa/'.$pet_data['Register']['id'].'/pets/'.$old_product_code;
                            $newname = UPLOAD_ROOT.'usa/'.$pet_data['Register']['id'].'/pets/'.$product_datas['Product']['product_code'];
                            rename($oldname, $newname);
					        if($this->request->data["status"]==3){
							   	$this->Product->id=$new_prod_id;
							   	$this->request->data["Product"]["tag_replace_request"]=0;
	                           	$this->request->data["Product"]["is_request_processed"]=0;
			                   	$this->request->data["Product"]["active"]=1;
			                   	$this->Product->save($this->request->data);	   
                               	$massage = '';
							   	$massage.='<!doctype html>
    <html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<!-- NAME: FOLLOW UP -->
		<!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
		<meta charset="UTF-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tag Replace Request</title>
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	/*
	@tab Page
	@section Heading 1
	@style heading 1
	*/
		h1{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:40px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 2
	@style heading 2
	*/
		h2{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:34px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 3
	@style heading 3
	*/
		h3{
			/*@editable*/color:#444444;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:22px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 4
	@style heading 4
	*/
		h4{
			/*@editable*/color:#999999;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:125%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Container Style
	*/
		#templateHeader{
			/*@editable*/background-color:#ea4847;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:50% 50%;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Header
	@section Header Interior Style
	*/
		.headerContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Header
	@section Header Text
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Link
	*/
		.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section Body Container Style
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:20px;
			/*@editable*/padding-bottom:54px;
		}
	/*
	@tab Body
	@section Body Interior Style
	*/
		.bodyContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Body
	@section Body Text
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section Body Link
	*/
		.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Footer
	@section Footer Style
	*/
		#templateFooter{
			/*@editable*/background-color:#333333;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:45px;
			/*@editable*/padding-bottom:63px;
		}
	/*
	@tab Footer
	@section Footer Interior Style
	*/
		.footerContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Footer
	@section Footer Text
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@section Footer Link
	*/
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}
}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}
}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
			max-width:100% !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:26px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}</style></head>
    <body>
		<!--*|IF:MC_PREVIEW_TEXT|*-->
		<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Tag Replace Request</span><!--<![endif]-->
		<!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="center" valign="top" id="templateHeader" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                    <a href="https://pinpaws.com" title="" class="" target="_blank">
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="192" align="middle">
                                    </a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
							<tr>
								<td align="center" valign="top" id="templateBody" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;" valign="top">
                            <h1 class="null">Your Tag Change Request Is Received :)</h1>
                        </td>
                    </tr>
                </tbody></table>
				
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            Your Request has been approved and the new tag has been 
							dispatched at your shipping address!<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
			
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
             
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" valign="middle" align="center">
                                <a class="mcnButton " title="Click Here to View Your Request Status" href="'.Configure::read('URL').'/login" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Click Here to View Your Request Status</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
  
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">
				
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                            <table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
                                        <h3 style="text-align: center;">Have Questions?</h3>
<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:'.$mails['Email']['support_email'].'" target="_blank">'.$mails['Email']['support_email'].'</a></div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									
								</td>
                            </tr>
                            <tr>
	<td align="center" valign="top" id="templateFooter" data-template-container>
									
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
					<tr>
            <td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding-left:9px;padding-right:9px;" align="center">
            <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                <td valign="top" align="center">
                                 
      <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
      <tbody><tr>
        <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
        <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
         <tbody><tr>
            <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                    <a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                </td>
                                        </tr>
                                                </tbody></table>
                                                    </td>
                            </tr>
                                    </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                      
 <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
             <tbody><tr>
                <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
            <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                          <tbody><tr>
                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                        <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
        <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                     <a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                       
     <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
            <tbody><tr>
                <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                <tbody><tr>
                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                    <a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            <em>Copyright Â© '.date("Y").' Pin Paws. All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
16775 Addison Rd., Suite 420 Addison, TX 75001<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									
								</td>
                            </tr>
                        </table>
                       
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';

                                $mail=$this->Email->find('first');//************
									   $mymail = $customer_email;
									   //$mymail ="shrabanti.dhara90@gmail.com";
									   $Email = new CakeEmail();
									   $Email->emailFormat('html');
									   $siteName=Configure::read("site_name");
									   $email = $mail['Email']['info_email'];//"info@pinpaws.com";
									   $Email->from(array($email=>$siteName));
									   $Email->to($mymail);
									   $Email->subject($mail['Email']['tag_replace_request']/*"Tag Replacement Request mail from ".$siteName*/);
									   $Email->send($massage);
									   $this->Session->setFlash(__("Your request has been successfully completed", null), 
                            'default', 
                             array('class' => 'alert data_succesfull_message'));
									  // $this->Session->setFlash(__('Your Request Has Been Successfully Completed'));	
									   }


					/////////////////////////////////////////////
					else{
					$this->Session->setFlash(__("Your request 
						has been successfully completed"));
					}
					}
					else{
					$this->Session->setFlash(__("Your request could Not Be completed.Try again"));	
					}
					$this->redirect(array("action"=>"new_changereq_list"));
				}
				else
				{
					$this->Session->setFlash(__("Database error"));
				}
		}
		else{
			$this->Session->setFlash(__("The new tag id is invalid"));
			$this->redirect(array("action"=>"admin_new_changereq_list"));
		}
		}
	 }
    else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}	 

	}










	public function admin_req_pet_details($id=NULL)
	{

        if( filter_var($id, FILTER_VALIDATE_INT) === false ){
        $datasb="";}else{
        $datasb=$this->PetProfile->findByProductId($id);}
        $this->set(compact('datasb'));


		$mails=$this->Email->find('first'); 
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			
			$datas=$this->PetProfile->findByProductId($id);
			
			$product_data_id=$this->Product->findById($id);
			$old_product_code=$product_data_id['Product']['product_code'];
			$product_code_data=$this->TagReplacementHistory->find('first', array(
			    'recursive' => -1,
			    'contain'=>array(
				),
	            'conditions' => array(
					'TagReplacementHistory.product_code' => $old_product_code,
					'TagReplacementHistory.is_deleted' => 0
	            ),
	        ));	
			$pet_data = $this->PetProfile->find('first', array(
				'recursive' => -1,
				'contain'=>array('Register','Product'=>array('Category','TagReplacementRequest'=>array('conditions' => array(
            		)
				))),
            	'conditions' => array(
				 	'PetProfile.id' => $datas['PetProfile']['id']
            	),
        	));
        	
			$customer_name=$pet_data['Register']['fname']." ".$pet_data['Register']['lname'];
			$customer_email=$pet_data['Register']['email'];
			$this->set('pet_data',$pet_data);



	/////After post	
			if($this->request->is("post") or $this->request->is("put"))
			{  
                $new_product_code=$this->request->data['new_tag_id'];
				$product_datas=$this->Product->find('first', array(
		           	'recursive' => -1,
		           	'contain'=>array(
			       	),
                   	'conditions' => array(
				   		'Product.product_code' => $new_product_code,
				   		'Product.isdeleted' => 0,
				   		'Product.active' =>0
                   	),
                ));
             	if(!empty($product_datas)){
            		$new_prod_id=$product_datas['Product']['id'];				 
					$this->Product->id=$this->request->data["product_id"];
		    		$this->request->data["Product"]["tag_replace_request"]=$this->request->data["status"];
					$this->request->data["Product"]["is_request_processed"]=$this->request->data["status"];
					if($this->Product->save($this->request->data))
					{   
						$new_product_id=$product_datas['Product']['id'];
						$new_product_code=$product_datas['Product']['product_code'];
						$pet_url=$product_datas['Product']['tags_url'];

						$this->request->data['TagReplacementHistory']['product_id']=$new_product_id;
						$this->request->data['TagReplacementHistory']['product_code']=$new_product_code;
						if(!empty($product_code_data)){

						$this->request->data['TagReplacementHistory']['parent_product_id']=$product_code_data['TagReplacementHistory']['parent_product_id'];
						$this->request->data['TagReplacementHistory']['parent_product_code']=$product_code_data['TagReplacementHistory']['parent_product_code'];
						}
						else{
						$this->request->data['TagReplacementHistory']['parent_product_id']=$new_product_id;
						$this->request->data['TagReplacementHistory']['parent_product_code']=$new_product_code;	
						}
						$this->request->data['TagReplacementHistory']['replacement_product_id']=$product_data_id['Product']['id'];
						$this->request->data['TagReplacementHistory']['replacement_product_code']=$product_data_id['Product']['product_code'];
						$this->TagReplacementHistory->create();
						$this->TagReplacementHistory->save($this->request->data);

						$this->PetProfile->id=$datas['PetProfile']['id'];
						$this->request->data['PetProfile']['product_id']=$new_product_id;
						$this->request->data['PetProfile']['pet_code']=$new_product_code;
						$this->request->data['PetProfile']['pet_url']=$pet_url;
						$this->PetProfile->save($this->request->data);

						$this->TagReplacementRequest->id=$this->request->data["tag_replacement_request_id"];
						$this->request->data['TagReplacementRequest']['tag_replace_request']=$this->request->data['status'];
						$this->request->data['TagReplacementRequest']['new_product_code']=$this->request->data['new_tag_id'];
						$this->request->data['TagReplacementRequest']['new_product_id']=$new_product_id;
					$this->request->data['TagReplacementRequest']['tag_replace_request']=3;
			        	if($this->TagReplacementRequest->save($this->request->data)){
			        		 $oldname = UPLOAD_ROOT.'usa/'.$pet_data['Register']['id'].'/pets/'.$old_product_code;

                            $newname = UPLOAD_ROOT.'usa/'.$pet_data['Register']['id'].'/pets/'.$product_datas['Product']['product_code'];
                            rename($oldname, $newname);
					        if($this->request->data["status"]==3){
							   	$this->Product->id=$new_prod_id;
							   	$this->request->data["Product"]["tag_replace_request"]=0;
	                           	$this->request->data["Product"]["is_request_processed"]=0;
			                   	$this->request->data["Product"]["active"]=1;
			                   	$this->Product->save($this->request->data);	   
                               	$massage = '';
							   	$massage.='<!doctype html>
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<!-- NAME: FOLLOW UP -->
		<!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG/>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
		<meta charset="UTF-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tag Replace Request</title>
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	/*
	@tab Page
	@section Heading 1
	@style heading 1
	*/
		h1{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:40px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 2
	@style heading 2
	*/
		h2{
			/*@editable*/color:#222222;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:34px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Page
	@section Heading 3
	@style heading 3
	*/
		h3{
			/*@editable*/color:#444444;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:22px;
			/*@editable*/font-style:normal;
			/*@editable*/font-weight:bold;
			/*@editable*/line-height:150%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:center;
		}
	/*
	@tab Page
	@section Heading 4
	@style heading 4
	*/
		h4{
			/*@editable*/color:#999999;
			/*@editable*/font-family:Georgia;
			/*@editable*/font-size:20px;
			/*@editable*/font-style:italic;
			/*@editable*/font-weight:normal;
			/*@editable*/line-height:125%;
			/*@editable*/letter-spacing:normal;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Container Style
	*/
		#templateHeader{
			/*@editable*/background-color:#ea4847;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:50% 50%;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0px;
			/*@editable*/padding-bottom:0px;
		}
	/*
	@tab Header
	@section Header Interior Style
	*/
		.headerContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Header
	@section Header Text
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Header
	@section Header Link
	*/
		.headerContainer .mcnTextContent a,.headerContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Body
	@section Body Container Style
	*/
		#templateBody{
			/*@editable*/background-color:#ffffff;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:20px;
			/*@editable*/padding-bottom:54px;
		}
	/*
	@tab Body
	@section Body Interior Style
	*/
		.bodyContainer{
			/*@editable*/background-color:#transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Body
	@section Body Text
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/color:#808080;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:16px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:left;
		}
	/*
	@tab Body
	@section Body Link
	*/
		.bodyContainer .mcnTextContent a,.bodyContainer .mcnTextContent p a{
			/*@editable*/color:#00ADD8;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	/*
	@tab Footer
	@section Footer Style
	*/
		#templateFooter{
			/*@editable*/background-color:#333333;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:45px;
			/*@editable*/padding-bottom:63px;
		}
	/*
	@tab Footer
	@section Footer Interior Style
	*/
		.footerContainer{
			/*@editable*/background-color:transparent;
			/*@editable*/background-image:none;
			/*@editable*/background-repeat:no-repeat;
			/*@editable*/background-position:center;
			/*@editable*/background-size:cover;
			/*@editable*/border-top:0;
			/*@editable*/border-bottom:0;
			/*@editable*/padding-top:0;
			/*@editable*/padding-bottom:0;
		}
	/*
	@tab Footer
	@section Footer Text
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-family:Helvetica;
			/*@editable*/font-size:12px;
			/*@editable*/line-height:150%;
			/*@editable*/text-align:center;
		}
	/*
	@tab Footer
	@section Footer Link
	*/
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			/*@editable*/color:#FFFFFF;
			/*@editable*/font-weight:normal;
			/*@editable*/text-decoration:underline;
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}
}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}
}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
			max-width:100% !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}

}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 1
	@tip Make the first-level headings larger in size for better readability on small screens.
	*/
		h1{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 2
	@tip Make the second-level headings larger in size for better readability on small screens.
	*/
		h2{
			/*@editable*/font-size:26px !important;
			/*@editable*/line-height:125% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 3
	@tip Make the third-level headings larger in size for better readability on small screens.
	*/
		h3{
			/*@editable*/font-size:20px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Heading 4
	@tip Make the fourth-level headings larger in size for better readability on small screens.
	*/
		h4{
			/*@editable*/font-size:18px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Boxed Text
	@tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Header Text
	@tip Make the header text larger in size for better readability on small screens.
	*/
		.headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Body Text
	@tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
	*/
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
			/*@editable*/font-size:16px !important;
			/*@editable*/line-height:150% !important;
		}
}	@media only screen and (max-width: 480px){
	/*
	@tab Mobile Styles
	@section Footer Text
	@tip Make the footer content text larger in size for better readability on small screens.
	*/
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			/*@editable*/font-size:14px !important;
			/*@editable*/line-height:150% !important;
		}
}</style></head>
    <body>
		<!--*|IF:MC_PREVIEW_TEXT|*-->
		<!--[if !gte mso 9]><!----><span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">Tag Replace Request</span><!--<![endif]-->
		<!--*|END:IF|*-->
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="center" valign="top" id="templateHeader" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                    <a href="https://pinpaws.com" title="" class="" target="_blank">
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="192" align="middle">
                                    </a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
							<tr>
								<td align="center" valign="top" id="templateBody" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;" valign="top">
                            <h1 class="null">Your Tag Replacement Request Was Received :)</h1>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            Your Request has been approved and the new tag has been 
							dispatched at your shipping address!<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" valign="top" align="center">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" valign="middle" align="center">
                                <a class="mcnButton " title="Click Here to View Your Request Status" href="'.Configure::read('URL').'/login" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Click Here to View Your Request Status</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">
				<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                            <table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
                                        <h3 style="text-align: center;">Have Questions?</h3>
<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:'.$mails['Email']['support_email'].'" target="_blank">'.$mails['Email']['support_email'].'</a></div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				<!--[if gte mso 9]>
				</td>
				<![endif]-->
				<!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                            <tr>
								<td align="center" valign="top" id="templateFooter" data-template-container>
									<!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding-left:9px;padding-right:9px;" align="center">
            <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                <td valign="top" align="center">
                                    <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
			    
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            <em>Copyright Â© '.date("Y").' Pin Paws. All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
16775 Addison Rd., Suite 420 Addison, TX 75001<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				<!--[if mso]>
				</td>
				<![endif]-->
                
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
								</td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
			// msg end
      $ip=$this->request->clientIp();
                  	if($ip!='::1'){
			$mail=$this->Email->find('first');//************
			$mymail = $customer_email;
			$Email = new CakeEmail();
			$Email->emailFormat('html');
			$siteName=Configure::read("site_name");
			$email = $mail['Email']['info_email'];//"info@pinpaws.com";
			$Email->from(array($email=>$siteName));
			$Email->to($mymail);
			$Email->subject($mail['Email']['tag_replace_request']/*"Tag Replacement Request mail from ".$siteName*/);
			$Email->send($massage);
            }
            else{}

			$this->Session->setFlash(__("Your request has been successfully completed", null), 
                            'default', 
                             array('class' => 'alert data_succesfull_message'));
									  // $this->Session->setFlash(__('Your Request Has Been Successfully Completed'));	
									   }


					/////////////////////////////////////////////
					else{
					$this->Session->setFlash(__("Your request 
						has been successfully completed"));
					}
					}
					else{
					$this->Session->setFlash(__("Your request could Not Be completed.Try again"));	
					}
					$this->redirect(array("action"=>"new_req_list"));
				}
				else
				{
					$this->Session->setFlash(__("Database error"));
				}
		}
		else{
			$this->Session->setFlash(__("The new tag id is invalid"));
			$this->redirect(array("action"=>"admin_new_req_list"));
		}
		}
	 }
    else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}	 
		  
	}


	public function admin_test_mail(){
		
$mails=$this->Email->find('first');
    				                   $massage = '';
									   $massage.='<!doctype html>
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		
		<meta charset="UTF-8">
        <meta https-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Tag Replace Request</title>
    <style type="text/css">
		p{
			margin:10px 0;
			padding:0;
		}
		table{
			border-collapse:collapse;
		}
		h1,h2,h3,h4,h5,h6{
			display:block;
			margin:0;
			padding:0;
		}
		img,a img{
			border:0;
			height:auto;
			outline:none;
			text-decoration:none;
		}
		body,#bodyTable,#bodyCell{
			height:100%;
			margin:0;
			padding:0;
			width:100%;
		}
		.mcnPreviewText{
			display:none !important;
		}
		#outlook a{
			padding:0;
		}
		img{
			-ms-interpolation-mode:bicubic;
		}
		table{
			mso-table-lspace:0pt;
			mso-table-rspace:0pt;
		}
		.ReadMsgBody{
			width:100%;
		}
		.ExternalClass{
			width:100%;
		}
		p,a,li,td,blockquote{
			mso-line-height-rule:exactly;
		}
		a[href^=tel],a[href^=sms]{
			color:inherit;
			cursor:default;
			text-decoration:none;
		}
		p,a,li,td,body,table,blockquote{
			-ms-text-size-adjust:100%;
			-webkit-text-size-adjust:100%;
		}
		.ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
			line-height:100%;
		}
		a[x-apple-data-detectors]{
			color:inherit !important;
			text-decoration:none !important;
			font-size:inherit !important;
			font-family:inherit !important;
			font-weight:inherit !important;
			line-height:inherit !important;
		}
		.templateContainer{
			max-width:600px !important;
		}
		a.mcnButton{
			display:block;
		}
		.mcnImage{
			vertical-align:bottom;
		}
		.mcnTextContent{
			word-break:break-word;
		}
		.mcnTextContent img{
			height:auto !important;
		}
		.mcnDividerBlock{
			table-layout:fixed !important;
		}
	
	
		.footerContainer .mcnTextContent a,.footerContainer .mcnTextContent p a{
			
		}
	@media only screen and (min-width:768px){
		.templateContainer{
			width:600px !important;
		}
}	@media only screen and (max-width: 480px){
		body,table,td,p,a,li,blockquote{
			-webkit-text-size-adjust:none !important;
		}
}	@media only screen and (max-width: 480px){
		body{
			width:100% !important;
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImage{
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCartContainer,.mcnCaptionTopContent,.mcnRecContentContainer,.mcnCaptionBottomContent,.mcnTextContentContainer,.mcnBoxedTextContentContainer,.mcnImageGroupContentContainer,.mcnCaptionLeftTextContentContainer,.mcnCaptionRightTextContentContainer,.mcnCaptionLeftImageContentContainer,.mcnCaptionRightImageContentContainer,.mcnImageCardLeftTextContentContainer,.mcnImageCardRightTextContentContainer{
			max-width:100% !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnBoxedTextContentContainer{
			min-width:100% !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupContent{
			padding:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnCaptionLeftContentOuter .mcnTextContent,.mcnCaptionRightContentOuter .mcnTextContent{
			padding-top:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardTopImageContent,.mcnCaptionBlockInner .mcnCaptionTopContent:last-child .mcnTextContent{
			padding-top:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardBottomImageContent{
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockInner{
			padding-top:0 !important;
			padding-bottom:0 !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageGroupBlockOuter{
			padding-top:9px !important;
			padding-bottom:9px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnTextContent,.mcnBoxedTextContentColumn{
			padding-right:18px !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcnImageCardLeftImageContent,.mcnImageCardRightImageContent{
			padding-right:18px !important;
			padding-bottom:0 !important;
			padding-left:18px !important;
		}
}	@media only screen and (max-width: 480px){
		.mcpreview-image-uploader{
			display:none !important;
			width:100% !important;
		}
}	@media only screen and (max-width: 480px){
	
		h1{
		
		}
}	@media only screen and (max-width: 480px){
	
		h2{
			
		}
}	@media only screen and (max-width: 480px){
	
		h3{
			
		}
}	@media only screen and (max-width: 480px){
	
		h4{
			
		}
}	@media only screen and (max-width: 480px){
	
		.mcnBoxedTextContentContainer .mcnTextContent,.mcnBoxedTextContentContainer .mcnTextContent p{
			
		}
}	@media only screen and (max-width: 480px){
	
		
}	@media only screen and (max-width: 480px){
	
		.bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
		}
}	@media only screen and (max-width: 480px){
	
		.footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
			
		}
}</style></head>
    <body>
		
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top" id="bodyCell">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td align="center" valign="top" id="templateHeader" data-template-container>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="headerContainer"><table class="mcnImageBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnImageBlockOuter">
            <tr>
                <td style="padding:9px" class="mcnImageBlockInner" valign="top">
                    <table class="mcnImageContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                        <tbody><tr>
                            <td class="mcnImageContent" style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align:center;" valign="top">
                                
                                    <a href="https://pinpaws.com" title="" class="" target="_blank">
                                        <img alt="" src="https://gallery.mailchimp.com/83159a81ac881c4987476a7f9/images/3d07bd37-de54-43e7-baaf-2f523db329f2.png" style="max-width:192px; padding-bottom: 0; display: inline !important; vertical-align: bottom;" class="mcnImage" width="192" align="middle">
                                    </a>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
    </tbody>
</table></td>
										</tr>
									</table>
								
								</td>
                            </tr>
							<tr>
								<td align="center" valign="top" id="templateBody" data-template-container>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
										<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="bodyContainer"><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
			    
				<td valign="top" width="600" style="width:600px;">
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding: 0px 18px 9px; line-height: 125%;" valign="top">
                            <h1><span style="font-size:25px"><strong>New Customer Tag Replacement Request</strong></span></h1>
                        </td>
                    </tr>
                </tbody></table>
				</td>
				</tr>
				</table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				
				<td valign="top" width="600" style="width:600px;">
				
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" cellspacing="0" cellpadding="0" border="0" align="left" width="100%">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">

                        </td>
                    </tr>
                </tbody></table>
				
				</td>
				
				</tr>
				</table>
				
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 9px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table class="mcnButtonBlock" style="min-width:100%;" cellspacing="0" cellpadding="0" border="0" width="100%">
    <tbody class="mcnButtonBlockOuter">
        <tr>
            <td style="padding-top:0; padding-right:18px; padding-bottom:18px; padding-left:18px;" class="mcnButtonBlockInner" align="center" valign="top">
                <table class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #028090;" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td class="mcnButtonContent" style="font-family: Helvetica; font-size: 18px; padding: 18px;" align="center" valign="middle">
                                <a class="mcnButton " title="Click Here to Process Tag Request" href="'.Configure::read('URL').'/admin" target="_self" style="font-weight: bold;letter-spacing: -0.5px;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;">Click Here to Process Tag Request</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #EAEAEA;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 18px 18px 0px;">
                <table class="mcnDividerContent" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table class="mcnBoxedTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<tbody class="mcnBoxedTextBlockOuter">
        <tr>
            <td class="mcnBoxedTextBlockInner" valign="top">
				<td align="center" valign="top" ">
                <table style="min-width:100%;" class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td style="padding-top:9px; padding-left:18px; padding-bottom:9px; padding-right:18px;">
                            <table class="mcnTextContentContainer" style="min-width: 100% !important;background-color: #FFFFFF;" width="100%" cellspacing="0" cellpadding="18" border="0">
                                <tbody><tr>
                                    <td class="mcnTextContent" style="color: #808080;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;" valign="top">
                                        <h3 style="text-align: center;">Have Questions?</h3>
<div style="text-align: center;">Call us at: <a href="tel:8447467297" target="_blank">844-PIN PAWS</a> &nbsp; |&nbsp;&nbsp; Email us at: <a href="mailto:'.$mails['Email']['support_email'].'" target="_blank">'.$mails['Email']['support_email'].'</a></div>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
				
				
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
								
								</td>
                            </tr>
                            <tr>
								<td align="center" valign="top" id="templateFooter" data-template-container>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
									<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer">
										<tr>
                                			<td valign="top" class="footerContainer"><table class="mcnFollowBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnFollowBlockOuter">
        <tr>
            <td style="padding:9px" class="mcnFollowBlockInner" valign="top" align="center">
                <table class="mcnFollowContentContainer" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding-left:9px;padding-right:9px;" align="center">
            <table style="min-width:100%;" class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody><tr>
                    <td style="padding-top:9px; padding-right:9px; padding-left:9px;" valign="top" align="center">
                        <table cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody><tr>
                                <td valign="top" align="center">
                                   
                                       
                                        <td align="center" valign="top">
                                        
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.facebook.com/pinpawsllc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        
                                        </td>
                                        
                                        <td align="center" valign="top">
                                       
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://www.instagram.com/pinpaws/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                       
                                        </td>
                                        
                                        <td align="center" valign="top">
                                            <table style="display:inline;" cellspacing="0" cellpadding="0" border="0" align="left">
                                                <tbody><tr>
                                                    <td style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer" valign="top">
                                                        <table class="mcnFollowContentItem" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody><tr>
                                                                <td style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;" valign="middle" align="left">
                                                                    <table width="" cellspacing="0" cellpadding="0" border="0" align="left">
                                                                        <tbody><tr>
                                                                                <td class="mcnFollowIconContent" width="24" valign="middle" align="center">
                                                                                    <a href="https://pinpaws.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" style="display:block;" class="" width="24" height="24"></a>
                                                                                </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                                </td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                    
                                    
                                    </tr>
                                    </table>
                                  
                                </td>
                            </tr>
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
            </td>
        </tr>
    </tbody>
</table><table class="mcnDividerBlock" style="min-width:100%;" border="0" width="100%" cellspacing="0" cellpadding="0">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width:100%; padding:18px;">
                <table class="mcnDividerContent" style="min-width: 100%;border-top: 2px solid #505050;" border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table><table class="mcnTextBlock" style="min-width:100%;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td class="mcnTextBlockInner" style="padding-top:9px;" valign="top">
              	<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table style="max-width:100%; min-width:100%;" class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                        <td class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;" valign="top">
                            <em>Copyright Â© '.date("Y").' Pin Paws. All rights reserved.</em><br>
<br>
<strong>Our mailing address is:</strong><br>
16775 Addison Rd., Suite 420 Addison, TX 75001<br>
<br>
<br>
&nbsp;
                        </td>
                    </tr>
                </tbody></table>
				
				</td>
				
				
				</tr>
				</table>
			
            </td>
        </tr>
    </tbody>
</table></td>
										</tr>
									</table>
									</td>
									</tr>
									</table>
								</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>';
									$mail=$this->Email->find('first');//*******************
									   $admin_mymail =$mail['Email']['admin_email'];//"sanket.mukherjee.5@gmail.com";
									   //$admin_mymail ="shrabanti.dhara90@gmail.com";
									   $customer_mymail ='dev.sm01@arctechsols.com';
									   $Email = new CakeEmail();
									   $Email->emailFormat('html');
									   $siteName=Configure::read("site_name");
									   $email = $mail['Email']['info_email'];//"info@pinpaws.com";
									   $Email->from(array($email=>$siteName));
									   $Email->to($admin_mymail);
									   $Email->subject($mail['Email']['tag_replace_request']/*"Tag Replacement Request mail from ".$siteName*/);
									   $Email->send($massage);
									   $Email->to($customer_mymail);
									   $Email->subject($mail['Email']['tag_replace_request']/*"Tag Replacement Request mail from ".$siteName*/);
									   $Email->send($message1);
									  // $msg=$this->admin_file('');
                						$this->Session->setFlash('User_Registers_tag_replace_request');
                						    return $this->redirect(array('action' => 'index'));

	}

//////////////////////////////////////////////////

	public function admin_fetch_shipping_addr(){
		$tag_id=$this->params['data']['tag_id'];
		$datas = $this->PetProfile->find('first', array(
			'recursive' => -1,
			'contain'=>array(
				'Register'
			),
        	'conditions' => array(
				'PetProfile.pet_code' =>$tag_id,
				'PetProfile.is_active' => 1
            ),
        ));
		$check_out_datas = $this->CheckOut->find('first', array(
			'recursive' => -1,
			'contain'=>array(
			),
            'conditions' => array(
				'CheckOut.register_id' => $datas['Register']['id'],
            )
        ));
		echo json_encode($check_out_datas);
	    exit;	
	}

////////////////////////////////////////////////

	public function faq_view($id=null)
	{
		//$this->layout = 'user_inner'; 
			$this->layout = 'pet_registration'; 
		$uid = $this->Auth->User();
        if(!empty($uid)){
			$user_id = $uid['id'];	
			$datas=$this->Register->findById($uid['id']);
			$this->set('user_details',$datas);		
			$faq_id=base64_decode($id);	
			if($faq_id==NULL)
			{
				throw new NotFoundException(__("Invalid id"));
			}
			$faq_data=$this->Faq->find('first', array(
         		'recursive' => -1,
		 		'contain' => array(
	    		),
	    		'conditions' => array(
	    			'Faq.id'=>$faq_id
				),
        		'order' => array(
            	)
        	));
			if(!$faq_data)
			{
				throw new NotFoundException(__("Invalid id"));
			}	
			$this->set(compact('faq_data'));
	  	}
	  	else{
			$this->redirect(array('action' => 'login'));
		}
	}

////////////////////End of 2nd Phase/////////////////////////

	public function admin_user()
	{
		$conditions=array();
		if($this->request->is("post"))
		{
			
			$datas=$this->request->data["Register"];
			foreach($datas as $keys => $values)
			{
				if(!empty($values))
				{
					if($keys=="fdate")
					{
						 
						$conditions[]=" substr(created,1,10) >='".$values."'";
					}
					if($keys=="tdate")
					{
						$conditions[]=" substr(created,1,10) <= '".$values."'";
					}
					if($keys!="tdate" and $keys!="fdate")
					{
						$conditions[]="$keys = '".$values."'";
					}
				}				
			}			
		}
		$this->set("datas",$this->paginate("Register",$conditions));
	}

//////////////////////////////////////////////////////

	public function admin_sellerindex($id = null)
	{
		$conditions=array();
		$conditions[]="operator_id = '".$id."'";
		if($this->request->is("post"))
		{
			
			$datas=$this->request->data["Register"];
			foreach($datas as $keys => $values)
			{
				if(!empty($values))
				{
					if($keys=="fdate")
					{
						 
						$conditions[]=" substr(created,1,10) >='".$values."'";
					}
					if($keys=="tdate")
					{
						$conditions[]=" substr(created,1,10) <= '".$values."'";
					}
					if($keys!="tdate" and $keys!="fdate")
					{
						$conditions[]="$keys = '".$values."'";
					}
				}				
			}			
		}
		$this->set("datas",$this->paginate("Register",$conditions));
	}

//////////////////////////////////////////////////

 	public function admin_add() {
        if ($this->request->is('post')) {
            $this->Register->create();
            if ($this->Register->save($this->request->data)) {
                $this->Session->setFlash('The data has been saved');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('The data could not be saved. Please, try again.');
            }
        }
    }

////////////////////////////////////////////////////

	public function admin_edit($id=NULL)
	{

        if( filter_var($id, FILTER_VALIDATE_INT) === false ){
        $datasb="";}else{
        $datasb=$this->Register->findById($id);}
        $this->set(compact('datasb'));


		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			
			$datas=$this->Register->findById($id);
			
			$customer_datas=$this->Register->findById($id);
			$this->set('customer_datas',$customer_datas);
			if($this->request->is("post") or $this->request->is("put"))
			{        
			 	
                $this->request->data['Register']['id']=$id;
                $this->request->data["Register"]["fname"]=$this->request->data["fname"];
				$this->request->data["Register"]["lname"]=$this->request->data["lname"];
				$this->request->data["Register"]["email"]=$this->request->data["email"];
				$this->request->data["Register"]["phone_no"]=$this->request->data["phone_no"];
				$this->request->data["Register"]["mobile_phone"]=$this->request->data["mobile_phone"];
				$this->request->data["Register"]["address"]=$this->request->data["locationTextField"];
				if($this->Register->save($this->request->data))
				{	
					$this->Session->setFlash(__("Data has been updated successfully", null), 
                        'default', 
                        array('class' => 'alert data_succesfull_message'));
					
					if($customer_datas['Register']['email_verified']==1){
						$this->redirect(array("action"=>"admin_index"));
					}else{
						$this->redirect(array("action"=>"nonsubscribers"));	   
					}
				}
				else
				{
					//$msg=$this->admin_file('Register_Edit_Fail');
					  $msg='Database error';
                	$this->Session->setFlash($msg);
				}			 
			}
		  	$datas["Register"]["dob"]=substr($datas["Register"]["dob"],0,10);
			$this->request->data=$datas;
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////////

	public function admin_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->Register->findById($id);
			 
				if($this->Register->delete($id))
				{
					$order=$this->Order->find('all',array(
					'conditions'=>array(
						'or'=>array('Order.user_id'=>$data['Register']['id'])),
					));
					$subscription=array();
					$i=0;
					foreach ($order as $value) {
						if(!empty($value['Order']['plan_id'])){
							$subscription[$i++]=$value['Order']['plan_id'];
						}
					}
				foreach ($subscription as $value) {
				$status=$this->Stripe->SubscriptionCancel($value);
				}
                 	$this->Session->setFlash(__("This user unsubscribed completely", null), 
                        'default', 
                        array('class' => 'alert data_succesfull_message'));
                }
				else
				{
					//$msg=$this->admin_file('Register_Delete_Fail');
					  $msg='Delete unsuccessful';
	                $this->Session->setFlash($msg);
				}

			 	if($data['Register']['email_verified']==1){
		         	$this->redirect(array("action"=>"admin_index"));
				}else{
					$this->redirect(array("action"=>"nonsubscribers"));		   
				}
			}
			$this->redirect(array("action"=>"admin_index"));
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

/////////////////*/   for subscriber trash */

	public function admin_nonsubscriber_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->Register->findById($id);
			 
				if($this->Register->delete($id))
				{
                 $this->Session->setFlash(__("Data deleted successfully", null), 
                            'default', 
                             array('class' => 'alert data_succesfull_message'));
             	}
				else
				{
					//$msg=$this->admin_file('Register_Delete_Fail');
					  $msg='Deleted unsuccessful';
	                $this->Session->setFlash($msg);
				}
			
			 	if($data['Register']['email_verified']==1){
					$this->redirect(array("action"=>"admin_index"));
				}else{
					$this->redirect(array("action"=>"nonsubscribers"));
				}
			}
			$this->redirect(array("action"=>"admin_nonsubscribers"));
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

///////////////////////////////////////////////////////

	public function admin_status_index($id=NULL)
	{  
	   $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
				$s=$this->Register->findById($id,array("fields"=>"status","name","email","hint"));
			
				if($s)
				{
					if($s["Register"]["status"]==1){ 
						$ns=0; 
					}
					elseif($s["Register"]["status"]==0){ 
						$ns=1; 
					}
					$this->Register->id=$id;
					if($this->Register->saveField("status",$ns))
					{
						
						$this->Session->setFlash(__('Status has been changed successfully', null),
	                            'default',
	                             array('class' => 'alert data_succesfull_message'));
					}
					else
					{
						$this->Session->setFlash(__('Status changed failed', null),
	                            'default',
	                             array('class' => 'alert alertt'));
					}
				}	
				else
				{
					$this->Session->setFlash(__("Invalid Id"));
				}
				$this->redirect(array("action"=>"admin_index"));			
		 	}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////

	public function admin_status_non($id=NULL)
	{  
	   $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
				$s=$this->Register->findById($id,array("fields"=>"status","name","email","hint"));
				if($s)
				{
					if($s["Register"]["status"]==1){ 
						$ns=0; 
					}
					elseif($s["Register"]["status"]==0){ 
						$ns=1; 
					}
					$this->Register->id=$id;
					if($this->Register->saveField("status",$ns))
					{					
						// $this->Session->setFlash(__("Status change successful"));
						$this->Session->setFlash(__('Status has been changed successfully', null), 'default', array('class' => 'alert data_succesfull_message'));
					}
					else
					{
						// $this->Session->setFlash(__("Database error"));
						$this->Session->setFlash(__('Status changed failed', null),'default', array('class' => 'alert alertt'));
					}
				}	
				else
				{
					// $this->Session->setFlash(__("Invalid Id"));
					$this->Session->setFlash(__('Invalid Id', null),'default', array('class' => 'alert alertt'));
				}
				$this->redirect(array("action"=>"nonsubscribers"));			
		 	}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

///////////////////////////////////////////////////////////

	public function admin_deactivate_customer($id=NULL)
	{  
	   	$currentUser = $this->Auth->user();
	    $product_id=$id; 
		if(!empty($currentUser)){
			if($product_id==NULL)
			{
				throw new NotFoundException(__("Invalid Id"));
			}
			else
			{
		 		$datas=$this->OrderItem->find('first', array(
         			'recursive' => -1,
		 			'contain' => array(
		 				'Order'=>array('OrderPaymentHistory')
	     			),
	    			'conditions'=>array(
						'OrderItem.product_id' =>$product_id				
            		),
        			'order' => array(
                		'OrderItem.id' => 'DESC'
            		)
        		));
				$price_of_product=$datas['OrderItem']['price'];
				$order_item_count=$datas['Order']['order_item_count'];
				$subtotal=$datas['Order']['subtotal'];
				$total=$datas['Order']['total'];
				$status=$this->PetProfile->find('all',array(
	        		"fields"=>array("cancellations_date","is_cancellations","id"),
	        		"conditions" => array(
	        			"product_id"=>$product_id,
	        			"is_cancellations"=>0,
	        		)
	        	));
		    	if($status)
				{
					$ns=1;
					$date = date("Y-m-d h:i:sa");
					$this->request->data['is_cancellations']= $ns;
					$this->request->data['cancellations_date']=$date;
					$this->PetProfile->id=$status[0]['PetProfile']['id'];
					if($this->PetProfile->save($this->request->data))
					{
						$payment_detail=$this->PaymentDetail->find('all', array(
								'recursive' => -1,
								'conditions' => array(
							'PaymentDetail.pet_profile_id'=>$status[0]['PetProfile']['id'],
							'PaymentDetail.is_active'=>1,
							'PaymentDetail.isdeleted'=>0
								),
						'order' => array('PaymentDetail.id' => 'desc')
							));
						$this->request->data['PaymentDetail']['id']=$payment_detail[0]['PaymentDetail']['id'];
						$this->request->data['PaymentDetail']['is_active']=0;
						$this->request->data['PaymentDetail']['isdeleted']=1;
						
						if($this->PaymentDetail->save($this->request->data)){

						$subscription_id=$datas['OrderItem']['subscription_id'];
						$refund=array();
						$refund['subscription_plan']=$datas['Order']['plan_id'];
						$refund['refund_amount']=$datas['OrderItem']['subtotal']*100;
						$refund['is_dispatch']=$datas['Order']['is_dispatch'];
						$refund['product_id']=$datas['OrderItem']['product_id'];
						$stripe_data=$this->StripeData->find('first',array(
							'conditions'=>array(
								'StripeData.subscription_id like'=>$refund['subscription_plan'],
								'StripeData.type like'=>"invoice.payment_succeeded",
							)));
						$refund['charge_id']=$stripe_data['StripeData']['charge_id'];
						if($refund['is_dispatch']==0)
						{
							$product_data=$this->Product->findById($refund['product_id']);
							$this->request->data['Product']['id']=$refund['product_id'];
							$this->request->data['Product']['active']=0;
							$this->request->data['Product']['sold_date']=null;
							$this->Product->save($this->request->data);
							$pet_profile=$this->PetProfile->find('first',array(
								'conditions'=>array(
									'PetProfile.product_id'=>$refund['product_id'],
								)));
							//$this->PetProfile->delete($pet_profile['PetProfile']['id']);
							if(!empty($refund['charge_id'])){
							//	echo 1;
								$refund_stripe=$this->Stripe->refund_Stripe($refund);
							//	pr($refund_stripe);
							//	echo 2;
							}
						}
						if($subscription_id!=NULL || !empty($subscription_id))
						{
							$status=$this->Stripe->SubscriptionItemCancel($subscription_id);
							if($status==1)
							{
								$this->Session->setFlash(__('Tag deactivated successfully', null), 
	                            'default', 
	                             array('class' => 'alert data_succesfull_message'));
							}
							if($status==0)
							{
								$result=$this->Stripe->SubscriptionItemRetrive($subscription_id);
								if(isset($result['sub_id']))
								{
									$result=$this->Stripe->customerRetrieveSubscriptions($result['sub_id']);
									if(sizeof($result['items'])==1)
									{
										$cancel_subscription=$this->Stripe->SubscriptionCancel($result['subscription_id']);
										if($cancel_subscription==1)
										{
											$this->Session->setFlash(__('Tag deactivated successfully', null), 
							                            'default', 
							                             array('class' => 'alert data_succesfull_message'));
										}
										else
										{
											$this->Session->setFlash(__('Account successfully deactivated but not deleted from stripe', null), 
							                            'default', 
							                             array('class' => 'alert alertt'));
										}
									}
								}
								else
										{
											$this->Session->setFlash(__('Account successfully deactivated but not deleted from stripe', null), 
							                            'default', 
							                             array('class' => 'alert alertt'));
										}
							}
						}
						else
						{
							$this->Session->setFlash(__('Account successfully deactivated', null), 
	                            'default', 
	                             array('class' => 'alert data_succesfull_message'));
						}						
						}
					}
					else
					{
						// $message="Account Could Not Be Deactivated"	;
						$this->Session->setFlash(__('Account could not be deactivated', null),
	                            'default', 
	                             array('class' => 'alert alertt'));
					}					
				}
				if (!empty($datas['OrderPaymentHistory'])) {
					$token=$datas['OrderPaymentHistory'][0]['stripe_id'];
					$result_plan_retrieve = $this->Stripe->planRetrieveDlete($token);
					if(!empty($result_plan_retrieve)){
			           	$data=$this->Register->findById($id);
						if($this->Register->delete($id)){
							$this->Session->setFlash(__("Customer deactivated successfully"));
						}			
					}			
					else
					{
						$this->Session->setFlash(__("Customer could not be deactivated"));
					}
				}		
				$this->redirect(array("action"=>"admin_req_deactive_tag_list"));
			}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////////

	private function random($n)
	{
		$a="";
		for ($i = 0; $i<$n; $i++) 
		{
			$a .= mt_rand(0,9);
		}
		return $a;	
	}

////////////////////////////////////////////////////

	public function getRegisterName($id)
	{
		 
		if($id==0)
		{
			return "Administrator";
		}
		else
		{
			$data=$this->Register->findById($id);
			if($data)
			{
				return $data["Register"]["me"]." ".$data["Register"]["name"];
			}
			else
			{
				return "Unknown";
			}
		}
	}

//////////////////////////////////////////////////

	public function admin_download()
	{
		$this->set("orders", $this->Register->find('all'));
		$this->layout = null;
		$this->autoLayout = false;
		Configure::write('debug', '0');
	}

////////////////////////////////////////////////

	public function admin_today()
	{
		$conditions=array();
		$registerOptions=array();
		$registerOptions[]=$this->Register->find("list",array("fields"=>array("Register.id","Register.name")));
		$registerOptions["0"]["0"]="Select Name";
		$this->set("registerOptions",$registerOptions);
		$conditions[]=" substr(Register.created,1,10)='".date("Y-m-d")."'";
		$registerData=$this->paginate("Register",$conditions);
		$this->set("datas",$registerData);
	}

////////////////////////////////////////////////

	public function admin_user_today()
	{
		$conditions=array();
		$registerOptions=array();
		$registerOptions[]=$this->Register->find("list",array("fields"=>array("Register.id","Register.name")));
		$registerOptions["0"]["0"]="Select Name";
		$this->set("registerOptions",$registerOptions);
		$conditions[]=" substr(Register.created,1,10)='".date("Y-m-d")."'";
		$registerData=$this->paginate("Register",$conditions);
		$this->set("datas",$registerData);
	}

//////////////////////////////////////////////////

 	public function imageUpload($data,$field)
	{
		$support_file=array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png");
		
		if(!empty($data[$field]["name"]))
		{
			if($data[$field]["error"]==0)
			{
				if(in_array($data[$field]["type"],$support_file))
				{
					$image= time().$data[$field]["name"];
					if(!move_uploaded_file($data[$field]["tmp_name"],"images/original/".$image))
					{
						$this->Session->setFlash(__("Image upload failed"));
						return false;
					}
					else
					{
						//create temp image
						copy("images/original/".$image,"images/large/".$image);
						$this->resize("images/large/".$image, 200, 200);
						copy("images/original/".$image,"images/small/".$image);
						$this->resize("images/small/".$image, 50, 50);
						copy("images/original/".$image,"images/medium/".$image);
						$this->resize("images/medium/".$image, 463, 414);
						
						return $image;
					}
				}
				else
				{
					$this->Session->setFlash(__("Image file is not valid "));
					return false;
				}
			}
			else
			{
				$this->Session->setFlash(__("Image file error."));
				return false;
			}				
		}
		else
		{
			return false;
		}
	}

////////////////////////////////////////////////////
 //image resize function
	private function resize($imagePath, $destinationWidth, $destinationHeight) { 
        // The file has to exist to be resized 
        if(file_exists($imagePath)) { 
            // Gather some info about the image 
            $imageInfo = getimagesize($imagePath); 
             
            // Find the intial size of the image 
            $sourceWidth = $imageInfo[0]; 
            $sourceHeight = $imageInfo[1]; 
			
			
			if($sourceWidth > $sourceHeight)
			{
				$ratio=$sourceHeight/$sourceWidth;
				$destinationHeight=round($destinationWidth*$ratio);
			}
			elseif($sourceWidth < $sourceHeight)
			{
				$ratio=$sourceWidth/$sourceHeight;
				$destinationWidth=round($destinationHeight*$ratio);
			}
          
            // Find the mime type of the image 
            $mimeType = $imageInfo['mime']; 
             
            // Create the destination for the new image 
            $destination = imagecreatetruecolor($destinationWidth, $destinationHeight); 

            // Now determine what kind of image it is and resize it appropriately 
            if($mimeType == 'image/jpeg' || $mimeType == 'image/pjpeg') { 
                $source = imagecreatefromjpeg($imagePath); 
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $destinationWidth, $destinationHeight, $sourceWidth, $sourceHeight);
                imagejpeg($destination, $imagePath); 
            } else if($mimeType == 'image/gif') { 
                $source = imagecreatefromgif($imagePath); 
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $destinationWidth, $destinationHeight, $sourceWidth, $sourceHeight);
                imagegif($destination, $imagePath); 
            } else if($mimeType == 'image/png' || $mimeType == 'image/x-png') { 
                $source = imagecreatefrompng($imagePath); 
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $destinationWidth, $destinationHeight, $sourceWidth, $sourceHeight);
                imagepng($destination, $imagePath); 
            } else { 
                $this->_error('This image type is not supported.'); 
            } 
             
            // Free up memory 
            imagedestroy($source); 
            imagedestroy($destination); 
        } else { 
            $this->_error('The requested file does not exist.'); 
        } 
    } 

//////////////////////////////////////////////////

    // Outputs errors... 
    private function _error($message) { 
        trigger_error('The file could not be resized for the following reason: ' . $message); 
    } 
	
///////////////////////////////////////////////////

	public function product_details()
	{
	 	$pro_id = $_POST['pro_id'];
		$newproducts=$this->Product->find("first",array("conditions"=>array("id"=>$pro_id),'fields' => array(
                'Product.id',
                'Product.name',
				'Product.product_code',
                'Product.price',
				'Product.offer_price'
            )
		));
		$name = $newproducts['Product']['name'];
		if(!empty($newproducts['Product']['offer_price'])){
		$price = $newproducts['Product']['price']-($newproducts['Product']['price'] * $newproducts['Product']['offer_price']/100);	
		}else{
		$price = $newproducts['Product']['price'];
		}
		echo $name."###".$price; exit;
	}

/////////////////////////////////////////////////////

	public function tour_list_seller()
	{
		$this->set('title_for_layout',"tour");
		$this->set('description_for_layout',"tour");
		$this->set('keywords_for_layout',"tour");
		$this->layout = 'saller_inner'; 
		$currentUser = $this->Auth->user();
		
		$user_id = $currentUser['id'];
		$isdeleted = 0;
		$datas=$this->Register->findById($user_id);
		$this->set('user_details',$datas);
		$products = $this->Product->find('all', array(
            'recursive' => -1,
            'contain' => array(
            ),
            'fields' => array(
                'Product.id',
                'Product.name',
				'Product.product_code',
				'Product.price',
                'Product.offer_price',
				'Product.special',
                'Product.modified',
				'Product.created',
                'Product.image',
				'Product.isdeleted'
            ),
            'conditions' => array(
                'Product.operator_id' => $user_id,
				'Product.isdeleted'=>0
            ),              
        ));
		$this->set('products_data',$products);			
	}
	
////////////////////////////////////////////////////////

	public function business()
	{
		$this->set('title_for_layout',"tour");
		$this->set('description_for_layout',"tour");
		$this->set('keywords_for_layout',"tour");
		$this->layout = 'saller_inner'; 
		$currentUser = $this->Auth->user();
		
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		$this->set('user_details',$datas);
		$products = $this->Product->find('all', array(
            'recursive' => -1,
            'contain' => array(
            ),
            'fields' => array(
                'Product.id',
                'Product.name',
				'Product.product_code',
                'Product.image'
            ),
            'conditions' => array(
                'Product.operator_id' => $user_id,
				'Product.isdeleted' => 0
            ),              
        ));
		$this->set('products_data',$products);		
	}
	
//////////////////////////////////////////////////

	public function seller_reviews()
	{
		$this->set('title_for_layout',"tour");
		$this->set('description_for_layout',"tour");
		$this->set('keywords_for_layout',"tour");
		$this->layout = 'saller_inner'; 
		$currentUser = $this->Auth->user();
		
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		$this->set('user_details',$datas);
		$products = $this->Product->find('all', array(
            'recursive' => -1,
            'contain' => array(
            ),
            'fields' => array(
                'Product.id',
                'Product.name',
				'Product.product_code',
                'Product.image'
            ),
            'conditions' => array(
                'Product.operator_id' => $user_id,
				'Product.isdeleted' =>0
            ),              
        ));
		$this->set('products_data',$products);			
	}
	
//////////////////////////////////////////////////////

	public function sponsored_listing()
	{
		$this->set('title_for_layout',"tour");
		$this->set('description_for_layout',"tour");
		$this->set('keywords_for_layout',"tour");
		$this->layout = 'saller_inner'; 
		$currentUser = $this->Auth->user();
		
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		$this->set('user_details',$datas);
		$products = $this->Product->find('all', array(
            'recursive' => -1,
            'contain' => array(
            ),
            'fields' => array(
                'Product.id',
                'Product.name',
				'Product.product_code',
                'Product.image'
            ),
            'conditions' => array(
                'Product.operator_id' => $user_id,
				'Product.isdeleted' =>0
            ),               
        ));
		$this->set('products_data',$products);			
	}

/////////////////////////////////////////////////

 	public function view_user($id=NULL)
	{
		$this->layout = 'user';
		if($id==NULL)
		{
			throw new NotFoundException(__("Invalid id"));
		}
		$currentUser = $this->Auth->user();
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		$dropdown_page = array('view_user');
		$this->set('dropdown_page',$dropdown_page);
		if($user_id==$id){
			if(!$datas)
			{
				throw new NotFoundException(__("Invalid id"));
			}
			$datas["Operator"]["password"]=$datas["Register"]["hint"];
			$this->request->data=$datas;
			$this->set('datas',$datas);			
	   	}else{
		    throw new NotFoundException(__("Invalid id"));		   
		}
	}

//////////////////////////////////////////////////

	public function update_user_view()
	{  
		if($this->request->is("post"))
		{	       
			$id = $this->request->data["user_id"];
			$firstname = $this->request->data["firstname"];
			$lastname = $this->request->data["lastname"];
			$name = $firstname." ".$lastname;
			$this->request->data["Register"]["name"]=$name;
			$this->request->data["Register"]["email"]=$this->request->data["email"];					
			$this->request->data["Register"]["gender"]=$this->request->data["gender"];				
			if(!empty($this->request->data["phone_no"])){  
				$this->request->data["Register"]["phone_no"]=$this->request->data["phone_no"];
			}
			else{
			 	$this->request->data["Register"]["phone_no"]='xxxxxxxxxx';
			}
			if(!empty($this->request->data["city"])){
				$this->request->data["Register"]["city"]=$this->request->data["city"];
			}
			if(!empty($this->request->data["address"])){
				$this->request->data["Register"]["address"]=$this->request->data["address"];
			}
			if(!empty($this->request->data["weknow_about"])){
				$this->request->data["Register"]["weknow_about"]=$this->request->data["weknow_about"];
			}
			$this->Register->id=$id;
			if($this->Register->save($this->request->data))
			{
				$this->Session->setFlash(__("Details updated successfully..."));
				$this->redirect(array("controller"=>"registers","action"=>"view_user/$id"));
			}
			else
			{
			 	$this->Session->setFlash(__("Please try again"));
			}
		}
	}

//////////////////////////////////////////////////////

	public function update_user_password()
	{ 
		if($this->request->is("post"))
		{	       
			$id = $this->request->data["user_id1"];
			$this->request->data["Register"]["hint"]=$this->request->data["password"];						  
			$this->request->data["Register"]["password"]=$this->request->data["password"];	

			$this->Register->id=$id;
			if($this->Register->save($this->request->data))
			{
				$this->Session->setFlash(__("Details updated successfully..."));
				$this->redirect(array("controller"=>"registers","action"=>"view_user/$id"));
			}
			else
			{
				$this->Session->setFlash(__("Please try again"));
			}						 
		}
	}

///////////////////////////////////////////////////////

	public function edit_profile_pic_user()
	{ 
		if($this->request->is("post"))
		{	       
			$id = $this->request->data["user_id_for_pic"];
			if($this->request->form["picture_upload_founder"]["name"])
			{
				$image=$this->imageUpload($this->request->form,"picture_upload_founder");
		  		if($image)
				{
					$flag=1;
					$preimage=$this->request->data["founder_img"];
					$this->request->data["founder_img"]=$image;
					
					if($preimage!=''){
						if(file_exists("images/small/$preimage"))
						{
							unlink("images/original/$preimage");
							unlink("images/small/$preimage");
							unlink("images/large/$preimage");						
						}
					}
				}
				else
				{
					$flag=0;
				}
			}
			else
			{
				unset($this->request->data["picture_upload_founder"]);
				$flag=1;
			}
							 
			$this->Register->id=$id;
			if($this->Register->save($this->request->data))
			{
				$this->Session->setFlash(__("Details updated successfully..."));
				$this->redirect(array("controller"=>"registers","action"=>"view_user/$id"));
			}
			else
			{
				$this->Session->setFlash(__("Please try again"));
			}						 
		}
	}

////////////////////////////////////////////////

	public function user_overview()
	{  
	    $this->layout = 'user';
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$user_id = $currentUser['id'];
			$datas=$this->Register->findById($user_id);
			$this->set('datas',$datas);
			$dropdown_page = array('user_overview');
			$this->set('dropdown_page',$dropdown_page);
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		
	}

/////////////////////////////////////////////

	public function overview_user()
	{
		$this->set('title_for_layout',"User");
		$this->set('description_for_layout',"User");
		$this->set('keywords_for_layout',"User");
		$this->layout = 'pet_registration'; 
		$currentUser = $this->Auth->user();
		$this->set('currentUser',$currentUser);
		$this->set('user_id',$currentUser['id']);
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($currentUser['id']);
		$this->set('user_details',$datas);
		if(!empty($user_id)){
			$petdetails = $this->Register->find('first', array(
            	'contain' => array('PetProfile'=>array('Product'=>array('Category'))),
            	'conditions' => array(
            	  	'AND'=>array(
                		'Register.id' => $currentUser['id'],
					),
            	),	
            	'order' => array(
            	),
        	));
    		$this->set('petdetails',$petdetails);
			$view_recentscan = $this->RecentScan->find('all', array(
            	'contain' => array(			
            	),
            	'conditions' => array(
                	'RecentScan.user_id' => $currentUser['id']
            	),
				'order' => array(
                    'RecentScan.id' => 'DESC'
                ),
			  	'limit' => 6,
        	));
			$this->set(compact('view_recentscan'));
			$session_id=$this->Session->read('browser_session');
			if(count($petdetails)>0 && $this->Session->read('new_user')==0){
				$cart_details = $this->Cart->find('all', array(
					'contain'=>array(
					'Category'
					),
					'conditions' => array(
						'Cart.sessionid'=>$session_id,
						'is_deleted' => 0
					),
					'order' => array('Cart.id' => 'desc')
				));	
				$count=count($cart_details);
				$i=0;
				$monthly_subtotal_cost=0;
				foreach($cart_details as $cart_detail){
					if($i==$count-1){
						$monthly_subtotal_cost=($cart_detail['Cart']['additional_price']*$cart_detail['Cart']['quantity']);
						
						$this->loadModel('Cart'); //or you can load it in beforeFilter()
						$data=$this->Cart->query('UPDATE `carts` SET `monthly_subtotal`='.$monthly_subtotal_cost.' WHERE id='.$cart_detail['Cart']['id']);
					}
					$i++;
				}
			}	
		}else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}			
	}

////////////////////////////////////////////////////

	public function activation_deactivation()
	{
		$this->set('title_for_layout',"User");
		$this->set('description_for_layout',"User");
		$this->set('keywords_for_layout',"User");
		//$this->layout = 'user_inner'; 
		$this->layout = 'pet_registration'; 
		$currentUser = $this->Auth->user();
		$this->set('currentUser',$currentUser);
		$this->set('user_id',$currentUser['id']);
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($currentUser['id']);
		$this->set('user_details',$datas);
		if(!empty($user_id)){
			$activity_name="Click To AccountActivation/Deactivation";
        	$activity_date=date("Y-m-d H:i:s");
        	$this->user_activity_log($activity_name,$activity_date);	
			$petdetails = $this->Register->find('first', array(
				'recursive' => -1,
            	'contain' => array('PetProfile'=>array('Product'=>array('Category'))),
            	'conditions' => array(
					'AND'=>array(
                		'Register.id' => $currentUser['id'],
					)
            	),
            	'order' => array(              
            	),
        	));

    		$this->set('petdetails',$petdetails);
			$view_recentscan = $this->RecentScan->find('all', array(
            	'contain' => array(
            	),
            	'conditions' => array(
                	'RecentScan.user_id' => $currentUser['id']
            	),
				'order' => array(
                    'RecentScan.id' => 'DESC'
                ),
			  	'limit' => 6,
        	));
        	$this->set(compact('view_recentscan'));	
		}else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

//////////////////////////////////////////////////////

	public function deactivate_account(){
		$flag=0;	
		$product_code=$this->params['data']['product_code'];
		$pet_code=base64_decode($product_code);
		$petdetails = $this->PetProfile->find('first', array(
			'recursive' => -1,
			'contain'=>array('Product'=>array('Category')),
				'conditions' => array(
					'PetProfile.pet_code' => $pet_code,
					'PetProfile.is_active' => 1,
				),
		));
		$pet_id=$petdetails['PetProfile']['id'];
		$status=$this->PetProfile->findById($pet_id,array(
			"fields"=>"is_active","cancellations_request_date"
		));
		if($status)
		{
			$ns=0;
			$date = date("Y-m-d h:i:sa");
			$this->PetProfile->id=$pet_id;
			if($this->PetProfile->saveField("is_active",$ns) && $this->PetProfile->saveField("cancellations_request_date",$date))
			{
				$message="Deactivation request has been sent"	;
			}
			else
			{
				$message="Account Could Not Be Deactivated"	;
			}
        }
		echo $message;
		exit;
    }

///////////////////////////////////////////////////////////

	public function pet_list()
	{
		$this->set('title_for_layout',"User");
		$this->set('description_for_layout',"User");
		$this->set('keywords_for_layout',"User");
		//$this->layout = 'user_inner';
		$this->layout = 'pet_registration'; 
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$this->set('currentUser',$currentUser);
			$this->set('user_id',$currentUser['id']);
			$user_id = $currentUser['id'];
			$datas=$this->Register->findById($currentUser['id']);
			$this->set('user_details',$datas);
		
			$petdetails = $this->Register->find('first', array(
				'recursive' => -1,
             	'contain' => array('PetProfile'=>array('Product'=>array('Category'))),
            	'conditions' => array(
					'AND'=>array(
                		'Register.id' => $currentUser['id'],
					)
            	),
            	'order' => array(              
            	),
        	));

    		$this->set('petdetails',$petdetails);
		}
   		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		
	}

//////////////////////////////////////////////////////

	public function my_booking()
	{  
	    $this->layout = 'user';
		$currentUser = $this->Auth->user();
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		$this->set('datas',$datas);
		$dropdown_page = array('my_booking');
		$this->set('dropdown_page',$dropdown_page);
		
	}

/////////////////////////////////////////////////////

	public function my_wallet()
	{  
	    $this->layout = 'user';
		$currentUser = $this->Auth->user();
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		$this->set('datas',$datas);
		$dropdown_page = array('my_wallet');
		$this->set('dropdown_page',$dropdown_page);
		
	}

//////////////////////////////////////////////////

	public function complaints()
	{  
	    $this->layout = 'user';
		$currentUser = $this->Auth->user();
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		$this->set('datas',$datas);
		$dropdown_page = array('complaints');
		$this->set('dropdown_page',$dropdown_page);
		
	}

//////////////////////////////////////////////////

	public function cancel_bookings()
	{  
	    $this->layout = 'user';
		$currentUser = $this->Auth->user();
		$user_id = $currentUser['id'];
		$datas=$this->Register->findById($user_id);
		$this->set('datas',$datas);
		$dropdown_page = array('cancel_bookings');
		$this->set('dropdown_page',$dropdown_page);
		
	}
//////////////////////////////////////////////////

	public function check_shipping_address()
	{
		if($this->request->is("post"))
		{
			$order = array();
			$tag_ida = $this->request->data['tagid'];
		       $tag_idb = explode(",",$tag_ida);
               $tag_id =$tag_idb[0];
              
			if ($tag_id) {
				$order = $this->OrderItem->find('first', array(
					'recursive' => -1,
	             	'contain' => array(
	             	),
	            	'conditions' => array(
	                	'OrderItem.product_code' => $tag_id,
	            	),
	            	'order' => array(              
	            	),
	        	));
	        	
	        	$order_details = $this->Order->find('first', array(
					'recursive' => -1,
	             	'contain' => array(
	             	),
	            	'conditions' => array(
	                	'Order.id' => $order['OrderItem']['order_id'],
	            	),
	            	'order' => array(              
	            	),
	        	));
        		echo json_encode($order_details);
        	}
		}
		exit;
	}

// \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\END FOR USER  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


public function admin_check_shipping_address()
	{
		if($this->request->is("post"))
		{
			$order = array();
			$tag_id = $this->request->data['tagid'];
			if ($tag_id) {
				$order = $this->OrderItem->find('first', array(
					'recursive' => -1,
	             	'contain' => array(
	             	),
	            	'conditions' => array(
	                	'OrderItem.product_code' => $tag_id,
	            	),
	            	'order' => array(              
	            	),
	        	));
	        	
	        	$order_details = $this->Order->find('first', array(
					'recursive' => -1,
	             	'contain' => array(
	             	),
	            	'conditions' => array(
	                	'Order.id' => $order['OrderItem']['order_id'],
	            	),
	            	'order' => array(              
	            	),
	        	));
        		echo json_encode($order_details);
        	}
		}
		exit;
	}

////////////////////////////////////////////////////////////////////////

	protected function update_price()
	{
		$prod_uid = $this->Auth->User();
		$prod_user_id =$prod_uid['id'];
		$session_id=$this->Session->read('browser_session');
		/////////////////////////
		$cart_check= $this->Cart->find('first', array(
			   		'recursive' => -1,
					'contain'=>array(
					),
					'conditions' => array(
						'AND'=>array(
							'Cart.sessionid'=>$session_id
						)
					),
					'fields'=>array(
						'id',
						'quantity',
						'monthly_subtotal',
						'yearly_subtotal',
						'category_id',
						'monthly_price',
						'additional_price',
						'yearly_price',
						'yearly_additional_price'
					)
			  	 ));
		//pr($cart_check);
		$prod_user_check=$this->PetProfile->find('all', array(
					'recursive' => -1,
					'contain'=>array(
					'Product'=>array('conditions' => array('active' =>1,'isdeleted' =>0))
					),

					'conditions' => array(
						'PetProfile.register_id'=>$prod_user_id,
						'PetProfile.is_active'=>1,
					),
					'order' => array('PetProfile.id' => 'desc')
				));
		$active_pet=0;
		if(!empty($prod_user_check))
		{
			$active_pet=1;
		}
		if(!empty($cart_check) && $active_pet==1)
		{
			$monthly_subtotal=0;
			$yearly_subtotal=0;
			$quantity=$cart_check['Cart']['quantity'];
			$monthly=$cart_check['Cart']['monthly_price'];
			$monthly_additional=$cart_check['Cart']['additional_price'];
			$yearly=$cart_check['Cart']['yearly_price'];
			$yearly_additional=$cart_check['Cart']['yearly_additional_price'];
			$monthly_subtotal=$quantity*$cart_check['Cart']['additional_price'];
			$yearly_subtotal=$quantity*$cart_check['Cart']['yearly_additional_price'];
			$this->Cart->id = $cart_check['Cart']['id'];
			$this->request->data['Cart']['quantity']=$quantity;
			$this->request->data['Cart']['monthly_subtotal']=$monthly_subtotal;
			$this->request->data['Cart']['yearly_subtotal']=$yearly_subtotal;
			$this->Cart->save($this->request->data) ;
		}
		//$this->redirect(array('controller'=>'pages','action' => 'product'));
	}

/////////////////////////////////////////////////////////////////////

	public function admin_setting()
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		$this->set('currentUser',$currentUser);

		if(!empty($currentUser)){

			$state=$this->State->find('all');
			$this->set('state',$state);

			$users = $this->Operator->find('all', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        
	        	'order' => array(              
	        	),
		    ));

		    $roles = $this->Role->find('all', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	        		'Role.isdeleted' => 0,
	        	),
	        	'order' => array(              
	        	),
		    ));

		    $this->set('roles',$roles);
		    $this->set('users',$users);	
		}
		else
		{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////////////////

	public function admin_update_menu_status()
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				$val = $this->request->data['val'];
				$chk = $this->request->data['chk'];
				$role_id = $this->request->data['role_id'];

				$menu = $this->Menu->find('first', array(
					'recursive' => -1,
		         	'contain' => array(
		         	),
		        	'conditions' => array(
		            	'Menu.menu_name' => $val,
		        	),
		        	'order' => array(              
		        	),
		    	));

		    	$menu_id = $menu['Menu']['id'];

		    	$func = $this->AdminFunctionality->find('first', array(
					'recursive' => -1,
		         	'contain' => array(
		         	),
		        	'conditions' => array(
		            	'AdminFunctionality.menu_id' => $menu_id,
		            	'AdminFunctionality.role_id' => $role_id,
		        	),
		        	'order' => array(              
		        	),
		    	));

		    	$this->request->data["AdminFunctionality"]["id"] = $func['AdminFunctionality']['id'];
				$this->request->data["AdminFunctionality"]["role_id"] = $role_id;
				$this->request->data["AdminFunctionality"]["active_menu"] = $menu_id;

				if ($chk == 'true') {
					$this->request->data["AdminFunctionality"]["active"] = 1;
				}
				else {
					$this->request->data["AdminFunctionality"]["active"] = 0;
					$this->request->data["AdminFunctionality"]["submenu_id"] = 0;
				}

				if ($this->AdminFunctionality->save($this->request->data))
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
			exit();
		}
		else
		{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

//////////////////////////////////////////////////////////////

	public function admin_update_sub_status()
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				$val = $this->request->data['val'];
				$role_id = $this->request->data['role_id'];
				$chk = $this->request->data['chk'];
				$parent_id = $this->request->data['parent_id'];

				// $parent_id = substr($sid,4,4);
				
				$submenu = $this->Submenu->find('first', array(
					'recursive' => -1,
		         	'contain' => array(
		         	),
		        	'conditions' => array(
		        		'Submenu.name' => $val,
		            	'Submenu.menu_id' => $parent_id,
		        	),
		        	'order' => array(              
		        	),
		    	));

		    	$submenu_id = $submenu['Submenu']['id'];

		    	$func = $this->AdminFunctionality->find('all', array(
					'recursive' => -1,
		         	'contain' => array(
		         	),
		        	'conditions' => array(
		            	'AdminFunctionality.menu_id' => $parent_id,
		            	'AdminFunctionality.role_id' => $role_id,
		        	),
		        	'order' => array(              
		        	),
		    	));

		    	if ($chk == 'true') {
			    	if ($func[0]['AdminFunctionality']['active'] == 1) {
				    	if ($func[0]['AdminFunctionality']['submenu_id'] == 0)
				    	{
				    		$this->request->data["AdminFunctionality"]["submenu_id"] = $submenu_id;
				    		// $this->request->data["AdminFunctionality"]["active"] = 1;
				    	}
				    	else {
				    		$active_subids = $func[0]['AdminFunctionality']['submenu_id'];
				    		$arr_subids = explode(",", $active_subids);
				    		array_push($arr_subids, $submenu_id);
				    		$new_subids = implode(",", $arr_subids);
				    		$this->request->data["AdminFunctionality"]["submenu_id"] = $new_subids;
				    	}
				    }
				    else {
				    	$this->request->data["AdminFunctionality"]["submenu_id"] = $submenu_id;
				    	$this->request->data["AdminFunctionality"]["active"] = 1;
				    }
				}
				else {
					$active_subids = $func[0]['AdminFunctionality']['submenu_id'];
					$arr_subids = explode(",", $active_subids);
					if (count($arr_subids) == 1)
					{
						$this->request->data["AdminFunctionality"]["submenu_id"] = 0;
						$this->request->data["AdminFunctionality"]["active"] = 0;
					}
					else if (count($arr_subids) > 1) {
						if (($key = array_search($submenu_id, $arr_subids)) !== false) {
						    unset($arr_subids[$key]);
						}
						$new_subids = implode(",", $arr_subids);
						$this->request->data["AdminFunctionality"]["submenu_id"] = $new_subids;
					}					
				}

		    	$this->request->data["AdminFunctionality"]["id"] = $func[0]['AdminFunctionality']['id'];
				$this->request->data["AdminFunctionality"]["role_id"] = $role_id;

				if ($this->AdminFunctionality->save($this->request->data))
				{
					echo 1;
				}
				else
				{
					echo 0;
				}
			}
		}
		else
		{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
		exit();
	}

/////////////////////////////////////////////////////

	public function admin_add_new_user()
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				$name = $this->request->data['user_name'];
				$email = $this->request->data['user_email'];
				$phone = $this->request->data['user_phone'];
				$city = $this->request->data['city'];
				$state = $this->request->data['state'];
				$country = $this->request->data['country'];
				$username = $this->request->data['username'];
				$password = $this->request->data['password'];
				$status = $this->request->data['status'];
				$comment = $this->request->data['comment'];
				$managertype = $this->request->data['manager_type'];

				$this->request->data["Operator"]["name"] = $name;
				$this->request->data["Operator"]["email"] = $email;
				$this->request->data["Operator"]["contact_no"] = $phone;
				$this->request->data["Operator"]["city"] = $city;
				$this->request->data["Operator"]["state"] = $state;
				$this->request->data["Operator"]["country"] = $country;
				$this->request->data["Operator"]["username"] = $username;
				$this->request->data["Operator"]["status"] = $status;
				$this->request->data["Operator"]["comments"] = $comment;
				$this->request->data["Operator"]["hint"] = $password;
				$this->request->data["Operator"]["type"] = 0;
				$this->request->data["Operator"]["created"] = date("Y-m-d H:i:s");
				$this->request->data["Operator"]["address"] = $city.', '.$state.', '.$country;
				$this->request->data["Operator"]["password"] = $password;
				$this->request->data["Operator"]["role_id"] = $managertype;
				
				$this->Operator->create();
				if ($this->Operator->save($this->request->data))
				{
					$this->Session->setFlash(__("Data has been added successfully"));
					echo 1;
				}
				else {
					echo 2;
				}
				
			}
		}
		else
		{
			$this->redirect(array('controller'=>'registers','action' => 'login'));			
		}
		exit();
	}

///////////////////////////////////////////////////////////

	public function admin_permission()
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){

			$datas = $this->Role->find('all', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	        		'Role.isdeleted'=> '0'
	        	),
	        	'order' => array(              
	        	),
	    	));
			$this->set(compact('datas'));
		}
		else
		{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

///////////////////////////////////////////////////////

	public function admin_add_new_role()
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				$this->request->data["Role"]["name"] = $this->request->data['role_name'];
				$this->request->data["Role"]["description"] = $this->request->data['role_des'];
				$this->request->data["Role"]["created"] = date("Y-m-d H:i:s");
				$this->request->data["Role"]["modified"] = 0;

				if ($this->Role->save($this->request->data))
				{
					$this->Session->setFlash(__("Data has been added successfully"));
					$flag = 1;
				}
				else {
					$flag = 0;
				}

				$id=$this->Role->getLastInsertId();

				$menu = $this->Menu->find('all', array(
					'recursive' => -1,
		         	'contain' => array(
		         	),
		        	'conditions' => array(
		            	// 'Menu.menu_name' => $val,
		        	),
		        	'order' => array(              
		        	),
		    	));

				foreach ($menu as $value) {
					$this->request->data["AdminFunctionality"]["role_id"] = $id;
					$this->request->data["AdminFunctionality"]["menu_id"] = $value['Menu']['id'];
					$this->request->data["AdminFunctionality"]["active"] = 0;
					$this->request->data["AdminFunctionality"]["submenu_id"] = 0;
					$this->AdminFunctionality->create();
					$this->AdminFunctionality->save($this->request->data);
				}

				if ($flag) {
					echo 1;
				}
				else {
					echo 0;
				}
			}
		}
		else {
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
		exit();
	}
	
////////////////////////////////////////////////////////////

	public function admin_user_edit($id)
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){

			$state=$this->State->find('all');
			$this->set('state',$state);
			
			$roles = $this->Role->find('all', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	        		'Role.isdeleted' => 0, 
	        	),
	        	'order' => array(              
	        	),
		    ));

		    $this->set('roles',$roles);

			$operators = $this->Operator->find('first', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	            	'Operator.id' => $id,
	        	),
	        	'order' => array(              
	        	),
	    	));

			$roledetails = $this->Role->find('first', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	            	'Role.id' => $operators['Operator']['role_id'],
	        	),
	        	'order' => array(              
	        	),
	    	));

	    	$this->set('operators',$operators);
	    	$this->set('roledetails',$roledetails);

	    	$menudetails = $this->Menu->find('all', array(
				'recursive' => -1,
	         	'contain' => array(
	         		'Submenu','AdminFunctionality'
	         	),
	        	'conditions' => array(
	        	),
	        	'order' => array(              
	        	),
		    ));

		    $access = $this->AdminFunctionality->find('all', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	        		'AdminFunctionality.role_id' => $operators['Operator']['role_id'],
	        	),
	        	'order' => array(              
	        	),
		    ));

		    for($i=0; $i < count($menudetails); $i++)
		    {
		    	array_push($menudetails[$i], $access[$i]['AdminFunctionality']);
		    	$menudetails[$i]['AdminFunctionality'] = $menudetails[$i][0];
				unset($menudetails[$i][0]);

				$sub_ids = explode(",", $access[$i]['AdminFunctionality']['submenu_id']);
				unset($menudetails[$i]['AdminFunctionality']['submenu_id']);
				array_push($menudetails[$i]['AdminFunctionality'], $sub_ids);
				$menudetails[$i]['AdminFunctionality']['submenu_id'] = $menudetails[$i]['AdminFunctionality'][0];
				unset($menudetails[$i]['AdminFunctionality'][0]);
				
		    }
		    $this->set('menudetails',$menudetails);
		    // pr($menudetails);
		}
		else
		{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

///////////////////////////////////////////////////////////

	public function admin_role_edit($id)
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){

			$operators = $this->Operator->find('first', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	            	'Operator.id' => $id,
	        	),
	        	'order' => array(              
	        	),
	    	));

			$roledetails = $this->Role->find('first', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	            	'Role.id' => $operators['Operator']['role_id'],
	        	),
	        	'order' => array(              
	        	),
	    	));

			$this->set('roleId',$id);
	    	$this->set('operators',$operators);
	    	$this->set('roledetails',$roledetails);

	    	$menudetails = $this->Menu->find('all', array(
				'recursive' => -1,
	         	'contain' => array(
	         		'Submenu','AdminFunctionality'
	         	),
	        	'conditions' => array(
	        	),
	        	'order' => array(              
	        	),
		    ));

		    $access = $this->AdminFunctionality->find('all', array(
				'recursive' => -1,
	         	'contain' => array(
	         	),
	        	'conditions' => array(
	        		'AdminFunctionality.role_id' => $id,
	        	),
	        	'order' => array(              
	        	),
		    ));

		    for($i=0; $i < count($menudetails); $i++)
		    {
		    	array_push($menudetails[$i], $access[$i]['AdminFunctionality']);
		    	$menudetails[$i]['AdminFunctionality'] = $menudetails[$i][0];
				unset($menudetails[$i][0]);

				$sub_ids = explode(",", $access[$i]['AdminFunctionality']['submenu_id']);
				unset($menudetails[$i]['AdminFunctionality']['submenu_id']);
				array_push($menudetails[$i]['AdminFunctionality'], $sub_ids);
				$menudetails[$i]['AdminFunctionality']['submenu_id'] = $menudetails[$i]['AdminFunctionality'][0];
				unset($menudetails[$i]['AdminFunctionality'][0]);
				
		    }
		    $this->set('menudetails',$menudetails);
		    // pr($menudetails);
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

////////////////////////////////////////////////////////////////

	public function admin_user_role_edit()
	{
		error_reporting(0);
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				$name = $this->request->data['user_name'];
				$email = $this->request->data['user_email'];
				$phone = $this->request->data['user_phone'];
				$city = $this->request->data['city'];
				$state = $this->request->data['state'];
				$country = $this->request->data['country'];
				$username = $this->request->data['username'];
				$password = $this->request->data['password'];
				$status = $this->request->data['status'];
				$comment = $this->request->data['comment'];
				$managertype = $this->request->data['manager_type'];
				$adminId = $this->request->data['admin_id'];

				$this->request->data["Operator"]["id"] = $adminId;
				$this->request->data["Operator"]["name"] = $name;
				$this->request->data["Operator"]["email"] = $email;
				$this->request->data["Operator"]["contact_no"] = $phone;
				$this->request->data["Operator"]["city"] = $city;
				$this->request->data["Operator"]["state"] = $state;
				$this->request->data["Operator"]["country"] = $country;
				$this->request->data["Operator"]["username"] = $username;
				$this->request->data["Operator"]["status"] = $status;
				$this->request->data["Operator"]["comments"] = $comment;
				$this->request->data["Operator"]["hint"] = $password;
				$this->request->data["Operator"]["type"] = 0;
				$this->request->data["Operator"]["modified"] = date("Y-m-d H:i:s");
				$this->request->data["Operator"]["address"] = $city.', '.$state.', '.$country;
				$this->request->data["Operator"]["password"] = $password;
				$this->request->data["Operator"]["role_id"] = $managertype;

				if ($this->Operator->save($this->request->data))
				{
					$this->Session->setFlash(__("Data has been updated successfully"));
					echo 1;
				}
				else {
					echo 0;
				}
			}
		}
		exit();
	}

//////////////////////////////////////////////

	public function admin_user_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
		if($this->request->is("post"))
		{   

			throw new MethodNotAllowedException();
		}
		else
		{
			if($this->Operator->delete($id)){
			$data=$this->Operator->findById($id);
           
			$this->Operator->id= $data['Operator']['id'];
			$this->request->data['Operator']['name'] = $data['Operator']['name'];
			$this->request->data['Operator']['email'] = $data['Operator']['email'];
			$this->request->data['Operator']['contact_no'] = $data['Operator']['contact_no'];
			$this->request->data['Operator']['role_id'] = $data['Operator']['role_id'];
			$this->request->data['Operator']['hear'] = $data['Operator']['hear'];
			$this->request->data['Operator']['online_experience'] = $data['Operator']['online_experience'];
			$this->request->data['Operator']['address'] = $data['Operator']['address'];
			$this->request->data['Operator']['city'] = $data['Operator']['city'];
			$this->request->data['Operator']['state'] = $data['Operator']['state'];
			$this->request->data['Operator']['country'] = $data['Operator']['country'];
			$this->request->data['Operator']['image'] = $data['Operator']['image'];
			$this->request->data['Operator']['username'] = $data['Operator']['username'];
			$this->request->data['Operator']['status']=0;
			$this->request->data['Operator']['created'] = $data['Operator']['created'];
			$this->request->data['Operator']['modified'] = date("Y-m-d h:i:s");
			$this->request->data['Operator']['type'] = $data['Operator']['type'];
			$this->request->data['Operator']['hint'] = $data['Operator']['hint'];
			$this->request->data['Operator']['password'] = $data['Operator']['hint'];
			$this->request->data['Operator']['comments'] = $data['Operator']['comments'];
			$this->Operator->save($this->request->data["Operator"]);

			$this->Session->setFlash(__("Data has been deleted successfully"));
			$this->redirect(array("action"=>"admin_setting"));
		}}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

///////////////////////////////////////////////////////

	public function admin_role_delete($id=NULL)
	{
		$currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			if($this->request->is("post"))
			{
				throw new MethodNotAllowedException();
			}
			else
			{
				$data=$this->Role->findById($id);
				$datas=$this->Operator->find('all', array(
					'recursive' => -1,
		         	'contain' => array(
		         	),
		        	'conditions' => array(
		        		'Operator.role_id' => $id,
		        	),
		        	'order' => array(              
		        	),
			    ));
				foreach ($datas as $val) {
					$this->Operator->id= $val['Operator']['id'];
					$this->request->data['Operator']['name'] = $val['Operator']['name'];
					$this->request->data['Operator']['email'] = $val['Operator']['email'];
					$this->request->data['Operator']['contact_no'] = $val['Operator']['contact_no'];
					$this->request->data['Operator']['role_id'] = $val['Operator']['role_id'];
					$this->request->data['Operator']['hear'] = $val['Operator']['hear'];
					$this->request->data['Operator']['online_experience'] = $val['Operator']['online_experience'];
					$this->request->data['Operator']['address'] = $val['Operator']['address'];
					$this->request->data['Operator']['city'] = $val['Operator']['city'];
					$this->request->data['Operator']['state'] = $val['Operator']['state'];
					$this->request->data['Operator']['country'] = $val['Operator']['country'];
					$this->request->data['Operator']['image'] = $val['Operator']['image'];
					$this->request->data['Operator']['username'] = $val['Operator']['username'];
					$this->request->data['Operator']['status']=0;
					$this->request->data['Operator']['created'] = $val['Operator']['created'];
					$this->request->data['Operator']['modified'] = date("Y-m-d h:i:s");
					$this->request->data['Operator']['type'] = $val['Operator']['type'];
					$this->request->data['Operator']['hint'] = $val['Operator']['hint'];
					$this->request->data['Operator']['password'] = $val['Operator']['hint'];
					$this->request->data['Operator']['comments'] = $val['Operator']['comments'];
					$this->Operator->save($this->request->data["Operator"]);
				}

				$this->request->data["Role"]["id"] = $id;
				$this->request->data["Role"]["isdeleted"] = 1;
				$this->Role->save($this->request->data);

				$this->Session->setFlash(__("Data has been deleted successfully"));
				$this->redirect(array("action"=>"admin_permission"));
			}
		}
		else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}
	}

/////////////////////////////////////////////////////

	public function admin_customer_view($id=null)
	{
		$currentUser = $this->Auth->user();

	}
///////////////////////////////////////////////////

	public function admin_customer_fetch($id=null)
	{
		$currentUser = $this->Auth->user();
		// $id=399;
		if(!empty($currentUser))
		{
			$datas=$this->Register->find('all',array(
		 				'contain'=>array('PetProfile'=>array('Product'=>array('conditions'=>array(
		     			// 'Product.is_retail' => 0,
		     			'Product.isdeleted' => 0)))),
		 				'conditions'=>array('Register.id'=>$id),
		 				'order' => array(
	                	'Register.id' => 'DESC'
	            			),
		 			));
		 			$data2=array();
		 			$pos=0;
		 			foreach ($datas as $data) {
		 					$pos2=0;
		 				foreach ($data['PetProfile'] as $pet) {
		 					if($pet['Product']['is_retail']==0){
		 						if($data['Register']['id']!=$data2[$pos-1]['Register']['id'])
		 						{
		 							$data2[$pos]['Register']=$data['Register'];
		 							$insert=$pos;
		 						}
		 						$data2[$insert]['PetProfile'][$pos2]=$pet;
		 						$pos2++;
		 						
		 					}
		 				}
		 				$pos++;
		 			}
		 // 			$datass=$data2;
		 // echo json_encode($datass[0]);
		 ////////////////////////
		 $registers=$this->Register->find('all',array(
		 				'contain'=>array('PetProfile'=>array('Product'=>array('conditions'=>array(
		     			'Product.is_retail' => 1,
		     			'Product.isdeleted' => 0)))),
		 				'conditions'=>array('Register.id'=>$id),
		 				'order' => array(
	                	'Register.id' => 'DESC'
	            			),
		 			));
		 			$data3=array();
		 			$pos=0;
		 			foreach ($registers as $data) {
		 					$pos2=0;
		 				foreach ($data['PetProfile'] as $pet) {
		 					// pr($pet);
		 					if($pet['Product']['is_retail']==1){
		 						//pr($pet);
		 						if($data['Register']['id']!=$data3[$pos-1]['Register']['id'])
		 						{
		 							$data3[$pos]['Register']=$data['Register'];
		 							$insert=$pos;
		 						}
		 						$data3[$insert]['PetProfile'][$pos2]=$pet;
		 						$pos2++;
		 						// $pos++;
		 					}
		 				}
		 				$pos++;
		 			}
		 	$data2[0]['Retail']=$data3[0]['PetProfile'];
		 	if(!isset($data2[0]['Register']))
		 	{
		 		$data2[0]['Register']=$data3[0]['Register'];
		 	}
		 	$datass=$data2;
		 echo json_encode($datass[0]);
		}
		exit;
	}

	/////////////////////////////////////
	public function admin_delete_order($id){
		// echo $id;
		$order=$this->Order->find('first',array(
			'conditions'=>array(
				'id'=>$id),
			'contain'=>array('OrderItem'),
			// 'fields'=>array('id','invoice_no','plan_id','shipping_cost','shipping_charge_id'),
		));
		// pr($order);
		$user_id=$order['Order']['user_id'];
		// echo "\n".$user_id;
		$stripe_data=$this->StripeData->find('first',array(
			'conditions'=>array(
				'StripeData.customer_id like'=>$order['Order']['invoice_no'],
				'StripeData.subscription_id like'=>$order['Order']['plan_id'],
			)));
		// echo 1;
		// pr($stripe_data);
		$stripe_id= $order['Order']['plan_id'];
		// echo $stripe_data['StripeData']['price'];
		$sub_retrive=$this->Stripe->customerRetrieveSubscriptions($stripe_id);
		$refund_amount=0;
		foreach($sub_retrive['items'] as $val){
			$refund_amount+=$val['amount'];
		}
		// echo $refund_amount;
		// pr($sub_retrive);
		// exit;
		// pr($stripe_data);
		$this->Order->id=$id;
		$this->Order->saveField('isadmindelete',1);
		if($this->Order->saveField('isadmindelete',1)){
			if($order['Order']['plan_id']){
			if($this->Stripe->SubscriptionCancel($order['Order']['plan_id'])) //Cancel Subscription
			{
				$data=array( 
					'charge_id'=>$stripe_data['StripeData']['charge_id'],
					'refund_amount'=>$refund_amount,);
				// pr($data);
				$refund=$this->Stripe->refund_stripe($data);
				$refund_delivery=array(
					'charge_id'=>$order['Order']['shipping_charge_id'],
					'refund_amount'=>$order['Order']['shipping_cost']*100,);
				// pr($refund_delivery);
				$refund=$this->Stripe->refund_stripe($refund_delivery);
				// pr($refund);
				if($refund)
				{
				   $msg="Order has been successfully deleted";
		           $this->Session->setFlash($msg);
				}  
			}
			}
			foreach ($order['OrderItem'] as $key => $value) {
				$this->Product->id=$value['product_id'];
				// echo "\n".$value['product_id'];
				$product=array('active'=>0,);
				if($this->Product->save($product))
				{
					$pet_profile=$this->PetProfile->find('first',array(
					'conditions'=>array(
						'product_id'=>$value['product_id'],
						'is_active'=>1,
						'is_cancellations'=>0,
						'register_id'=>$user_id,
					),
				));
				// pr($pet_profile);
				$pet_profile['PetProfile']['cancellations_date']=date('Y-m-d h-m-s');
				$pet_profile['PetProfile']['is_cancellations']=1;
				$d=$this->PetProfile->save($pet_profile);
				// pr($d);
				}
			}
		}
		$this->redirect(array('controller'=>'registers','action' => 'admin_new_order_list'));
	}
	public function admin_deleted_order_list()
	{
	    $currentUser = $this->Auth->user();
		if(!empty($currentUser)){
			$order_details=$this->Order->find('all', array(
         		'recursive' => -1,
		 		'contain' => array(
	     			'OrderItem'
   				),
	   			'conditions' => array(
                	'Order.isadmindelete' =>1
        		),
       			'order' => array(
                	'Order.id' => 'DESC'
            	)
        	));
			$this->set(compact('order_details'));
			$count_order = sizeof($order_details);
			$this->set(compact('count_order'));
			if($this->request->is("post") or $this->request->is("put")){
				$post_start_date=$this->request->data['start_date'];
				$post_end_date=$this->request->data['end_date'];				
				$start_date=date('Y-m-d', strtotime( $this->request->data['start_date'] ));
				$end_date=date('Y-m-d', strtotime( $this->request->data['end_date'] ));
				$order_details=$this->Order->find('all', array(
            		'recursive' => -1,
		    		'contain' => array(
	        			'OrderItem'
	        		),
	        		'conditions' => array(
			    		'and' => array(
							'Order.created BETWEEN ? and ?' => array($start_date, $end_date),
                			'Order.is_dispatch' =>1
             			)
			    	),
            		'order' => array(
                		'Order.id' => 'DESC'
         			)
            	));
				$this->set(compact('order_details'));
				$this->set(compact('post_start_date','post_end_date'));
			}
		}
     	else{
			$this->redirect(array('controller'=>'registers','action' => 'login'));
		}		 
	}
	public function admin_deleted_order_view($id=NULL)
    {


       if($id==NULL){
				$msg="REQUIRE PET ID ";
				$this->set(compact('msg'));}
           if( filter_var($id, FILTER_VALIDATE_INT) === false ){
            $datas="";}else{
		$datas=$this->Order->findById($id);}
        $this->set(compact('datas'));




        // if($id==NULL)
        // {
        //     throw new NotFoundException(__("Invalid id"));
        // }
       	$order_view=$this->Order->find('first', array(
         	'recursive' => -1,
		 	'contain' => array(
	     		'OrderItem' =>array('Product','Category')
	   		),
	   		'conditions'=>array(
                'Order.id' => $id,
				'Order.isadmindelete' => 1
            ),
      		'order' => array(
                'Order.id' => 'DESC'
            )
        ));
        // if(!$order_view)
        // {
        //     throw new NotFoundException(__("Invalid id"));
        // }
        $this->set("order_view",$order_view);
    }

//////////////////////////////////////////////////////////

}
?>