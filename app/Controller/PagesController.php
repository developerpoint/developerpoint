<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link https://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

	// public $components= array('Session');


/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array("Register","Order");
    public $helpers = array('Html', 'Form', 'Js'=>array("Jquery"),"Session");
    public $components = array('RequestHandler','Session');
/** 
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function index()
	{
		$this->layout='bisu';

		//$hh=$this->Auth->User();
		
	}
	
	public function test()
	{
		$this->layout='ajax';
		$results = $this->Register->find('all');
		pr($results);
 
	}
	public function project()
	{
		$this->layout='bisu';
		
	}
	public function addorder()
	{
		$this->layout='bisu';
		if($this->request->is("post")){
		    $orderid=substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$this->request->data['Order']['price']= 10;
		$this->request->data['Order']['name']=$this->request->data['name'];
		$this->request->data['Order']['mail']=$this->request->data['mail'];
		$this->request->data['Order']['number']=$this->request->data['number'];
		$this->request->data['Order']['address']=$this->request->data['address'];
		$this->request->data['Order']['orderid']= $orderid;
			$name=$this->request->data['Order']['name'];
		     if($this->Order->save($this->request->data)){

		     	session_start();  
		     	 $_SESSION["mail"] = $this->request->data['mail'];
			     $_SESSION["number"] =$this->request->data['number'];
			     $_SESSION["address"] = $this->request->data['address'];
			     $_SESSION["price"] = 10;
			     $_SESSION["product"] = xyz;
			     $_SESSION["orderid"] = $orderid;
		     	$this->redirect(array('controller' => 'pages', 'action' => 'payu', $orderid));
		     }
		}
	}

public function payu($orderid=null){
	$this->layout='bisu';
		$orderdata=$this->Order->find('all', array(
     			'recursive' => 1,
   				'conditions' => array(
   					'Order.orderid' => $orderid			
   				)	
  			));
		$name = $orderdata['0']['Order']['name'];
		$mail = $orderdata['0']['Order']['mail'];
		$number = $orderdata['0']['Order']['number'];
		$price = $orderdata['0']['Order']['price'];
$this->set(compact('orderdata'));
$this->set(compact('name'));
$this->set(compact('mail'));
$this->set(compact('number'));
$this->set(compact('price'));		
$MERCHANT_KEY = "3ixb5KLn";
$SALT = "D4geUBBAEm";
$PAYU_BASE_URL = "https://sandboxsecure.payu.in";
//$PAYU_BASE_URL = "https://secure.payu.in";
$action = '';
$posted = array();
if(!empty($_POST)) {
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
  }
}
$formError = 0;
if(empty($posted['txnid'])) {
  $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
  $txnid = $posted['txnid'];
}
$hash = '';
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
             empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }
    $hash_string .= $SALT;
    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
$this->set(compact('action'));
$this->set(compact('hash'));
$this->set(compact('txnid'));
$this->set(compact('MERCHANT_KEY'));
$this->set(compact('formError'));
  }

public function success($orderid=null,$txnid=null){
       $this->layout='bisu';
       
 $txnid=$_POST["txnid"];

       $orderdata=$this->Order->find('all', array(
     			'recursive' => 1,
   				'conditions' => array(
   					'Order.orderid' => $orderid			
   				)	
  			));
        $id = $orderdata['0']['Order']['id'];
		$name = $orderdata['0']['Order']['name'];
		$mail = $orderdata['0']['Order']['mail'];
		$number = $orderdata['0']['Order']['number'];
		$price = $orderdata['0']['Order']['price'];

$this->set(compact('orderdata'));
$this->set(compact('name'));
$this->set(compact('mail'));
$this->set(compact('number'));
$this->set(compact('price'));

        $this->request->data["Order"]["id"]=$id;
        $this->request->data["Order"]["payment"]=$txnid;
		$this->Order->save($this->request->data);
		

}

////////////////////////////////////////////////////////////////////////////////////////////////////
}
